package org.fm.impl.swing.map;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import org.fm.FmEventsDispatcher;
import org.fm.map.model.FmDevicePoint;
import org.fm.map.model.FmDeviceExtent;
import org.fm.map.model.FmDevicePolygon;
import org.fm.map.model.FmMapDisplayDriver;
import org.fm.map.model.FmMapLineSymbol;
import org.fm.map.model.FmMapMarkerSymbol;
import org.fm.map.model.FmMapPolygonSymbol;

public class FmMapDisplayDriverSwing implements FmMapDisplayDriver {

    private static final Map<String, Font> fontCache = new HashMap<String, Font>();
    private FmEventsDispatcher evDispatcher;
    private final JComponent swingComponent;

    public FmMapDisplayDriverSwing(JComponent c) {
        super();
        swingComponent = c;
        _init();
    }

 
    private void _init() {
        evDispatcher = new FmEventsDispatcher(this);
    }


    public Object getCanvas() {
        return swingComponent.getGraphics();
    }

    @Override
    public FmDeviceExtent getViewSize() {
        Rectangle bounds = swingComponent.getBounds();

        return new FmDeviceExtent(bounds.x, bounds.y, bounds.width + bounds.x - 1, bounds.height + bounds.y - 1);
    }

    public void drawImage(Object canvas, int[] imagearr, int cols, int rows, int xdsp, int ydsp) {
        BufferedImage image = new BufferedImage(cols, rows, BufferedImage.TYPE_INT_ARGB);
        image.setRGB(0, 0, cols, rows, imagearr, 0, cols);
        ((Graphics) canvas).drawImage(image, xdsp, ydsp, null);
    }

    public void drawMarker(Object canvas, FmDevicePoint pt, FmMapMarkerSymbol sym) {
        Graphics2D g = (Graphics2D) canvas;
        Color oldcolor = g.getColor();
        Font oldfont = g.getFont();

        int style = sym.getStyle() == FmMapMarkerSymbol.Style.ITALIC
                ? Font.ITALIC : (sym.getStyle() == FmMapMarkerSymbol.Style.BOLD
                ? Font.BOLD : Font.PLAIN);
        Font f = _getFont(sym.getFontName(), style, sym.getSize());
        g.setFont(f);
        g.setColor(new Color(sym.getRgba(), true));

        FontMetrics fm = g.getFontMetrics();
        Rectangle2D rect = fm.getStringBounds(sym.getChar(), g);
        int xt = pt.getX() - (int) (rect.getWidth() / 2);
        int yt = pt.getY() - fm.getAscent() / 2;
        g.drawString(sym.getChar(), xt, yt);

        g.setFont(oldfont);
        g.setColor(oldcolor);

    }

    public void drawPolygon(Object canvas, FmDevicePolygon poly, FmMapPolygonSymbol sym) {
        Graphics2D g = (Graphics2D) canvas;
        Color oldcolor = g.getColor();
        Stroke oldstroke = g.getStroke();
        Paint oldpaint = g.getPaint();
        List<List<FmDevicePoint>> outlines = poly.getOutlines();
        if (outlines == null) {
            return;
        }
        FmDevicePolygon[] holes = poly.getInnerPolygons();

        // outlines
        GeneralPath gpPoly = null;

        for (List<FmDevicePoint> outline : outlines) {
            GeneralPath gpOline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 20);
            for (int dpi = 0; dpi < outline.size() - 1; dpi++) { // poly outline is closed
                FmDevicePoint dp = outline.get(dpi);
                if (dpi == 0) {
                    gpOline.moveTo(dp.getX(), dp.getY());
                } else {
                    gpOline.lineTo(dp.getX(), dp.getY());
                }
            }
            gpOline.closePath();
            if (gpPoly == null) {
                gpPoly = gpOline;
            } else {
                gpPoly.append(gpOline, false);
            }
        }

        // holes        
        if (holes != null) {
            for (int h = 0; h < holes.length; h++) {
                outlines = holes[h].getOutlines();
                for (List<FmDevicePoint> outline : outlines) {
                    GeneralPath gpHole = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 20);
                    for (int dpi = 0; dpi < outline.size() - 1; dpi++) { // poly outline is closed
                        FmDevicePoint dp = outline.get(dpi);
                        if (dpi == 0) {
                            gpHole.moveTo(dp.getX(), dp.getY());
                        } else {
                            gpHole.lineTo(dp.getX(), dp.getY());
                        }
                    }
                    gpHole.closePath();
                    gpPoly.append(gpHole, false);
                }
            }
        }
        g.setPaint(new Color(sym.getRgba(), true));
        g.fill(gpPoly);
        g.setPaint(oldpaint);
        
        g.setColor(new Color(sym.getOutlineRgba(), true));
        g.setStroke(new BasicStroke(sym.getOutlineWidth(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));        
        g.draw(gpPoly);
        g.setStroke(oldstroke);
        g.setColor(oldcolor);
        
    }

    public void drawLine(Object canvas, FmDevicePoint[] ptarr, FmMapLineSymbol sym) {
        Graphics2D g = (Graphics2D) canvas;
        if (sym == null) {
            return;
        }

        Color oldcolor = g.getColor();
        Stroke oldstroke = g.getStroke();

        BasicStroke stroke;
        if (sym.getSections().length < 1) {
            stroke = new BasicStroke(
                    sym.getWidth(),
                    (sym.getRounding().equals(FmMapLineSymbol.Rounding.SQUARE)
                    ? BasicStroke.CAP_SQUARE
                    : (sym.getRounding().equals(FmMapLineSymbol.Rounding.ROUND)
                    ? BasicStroke.CAP_ROUND
                    : BasicStroke.CAP_BUTT)),
                    BasicStroke.JOIN_ROUND);
        } else {
            stroke = new BasicStroke(
                    sym.getWidth(),
                    (sym.getRounding().equals(FmMapLineSymbol.Rounding.SQUARE)
                    ? BasicStroke.CAP_SQUARE
                    : (sym.getRounding().equals(FmMapLineSymbol.Rounding.ROUND)
                    ? BasicStroke.CAP_ROUND
                    : BasicStroke.CAP_BUTT)),
                    BasicStroke.JOIN_ROUND,
                    10f,
                    sym.getSections(),
                    0.0f);
        }
        g.setStroke(stroke);
        g.setColor(new Color(sym.getRgba(), true));

        int[] xPoints = new int[ptarr.length];
        int[] yPoints = new int[ptarr.length];
        for (int i = 0; i < ptarr.length; i++) {
            xPoints[i] = ptarr[i].getX();
            yPoints[i] = ptarr[i].getY();
        }
        g.drawPolyline(xPoints, yPoints, xPoints.length);

        g.setStroke(oldstroke);
        g.setColor(oldcolor);
    }

    private Font _getFont(String name, int style, int s) {
        String fid = name + "-" + style + "-" + s;
        if (fontCache.containsKey(fid)) {
            return fontCache.get(fid);
        }
        Font f = new Font(name, style, s);
        fontCache.put(fid, f);
        return f;
    }

    public void clear(Object canvas) {
        try {
            Graphics g = (Graphics) canvas;
            g.setColor(Color.white);
            g.fillRect(0, 0, swingComponent.getWidth(), swingComponent.getHeight());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Dispatcher ifc
    public <T> boolean addListener(T receiver) {
        return evDispatcher.addListener(receiver);
    }

    public <T> void fireEvent(String ev, T evData) {
        evDispatcher.fireEvent(ev, evData);
    }

    public void removeAllListeners() {
        evDispatcher.removeAllListeners();
    }

    public <T> boolean removeListener(T receiver) {
        return evDispatcher.removeListener(receiver);
    }

    public int getListenersCount() {
        return evDispatcher.getListenersCount();
    }

    public <T> List<T> getListeners() {
        return evDispatcher.getListeners();
    }

    public <T> boolean isListener(T receiver) {
        return evDispatcher.isListener(receiver);
    }

    public String getID() {
        return evDispatcher.getID();
    }

}
