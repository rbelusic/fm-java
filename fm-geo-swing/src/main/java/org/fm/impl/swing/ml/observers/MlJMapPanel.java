package org.fm.impl.swing.ml.observers;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoPoint;
import org.fm.impl.swing.map.FmMapDisplayDriverSwing;
import org.fm.impl.swing.ml.MlObserverSwing;
import org.fm.impl.swing.ml.MlObserverSwingComponent;
import org.fm.map.model.FmDevicePoint;
import org.fm.map.model.FmMapDisplay;
import org.fm.map.model.FmMapDisplayDriver;
import org.fm.map.model.FmMapDisplayImpl;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;

/**
 *
 * @author rbelusic
 */
public class MlJMapPanel extends JPanel implements
        MlObserverSwingComponent,
        ComponentListener, MouseListener, MouseMotionListener {
    private long updateTimestamp=0;
    
    private Timer refreshTimer = null;
    private Rectangle selection;
    private Point anchor;
    private FmMapDisplay display;
    private FmMapDisplayDriver driver;
    private MlObserverSwing observer;
    private String attributeName;
    private Object updateValue;

    public MlJMapPanel() {
        super();
        _init();
    }

    public MlJMapPanel(boolean isDoubleBuffered) {
        super(isDoubleBuffered);
        _init();
    }

    public MlJMapPanel(LayoutManager layout) {
        super(layout);
        _init();
    }

    public MlJMapPanel(LayoutManager layout, boolean isDoubleBuffered) {
        super(layout, isDoubleBuffered);
        _init();
    }

    private void _init() {
        observer = new MlObserverSwing(this);
        
        setBackground(Color.WHITE);
        setForeground(Color.BLACK);
        addMouseListener(this);
        addMouseMotionListener(this);
        addComponentListener(this);
        driver = new FmMapDisplayDriverSwing(this);
        display = new FmMapDisplayImpl(driver);
    }

    // -- oWR ---
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        _redrawMap(g);
    }

    // -- priv --
    private void _showSelection(Rectangle sel) {
        Graphics g = getGraphics();
        g.setXORMode(Color.RED);
        g.drawRect((int) sel.getMinX(), (int) sel.getMinY(), (int) sel.getWidth(), (int) sel.getHeight());
        g.fillRect((int) sel.getMinX(), (int) sel.getMinY(), (int) sel.getWidth(), (int) sel.getHeight());
    }

    private void _redrawMap(Graphics g) {
        // redraw all layers
        getDisplay().redraw(g);
    }
    // ComponentListener    
    private volatile int refreshRequired = 0;

    @Override
    public void componentResized(ComponentEvent e) {
        refreshRequired++;
        if (refreshTimer == null) {
            refreshTimer = new Timer(500, new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (refreshRequired == 0) {
                        refreshTimer.stop();
                    } else {
                        refreshRequired = 0;
                        _fireEvent("onMapSizeChanged", getDisplay().getViewSize());
                        getDisplay().setMapExtent(getDisplay().getMapExtent());
                        _fireEvent("onMapExtentChanged", getDisplay().getMapExtent());
                    }
                }
            });
        } else if (!this.refreshTimer.isRunning()) {
            this.refreshTimer.restart();
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    // MouseListener 
    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("EV CLICK:" + e.getButton());

        if (SwingUtilities.isRightMouseButton(e)) {
            getDisplay().setMapCenter(getDisplay().device2geo(new FmDevicePoint(e.getX(), e.getY())));
            _fireEvent("onMapExtentChanged", getDisplay().getMapExtent());
        } else if(SwingUtilities.isLeftMouseButton(e)) {
            _fireEvent("onMouseMapLocationSelected", new FmDevicePoint(e.getX(), e.getY()));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        anchor = e.getPoint();
        selection = new Rectangle(anchor);
        _showSelection(selection);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (selection.getWidth() < 2 || selection.getHeight() < 2) {
            // zoom to all
            if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2) {
                getDisplay().zoomToAll();
            }
            return;
        }

        FmGeoPoint pt1 = getDisplay().device2geo(new FmDevicePoint(selection.x, selection.y));
        FmGeoPoint pt2 = getDisplay().device2geo(new FmDevicePoint(selection.x + selection.width - 1, selection.y + selection.height - 1));

        _showSelection(selection);
        selection = null;

        if (e.getButton() == MouseEvent.BUTTON1) {
            getDisplay().setMapExtent(new FmGeoExtent(pt1, pt2));
            _fireEvent("onMapExtentChanged", getDisplay().getMapExtent());
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    // MouseMotionListener
    @Override
    public void mouseDragged(MouseEvent e) {
        _showSelection(selection);
        selection.setBounds(
                (int) Math.min(anchor.x, e.getX()), (int) Math.min(anchor.y, e.getY()),
                (int) Math.abs(e.getX() - anchor.x), (int) Math.abs(e.getY() - anchor.y));
        _showSelection(selection);

    }

    @Override
    public void mouseMoved(MouseEvent e) {        
            _fireEvent( "onMouseMapLocation", 
                getDisplay().device2geo(new FmDevicePoint(e.getX(), e.getY())));
    }

    public void init() {
        getObserver().init();
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }

    // MlObserverSwingComponent
    public void update(MlHost caller) {
        DmObject dm = observer.getHost().getDmObject();

        if (getAttributeName() != null && getUpdateTimestamp() < dm.getUpdateTimestamp()) {
            Object value = dm.getAttr(getAttributeName(), null);
            if(value != updateValue) {
                updateValue = value;
                setUpdateTimestamp(dm.getUpdateTimestamp());
                _fireEvent("onMapRedrawStart", getDisplay());
                getDisplay().redraw(getGraphics());
                _fireEvent("onMapRedrawEnd", getDisplay());
            }
        }
    }

    public void verify() throws FmException {
    }

    public MlObserver getObserver() {
        return observer;
    }

    public Container getNode() {
        return this;
    }

    // obs props
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    
    private void _fireEvent(String ev, Object args) {
        if(observer.getHost() != null) 
            observer.getHost().onObserverEvent(observer, ev,args); 
    }    

    /**
     * @return the display
     */
    public FmMapDisplay getDisplay() {
        return display;
    }
}
