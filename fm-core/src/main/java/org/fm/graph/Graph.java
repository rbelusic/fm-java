package org.fm.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.graph.bo.BoGraph;
import org.fm.graph.model.DmGraph;
import org.fm.graph.model.DmGraphEdge;
import org.fm.graph.model.DmGraphVertex;

/**
 *
 * @author RobertoB
 * @param <V>
 * @param <E>
 */
public class Graph<V extends DmGraphVertex, E extends DmGraphEdge> {
    private final HashMap<String, V> vertexCache = new HashMap<String, V>();
    
    private Class<V> vertexClass;
    private Class<E> edgesClass;
    
    private DmGraph model;

    public  Graph(DmGraph m, Class<V> vtxClass, Class<E> edgClass) {
        model = m;
        vertexClass = vtxClass;
        edgesClass = edgClass;
    }

    public Graph(String id,Class<V> vtxClass, Class<E> edgClass) {
        model = DmFactory.create(DmGraph.class);
        model.setId(id);
        vertexClass = vtxClass;
        edgesClass = edgClass;
    }

    
    public String getId() {
        return model.getID();
    }

    public V getVertex(String id) {
        if (vertexCache.containsKey(id)) {
            return (vertexCache.get(id));
        }
        V vtx = (V) ((BoGraph)FmApplication.instance().getBo(BoGraph.class)).getVertex(vertexClass,id);
        if (vtx != null) {
            vertexCache.put(id, vtx);
            vtx.setInsAttr("graph", this);
        }
        return vtx;
    }

    
    // -- static DM Attr methods -----------------------------------------------
    public static String[] getVertexNeighborhood(DmGraphVertex dm) {
        ArrayList<String> exits = new ArrayList<String>();
        if (dm == null) {
            return exits.toArray(new String[exits.size()]);
        }

        for (DmGraphEdge e : dm.getEdges(null)) {
            exits.add(
                    e.getFromNode(null).equals(dm.getDataID()) ? 
                    e.getToNode(null) : e.getFromNode(null)
                );
        }

        return exits.toArray(new String[exits.size()]);
    }
    
    public static List<DmGraphEdge> getVertexEdges(DmObject dm, DmObject dmDef /* ignore*/) {
        if (dm == null) {
            return null;
        }
        if (dm.getAttr("edges", null) == null && dm.getInsAttr("graph", null)!= null) {
            Graph g= dm.getInsAttr("graph", null);
            BoGraph bo = (BoGraph) FmApplication.instance().getBo(BoGraph.class);
            if (bo != null) {
                try {
                    bo.loadVertexEdges(g.edgesClass, (DmGraphVertex)dm);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (dm.getAttr("edges", null) == null) {
                dm.setAttr("edges", new ArrayList());
            }
        }

        return dm.getAttr("edges", null);
    }

    public static List<DmGraphEdge> getVertexExits(DmObject dm, DmObject dmDef /* ignore*/) {
        ArrayList<DmGraphEdge> exits = new ArrayList<DmGraphEdge>();
        if (dm == null) {
            return exits;
        }

        DmGraphVertex vtx = (DmGraphVertex)dm;
        for (DmGraphEdge e : vtx.getEdges(null)) {
            if ((e.getFromNode(null).equals(vtx.getDataID()) && e.getDirection(0) >= 0)
                    || (e.getToNode(null).equals(vtx.getDataID()) && e.getDirection(0) <= 0)) {
                exits.add(e);
            }
        }

        return exits;
    }
    
    
    public static List<DmGraphEdge> getVertexEntrances(DmObject dm, DmObject dmDef /* ignore*/) {
        ArrayList<DmGraphEdge> entrances = new ArrayList<DmGraphEdge>();
        if (dm == null) {
            return entrances;
        }

        DmGraphVertex vtx = (DmGraphVertex)dm;
        for (DmGraphEdge e : vtx.getEdges(null)) {
            if ((e.getFromNode(null).equals(vtx.getDataID()) && e.getDirection(0) <= 0)
                    || (e.getToNode(null).equals(vtx.getDataID()) && e.getDirection(0) >= 0)) {
                entrances.add(e);
            }
        }

        return entrances;
    }
}