package org.fm.graph.algol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import org.fm.graph.Graph;
import org.fm.graph.model.DmGraphEdge;
import org.fm.graph.model.DmGraphVertex;


public class Dijkstra {

    public static class VertexDistanceComparator implements Comparator<DmGraphVertex> {

        @Override
        public int compare(DmGraphVertex x, DmGraphVertex y) {
            // Assume neither string is null. Real code should
            // probably be more robust
            double xmin =x.getInsAttr("minDistance", 0.);
            double ymin =y.getInsAttr("minDistance", 0.);
            if (xmin < ymin) {
                return -1;
            }
            if (xmin > ymin) {
                return 1;
            }
            return 0;
        }
    }

    public static ArrayList<DmGraphVertex> computeShortestPath(
        Graph g, String sourceId, String destId
    ) {
        System.out.println("csp:" + sourceId + " => " + destId);
        DmGraphVertex source = g.getVertex(sourceId);
        DmGraphVertex dest = g.getVertex(destId);
        source.setInsAttr("minDistance", 0.);
        source.setInsAttr("previous", null);
        PriorityQueue<DmGraphVertex> vertexQueue = new PriorityQueue<DmGraphVertex>(
                10,
                new Dijkstra.VertexDistanceComparator()
        );
        vertexQueue.add(source);

        while (!vertexQueue.isEmpty()) {
            DmGraphVertex u = vertexQueue.poll();
            u.setInsAttr("processed", true);
            double minDistance = u.getInsAttr("minDistance", Double.POSITIVE_INFINITY);
            System.out.println("U vertex:" + u.getDataID());
            if (minDistance  == Double.POSITIVE_INFINITY) {
                break;
            }
            // Visit each edge exiting u            
            for (DmGraphEdge e : u.getExits(null)) {                
                DmGraphVertex v = g.getVertex(
                        e.getFromNode("").equals(u.getDataID()) ?
                        e.getToNode("") :e.getFromNode("")
                    );
                if(v.getInsAttr("processed", false)) continue;
                
                System.out.println("   Edge to vertex:" + v.getDataID());
                double alt = minDistance + e.getVeight(1.);
                if (alt < v.getInsAttr("minDistance", Double.POSITIVE_INFINITY)) {
                    System.out.println("   New distance for " + v.getDataID() + ":" + alt);
                    System.out.println("   Remove from queue:" + v.getDataID());
                    vertexQueue.remove(v);
                    v.setInsAttr("minDistance", alt);
                    v.setInsAttr("previous", u);
                    System.out.println("   Add to queue:" + v.getDataID());
                    vertexQueue.add(v);
                }
                if (v.equals(dest)) {
                    ArrayList<DmGraphVertex> retc = new ArrayList<DmGraphVertex>();
                    while(v != null) {
                        retc.add(v);
                        v = v.getInsAttr("previous", null);
                    }
                    
                    System.out.println("End, found!");
                    Collections.reverse(retc);
                    return retc; 
                }
            }
        }

        System.out.println("End, not found");
        return null;
    }
}
