/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.graph.bo;

import org.fm.bo.BoObject;
import org.fm.graph.model.DmGraph;
import org.fm.graph.model.DmGraphEdge;
import org.fm.graph.model.DmGraphVertex;

public interface BoGraph extends BoObject {

    public <G extends DmGraph> G getGraph(Class<G> cls,String id);
    public <V extends DmGraphVertex> V getVertex(Class<V> cls, String id);
    public <E extends DmGraphEdge> E getEdge(Class<E> cls,String id);

    public <E extends DmGraphEdge,V extends DmGraphVertex> 
            void loadVertexEdges(Class<E> cls,V vtx);
}
