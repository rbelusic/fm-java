package org.fm.graph.model;

import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
public interface DmGraphEdge extends DmObject {
    
    public String getId(String def);

    public void setId(String def);

    public String getGraphId(String def);

    public void setGraphId(String def);

    public String getFromNode(String def);

    public void setFromNode(String def);

    public String getToNode(String def);

    public void setToNode(String def);

    public Integer getDirection(Integer def);

    public void setDirection(Integer def);

    public Double getVeight(Double def);

    public void setVeight(Double def);
}
