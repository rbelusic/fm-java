package org.fm.graph.model;

import java.util.List;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmAttributeDef;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
public interface DmGraphVertex extends DmObject {

    public String getId(String def);

    public void setId(String def);

    public String getGraphId(String def);

    public void setGraphId(String def);
    
    @DmAttributeDef(getter = "org.fm.graph.Graph.getVertexEdges")
    public <E extends DmGraphEdge> List<E> getEdges(List<E> def);
    
    public <E extends DmGraphEdge> void setEdges(List<E> def);

    @DmAttributeDef(getter = "org.fm.graph.Graph.getVertexExits")
    public <E extends DmGraphEdge> List<E> getExits(List<E> def);

    @DmAttributeDef(getter = "org.fm.graph.Graph.getVertexEntrances")
    public <E extends DmGraphEdge> List<E> getEntrances(List<E> def);
}
