package org.fm.graph.model;

import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
public interface DmGraph extends DmObject{
   public String getId(String def);
   public void setId(String def);
    
    
}
