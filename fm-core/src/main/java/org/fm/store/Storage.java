package org.fm.store;

import java.lang.annotation.Annotation;
import javax.persistence.Table;
import org.fm.FmException;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;

/**
 *
 * @author RobertoB
 */
public class Storage {
    public static <T extends StorageConnection> StorageConnection instance(Class<T>provider) {
        try {
            return provider.newInstance();
        } catch (InstantiationException ex) {
            throw new FmException("FEM001", "Unable to create driver instance.", ex);            
        } catch (IllegalAccessException ex) {
            throw new FmException("FEM001", "Unable to create driver instance.", ex);
        }
    }
    
    public static <T extends DmObject> String getDbCollectionName(Class<T> dmObjectClass) {

        Annotation annStoreCol = DmFactory.getDmAnnotation(dmObjectClass, Table.class);
        if (!(annStoreCol instanceof Table)) {
            return "";
        } else {
            return ((Table) annStoreCol).name();
        }
    }
    
}
