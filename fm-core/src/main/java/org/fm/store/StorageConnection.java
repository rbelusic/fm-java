package org.fm.store;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fm.dm.DmObject;

public interface StorageConnection {

    public void dispose();

    public <T extends DmObject> Object serializeDmObject(T dm);

    public <T extends DmObject, A extends Object> T deserializeDmObject(Class<T> dmObjectClass, Map<String, Object> propSet);
    public <T extends DmObject, A extends Object> T deserializeDmObject(T dm, Map<String, Object> propSet);

    // CRUD
    public <T extends DmObject> T get(Class<T> dmObjectClass, Object id);

    public <T extends DmObject> T get(T dmObject, Object id);

    public <T extends DmObject> List<T> get(Class<T> dmObjectClass, Object[] ids);

    public <T extends DmObject> void put(T dm);

    public <T extends DmObject> void put(T[] dm);

    public <T extends DmObject> void update(T dm, Set<String> attrs);

    public <T extends DmObject> void update(T[] dm, Set<String> attrs);

    public <T extends DmObject> void delete(Class<T> dmObjectClass, Object id);

    public <T extends DmObject> void delete(Class<T> dmObjectClass, Object[] id);

    public <T extends DmObject> void delete(T dm);

    public <T extends DmObject> void delete(T[] dm);

    public <T extends DmObject> List<T> find(
            Class<T> dmClass, String query, Map<String, Object> args
    );

    public <T extends DmObject> List<T> findBy(
            Class<T> dmObjectClass, String attr, Object value
    );
    
    public void execute(String sql, Map<String, Object> params);
}
