package org.fm.store.impl.jdbc;

import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fm.FmException;
import org.fm.application.FmContext;

/**
 *
 * @author rbelusic
 */
public final class JdbcDataStoreQuery {
    private final static Logger LOG = FmContext.getLogger(JdbcDataStoreConnection.class);

    private final static Pattern regexPattern = Pattern.compile("\\[:(.*?)\\]");
    private Connection con;
    private String query;
    private Map<String, Object> args;
    private ResultSet resultSet;
    private int execResult;
    
    private String queryBindStr;
    private ArrayList<String> queryBindVars;
    private PreparedStatement preparedStmt;

    public static final int FETCH = 1;
    public static final int UPDATE = 2;
    public static final int EXECUTE = 3;
    
    public static JdbcDataStoreQuery fetch(Connection con, String q, Map<String, Object> args) {
        JdbcDataStoreQuery jdbcQuery = new JdbcDataStoreQuery(con, q, args, FETCH);
        return jdbcQuery;
    }

    public static JdbcDataStoreQuery update(Connection con, String q, Map<String, Object> args) {
        JdbcDataStoreQuery jdbcQuery = new JdbcDataStoreQuery(con, q, args, UPDATE);
        return jdbcQuery;
    }

    public static JdbcDataStoreQuery execute(Connection con, String q, Map<String, Object> args) {
        JdbcDataStoreQuery jdbcQuery = new JdbcDataStoreQuery(con, q, args, EXECUTE);
        return jdbcQuery;
    }


    private JdbcDataStoreQuery(Connection con, String q, Map<String, Object> args, int sqlOper) {
        this.con = con;
        this.query = q;
        this.args = args;
        this.resultSet = null;

        LOG.log(Level.INFO, "Executing SQL: ["+ q + "]");
        LOG.log(Level.INFO, " binds: " + args);
        templateToJdbcQuery();

        if (con == null || this.preparedStmt == null) {
            throw new FmException("FEM003", "No connection");
        }

        Map<String, Object> dmattrs = args;

        // bind
        try {
            int pos = 1;
            for (int i = 0; i < this.queryBindVars.size(); i++) {
                String attrName = this.queryBindVars.get(i);
                Object attrValue = dmattrs.get(attrName);
                if (attrValue != null && attrValue instanceof List) {
                    for (Object itm : (List) attrValue) {
                        this.preparedStmt.setObject(pos, itm);
                        pos++;
                    }
                } else if (attrValue != null && attrValue.getClass().isArray()) {
                    for (Object itm : (Object[]) attrValue) {
                        this.preparedStmt.setObject(pos, itm);
                        pos++;
                    }
                } else {
                    this.preparedStmt.setObject(pos, attrValue);
                    pos++;
                }
            }

            // fetch
            if(sqlOper == FETCH) {
                resultSet = this.preparedStmt.executeQuery();
            } else if(sqlOper == UPDATE) {
                execResult = this.preparedStmt.executeUpdate();
            } else if(sqlOper == EXECUTE) {
                this.preparedStmt.execute();                
            } else {
                throw new FmException("FEM003", "Unknown SQL operation.");
            } 
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new FmException("FEM003", "Execute SQL query error.", ex);
        }
    }
    

    public Map<String, Object> getNextResult() {
        try {
            if (resultSet == null || !resultSet.next()) {
                return null;
            }
            ResultSetMetaData emd = resultSet.getMetaData();
            HashMap<String, Object> propSet = new HashMap<String, Object>();

            for (int f = 1; f <= emd.getColumnCount(); f++) {
                // TODO: check types
                
                Object rsValue=null;
                try {
                    rsValue = resultSet.getObject(f);
                } catch(Exception e) {
                }
                propSet.put(emd.getColumnName(f), jdbcTypeConvert(rsValue));
            }
            
            return propSet;
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            throw new FmException("FEM003", "Store access error", ex);
        }
    }

    public void close() {
        if (this.resultSet != null) {
            try {
                this.resultSet.close();
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
            this.resultSet = null;
        }
        if (this.preparedStmt != null) {
            try {
                this.preparedStmt.close();
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
            this.preparedStmt = null;
        }
        this.con = null;
    }


    
    public void fetch() {
        if (con == null || this.preparedStmt == null) {
            throw new FmException("FEM003", "No connection");
        }

        Map<String, Object> dmattrs = args;

        // bind
        try {
            int pos = 1;
            for (int i = 0; i < this.queryBindVars.size(); i++) {
                String attrName = this.queryBindVars.get(i);
                Object attrValue = dmattrs.get(attrName);
                if (attrValue != null && attrValue instanceof List) {
                    for (Object itm : (List) attrValue) {
                        this.preparedStmt.setObject(pos, itm);
                        pos++;
                    }
                } else if (attrValue != null && attrValue.getClass().isArray()) {
                    for (Object itm : (Object[]) attrValue) {
                        this.preparedStmt.setObject(pos, itm);
                        pos++;
                    }
                } else {
                    this.preparedStmt.setObject(pos, attrValue);
                    pos++;
                }
            }

            // fetch
            resultSet = this.preparedStmt.executeQuery();
        } catch (SQLException ex) {
            throw new FmException("FEM003", "Execute SQL query error.", ex);
        }
    }

    /**
     * Process template str, createpreparedstatement (with ? in query str)and
     * list of params
     *
     * @param templateStr
     * @param dmattrs
     * @throws SQLException
     */
    private void templateToJdbcQuery() {
        String templateStr = this.query;
        Map<String, Object> dmattrs = this.args;

        ArrayList<String> newVars = new ArrayList<String>();
        String jdbcQueryStr = "";
        int last = 0;

        Matcher m = regexPattern.matcher(templateStr);

        while (m.find()) {
            String bv = m.group(1);
            int mstart = m.start(1) - 2;
            int mend = m.end(1) + 1;

            newVars.add(bv);
            Object val = dmattrs.get(bv);
            jdbcQueryStr += templateStr.substring(last, mstart);
            if (val != null && val instanceof List) {
                for (int i = 0; i < ((List) val).size(); i++) {
                    jdbcQueryStr += (i == 0 ? "?" : ",?");
                }
            } else if (val != null && val.getClass().isArray()) {
                for (int i = 0; i < ((Object[]) val).length; i++) {
                    jdbcQueryStr += (i == 0 ? "?" : ",?");
                }
            } else {
                jdbcQueryStr += "?";
            }
            last = mend;
        }
        jdbcQueryStr += templateStr.substring(last);

        this.queryBindStr = jdbcQueryStr;
        this.queryBindVars = newVars;
        try {
            this.preparedStmt = con.prepareStatement(this.queryBindStr);
        } catch (SQLException ex) {
            throw new FmException("FEM003", "Error preparing SQL statement:" + ex.getMessage(), ex);
        }
    }

    public static void dump(Map<String,Object>  m) {
        LOG.info("dump:\n");
        for(Map.Entry mentry: m.entrySet()){
            LOG.info(String.format(
                    "%30s: %s\n",
                    mentry.getKey(),
                    mentry.getValue()
                ));
            
        }
    }

    private Object jdbcTypeConvert(Object jval) {
        try {

            if (jval == null) {
                return jval;
            } else if (jval instanceof java.sql.Date) {
                return new Date(((java.sql.Date) jval).getTime());
            } else if (jval instanceof java.sql.Timestamp) {
                return new Date(((java.sql.Timestamp) jval).getTime());
            } else if (jval instanceof java.sql.Time) {
                return new Date(((java.sql.Time) jval).getTime());
            } else if (jval instanceof java.sql.Array) {
                return arrayToArray((java.sql.Array) jval);
            } else if (jval instanceof java.sql.Clob) {
                return clobToString((java.sql.Clob) jval);
            } else if (jval instanceof java.sql.Blob) {
                return blobToByteArray((java.sql.Blob) jval);
            }

            if (jval.getClass().getName().startsWith("java.sql.")) {
                // System.err.println("Warning! SQL type -> " + jval.getClass().getName());
            }
            
            return jval;
        } catch (Exception ex) {
            return null;
            //throw new FmException("FEM003", "Error converting fetched values.", ex);
        }
    }

    private Object blobToByteArray(java.sql.Blob data) throws Exception {
        // return clob data, size is too big
        if (data.length() > Integer.MAX_VALUE) {
            return data;
        }
        return data.getBytes(1, (int) data.length());
    }

    private Object clobToString(java.sql.Clob data) throws Exception {
        // return clob data, size is too big
        if (data.length() > Integer.MAX_VALUE) {
            return data;
        }

        final StringBuilder sb = new StringBuilder();

        try {
            final Reader reader = data.getCharacterStream();
            final BufferedReader br = new BufferedReader(reader);

            int b;
            while (-1 != (b = br.read())) {
                sb.append((char) b);
            }

            br.close();
            return sb.toString();
        } catch (Exception e) {            
            throw e;
        }
    }

    private Object arrayToArray(java.sql.Array array) throws Exception {
        if (array == null) {
            return array;
        }

        Object[] arr = (Object[]) array.getArray();
        ArrayList<Object> al = new ArrayList<Object>();

        for (Object v : arr) {
            al.add(jdbcTypeConvert(v));
        }
        return al.toArray();
    }
}
