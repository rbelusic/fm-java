package org.fm.store.impl.jdbc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Transient;
import org.fm.FmException;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.dm.DmAttribute;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.store.Storage;
import org.fm.store.StorageConnection;

public class JdbcDataStoreConnection implements StorageConnection {
    private final static Logger LOG = FmContext.getLogger(JdbcDataStoreConnection.class);
    /*
     fm.store.jdbc.driver.names.caseSensitive=false
     fm.store.jdbc.driver.names.case=upper
     */

    private static Connection jdbcStoreDbConnection = null;
    private boolean jdbcNamesIgnoreCase;
    private boolean jdbcNamesUpperCase;

    public JdbcDataStoreConnection() {
        configure();
    }

    private <V> boolean configure() {
        FmApplication app = FmApplication.instance();
        if (app == null) {
            throw new FmException("FEM001", "Unable to connect: configuration not found.");
        }

        try {
            Class<Driver> cls = (Class<Driver>) Class.forName(
                    app.getApplicationProperty("fm.store.jdbc.driver")
            );

            Driver drv = cls.newInstance();

            Properties conProps = new Properties();
            conProps.setProperty("user", app.getApplicationProperty("fm.store.jdbc.username"));
            conProps.setProperty("password", app.getApplicationProperty("fm.store.jdbc.password"));
            jdbcStoreDbConnection = drv.connect(
                    app.getApplicationProperty("fm.store.jdbc.driver.url"),
                    conProps
            );
            jdbcNamesIgnoreCase = app.getApplicationProperty(
                    "fm.store.jdbc.driver.names.caseSensitive"
            ).equals("false");
            jdbcNamesUpperCase = app.getApplicationProperty(
                    "fm.store.jdbc.driver.names.case"
            ).equalsIgnoreCase("upper");
            getDriverId();

        } catch (Exception ex) {
            throw new FmException(
                    "FEM001",
                    "Unable to connect with[" + app.getApplicationProperty("fm.store.jdbc.driver.url") + "] driver.", ex);
        }
        return jdbcStoreDbConnection != null;
    }

    @Override
    public void dispose() {
        if (jdbcStoreDbConnection != null) {
            try {
                jdbcStoreDbConnection.close();
            } catch (SQLException ex) {
                LOG.log(Level.WARNING, null, ex);
            }
            jdbcStoreDbConnection = null;
        }
    }

    @Override
    public <T extends DmObject> Object serializeDmObject(T dm) {
        return dm.getAttr();
    }

    @Override
    public <T extends DmObject, A extends Object> T deserializeDmObject(Class<T> dmObjectClass, Map<String, Object> propSet) {
        // namjerno se ignorira exc radi testa
        return DmFactory.create(dmObjectClass, propSet);
    }

    @Override
    public <T extends DmObject, A extends Object> T deserializeDmObject(T dm, Map<String, Object> propSet) {
        // namjerno se ignorira exc radi testa
        for (Map.Entry<String, Object> prop : propSet.entrySet()) {
            dm.setAttr(prop.getKey(), prop.getValue());
        }
        return dm;
    }

    private <T> T convertTo(Class<T> cls, Object value) {
        try {
            // null
            if(value == null || cls == null) return null;
            
            
            if(cls.isAssignableFrom(value.getClass())) return (T) value;

            // dest i cls su inst of Number
            if((value instanceof Number) && Number.class.isAssignableFrom(cls)) {
                Number num = (Number)value;
                if(Double.class.equals(cls)) {
                    return (T) new Double(num.doubleValue());
                } else if(Float.class.equals(cls)) {
                    return (T) new Float(num.floatValue());
                } else if(Long.class.equals(cls)) {
                    return (T) new Long(num.longValue());
                } else if(Integer.class.equals(cls)) {
                    return (T) new Integer(num.intValue());
                } else if(Short.class.equals(cls)) {
                    return (T) new Short(num.shortValue());
                } else if(Byte.class.equals(cls)) {
                    return (T) new Byte(num.byteValue());
                } else if(BigInteger.class.equals(cls)) {
                    return (T) BigInteger.valueOf(num.longValue());
                } else if(BigDecimal.class.equals(cls)) {
                    return (T) BigDecimal.valueOf(num.doubleValue());
                } else if(AtomicLong.class.equals(cls)) {
                    return (T) new AtomicLong(num.longValue());
                } else if(AtomicInteger.class.equals(cls)) {
                    return (T) new AtomicInteger(num.intValue());
                }                
            }

            // date
            if(cls.isAssignableFrom(Date.class)) {
                if(value instanceof Number) {
                    return (T) new Date(((Number)value).longValue());
                } else {
                    return (T) new Date(value.toString());
                }
            } 
            
            // try string constructor
            if(cls.getConstructor(String.class) != null){
                return cls.getConstructor(String.class).newInstance(new Object[] { value.toString() });
            }
            
            // try cast
            return (T) value;
        } catch (Exception ex) {
            LOG.log(Level.WARNING, "Error converting " + value + " to " + cls.getSimpleName(),ex);            
            return null;
        }
    }
    
    private <T extends DmObject> T entityToDmObject(Map<String, Object> e, Class<T> dmObjectClass) {

        Map<String, Object> dmProps = new HashMap<String, Object>();

        //translate all to lowrcase 
        HashMap<String, Object> eLc = new HashMap();
        for (Map.Entry<String, Object> me : e.entrySet()) {
            eLc.put(me.getKey().toLowerCase(), me.getValue());
        }
        e = eLc;

        for (DmAttribute attr : DmFactory.getDmClassAttributes((Class<DmObject>) dmObjectClass).values()) {
            String colName = getAttrColumnName(attr, "get");
            dmProps.put(attr.getName(), convertTo(attr.getType(),e.get(colName.toLowerCase())));
        }

        T dm = deserializeDmObject(dmObjectClass, dmProps);
        dm.setInsAttr("fetched", true);
        return dm;
    }

    private <T extends DmObject> void entityToDmObject(Map<String, Object> e, T dm) {
        Map<String, Object> dmProps = new HashMap<String, Object>();

        //translate all to lowrcase 
        HashMap<String, Object> eLc = new HashMap();
        for (Map.Entry<String, Object> me : e.entrySet()) {
            eLc.put(me.getKey().toLowerCase(), me.getValue());
        }
        e = eLc;

        for (DmAttribute attr : DmFactory.getDmClassAttributes((Class<DmObject>) dm.getSubClass()).values()) {
            String colName = getAttrColumnName(attr, "get");
            dmProps.put(attr.getName(), convertTo(attr.getType(),e.get(colName.toLowerCase())));
        }

        deserializeDmObject(dm, dmProps);
        dm.setInsAttr("fetched", true);
    }

    /**
     *
     * @param <T>
     * @param dm
     * @param id
     * @return
     * @throws FmException
     */
    @Override
    public <T extends DmObject> T get(T dm, Object id) {
        if (id == null || (id instanceof String && id.toString().isEmpty())) {
            return null;
        }

        DmAttribute attrdef = DmFactory.getDmAttributeMetadata(
                dm.getSubClass(), DmFactory.getDmDataIdAttribute(dm.getSubClass())
        );
        String columnName = getAttrColumnName(attrdef, "get");

        String qry = "SELECT * FROM " + getDmTableName(dm.getSubClass())
                + "  WHERE " + columnName
                + " = [:id]";

        HashMap params = new HashMap();
        params.put("id", id);

        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.fetch(_getConnection(), qry, params);
        Map<String, Object> rslt = jQuery.getNextResult();
        jQuery.close();

        if (rslt == null) {
            return null;
        }
        this.entityToDmObject(rslt, dm);
        return dm;
    }

    /**
     *
     * @param <T>
     * @param dmObjectClass
     * @param id
     * @return
     */
    @Override
    public <T extends DmObject> T get(Class<T> dmObjectClass, Object id) {
        return get(DmFactory.create(dmObjectClass), id);
    }

    @Override
    public <T extends DmObject> List<T> get(Class<T> dmObjectClass, Object[] ids) {
        List<T> results = new ArrayList<T>();

        if (ids == null || ids.length == 0) {
            return results;
        }

        List<Object> l = new ArrayList<Object>();
        l.addAll(Arrays.asList(ids));
        HashMap params = new HashMap();
        params.put("ids", l);

        DmAttribute attrdef = DmFactory.getDmAttributeMetadata(
                dmObjectClass, DmFactory.getDmDataIdAttribute(dmObjectClass)
        );
        String columnName = getAttrColumnName(attrdef, "get");

        String qry = "SELECT * FROM " + getDmTableName(dmObjectClass)
                + "  WHERE " + columnName + " IN ([:ids])";

        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.fetch(_getConnection(), qry, params);

        Map<String, Object> row;
        while ((row = jQuery.getNextResult()) != null) {
            results.add(this.entityToDmObject(row, dmObjectClass));
        }
        jQuery.close();
        return results;
    }

    @Override
    public <T extends DmObject> void update(T[] dms, Set<String> attrs) {
        for (T dm : dms) {
            update(dm, attrs);
        }
    }

    @Override
    public <T extends DmObject> void update(T dm, Set<String> attrs) {
        String qry = "UPDATE " + getDmTableName(dm.getSubClass()) + " SET ";
        String flds = "";
        Map<String, Object> params = new HashMap<String, Object>();

        if (attrs == null) {
            attrs = dm.getAttributeNames();
        }
        for (String attr : dm.getAttributeNames()) {
            if (!attrs.contains(attr)) {
                continue;
            }

            DmAttribute attrdef = DmFactory.getDmAttributeMetadata(dm.getSubClass(), attr);

            Transient transAnn = attrdef.getMethodAnnotation("set" + attrdef.getName(), Transient.class);
            if (transAnn == null) {
                String columnName = getAttrColumnName(attrdef, "set");
                flds += (flds.isEmpty() ? "" : ", ") + columnName + " = [:" + attr + "]";
                params.put(attr, dm.getAttr(attr, null));
            }
        }
        DmAttribute attrdef = DmFactory.getDmAttributeMetadata(dm.getSubClass(), dm.getDataIDAttribute());
        String columnName = getAttrColumnName(attrdef, "set");
        qry += flds + " WHERE " + columnName + " = [:" + dm.getDataIDAttribute() + "]";

        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.update(_getConnection(), qry, params);
        jQuery.close();
    }

    private String getAttrColumnName(DmAttribute attrdef, String method) {
        Column colAnn = attrdef.getMethodAnnotation(method + attrdef.getName(), Column.class);
        String name = colAnn != null && colAnn.name() != null && !colAnn.name().isEmpty() ? colAnn.name() : attrdef.getName();
        return jdbcNamesIgnoreCase ? (jdbcNamesUpperCase ? name.toUpperCase() : name.toLowerCase()) : name;
    }

    private <T extends DmObject> String getDmTableName(Class<T> dmObjectClass) {
        String name = Storage.getDbCollectionName(dmObjectClass);
        return jdbcNamesIgnoreCase ? (jdbcNamesUpperCase ? name.toUpperCase() : name.toLowerCase()) : name;
    }

    @Override
    public <T extends DmObject> void put(T[] dms) {
        for (T dm : dms) {
            put(dm);
        }
    }

    @Override
    public <T extends DmObject> void put(T dm) {
        String attrList = "";
        String valList = "";

        for (String attr : dm.getAttributeNames()) {
            DmAttribute attrdef = DmFactory.getDmAttributeMetadata(dm.getSubClass(), attr);

            Transient transAnn = attrdef.getMethodAnnotation("set" + attrdef.getName(), Transient.class);
            if (attrdef.isSetMehodDefined() && transAnn == null) {
                String columnName = getAttrColumnName(attrdef, "set");
                attrList += (attrList.isEmpty() ? "" : ",") + columnName;
                valList += (valList.isEmpty() ? "" : ",") + "[:" + attr + "]";
            }
        }
        String qry = "INSERT INTO  " + getDmTableName(dm.getSubClass())
                + "(" + attrList + ") values (" + valList + ")";

        Map<String, Object> params = dm.getAttr();
        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.update(_getConnection(), qry, params);
        jQuery.close();
    }

    @Override
    public <T extends DmObject> void delete(Class<T> dmObjectClass, Object id) {
        Object[] ids = new Object[]{id};
        delete(dmObjectClass, ids);
    }

    @Override
    public <T extends DmObject> void delete(Class<T> dmObjectClass, Object[] ids) {
        if (ids == null || ids.length == 0) {
            return;
        }

        DmAttribute attrdef = DmFactory.getDmAttributeMetadata(
                dmObjectClass, DmFactory.getDmDataIdAttribute(dmObjectClass)
        );
        String columnName = getAttrColumnName(attrdef, "put");

        String qry = "DELETE FROM " + getDmTableName(dmObjectClass)
                + " WHERE " + columnName
                + " IN ([:ids])";
        HashMap params = new HashMap();
        params.put("ids", ids);

        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.update(_getConnection(), qry, params);
        jQuery.close();
    }

    @Override
    public <T extends DmObject> void delete(T dm) {
        delete(dm.getSubClass(), dm.getDataID());
    }

    @Override
    public <T extends DmObject> void delete(T[] dms) {
        if (dms.length < 1) {
            return;
        }

        ArrayList<String> ids = new ArrayList();
        for (T dm : dms) {
            ids.add(dm.getDataID());
        }
        delete(dms[0].getSubClass(), ids.toArray(new String[ids.size()]));
    }

    @Override
    public <T extends DmObject> ArrayList<T> findBy(
            Class<T> dmObjectClass, String attr, Object value
    ) {
        ArrayList<T> results = new ArrayList<T>();
        DmAttribute attrdef = DmFactory.getDmAttributeMetadata(
                dmObjectClass, attr
        );
        String columnName = getAttrColumnName(attrdef, "get");

        String qry = "SELECT * FROM " + getDmTableName(dmObjectClass)
                + "  WHERE " + columnName + " = [:value]";

        HashMap params = new HashMap();
        params.put("value", value);

        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.fetch(_getConnection(), qry, params);
        Map<String, Object> row;
        while ((row = jQuery.getNextResult()) != null) {
            results.add(this.entityToDmObject(row, dmObjectClass));
        }
        jQuery.close();

        return results;
    }

    @Override
    public <T extends DmObject> ArrayList<T> find(
            Class<T> dmObjectClass, String query, Map<String, Object> params
    ) {
        String q = query.endsWith(".sql")
                ? loadTemplate(FmApplication.instance().getConfigProperty(this.getClass().getPackage().getName() + ".templates"), getDriverId(), query)
                : query;

        ArrayList<T> results = new ArrayList<T>();

        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.fetch(_getConnection(), q, params);

        Map<String, Object> row;
        while ((row = jQuery.getNextResult()) != null) {
            results.add(this.entityToDmObject(row, dmObjectClass));
        }
        jQuery.close();
        return results;
    }

    @Override
    public void execute(String sql, Map<String, Object> params) {
        String q = sql.endsWith(".sql")
            ? loadTemplate(FmApplication.instance().getConfigProperty(this.getClass().getPackage().getName() + ".templates"), getDriverId(), sql)
            : sql;
        JdbcDataStoreQuery jQuery = JdbcDataStoreQuery.execute(_getConnection(), q, params);
        jQuery.close();
    }

    
    /**
     * @return the jdbc connection
     */
    public static Connection _getConnection() {
        return jdbcStoreDbConnection;
    }

    public static <T extends DmObject> String getAttrNameFromColumnName(String cname, Class<T> dmObjectClass) {
        DmAttribute attr = DmFactory.getDmAttributeMetadata((Class<DmObject>) dmObjectClass, cname, true);
        return (attr != null ? attr.getName() : cname);
    }

    public enum SQL_VARIANT {

        DERBY, MYSQL
    }

    public SQL_VARIANT getDriverId() {
        try {
            return jdbcStoreDbConnection.getMetaData().getDatabaseProductName().contains("Derby")
                    ? SQL_VARIANT.DERBY : SQL_VARIANT.MYSQL;
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return SQL_VARIANT.MYSQL;
    }

    private final static Map<String, String> TEMPLATES = new HashMap<String, String>();

    public static String loadTemplate(String pkg, SQL_VARIANT variant, String fname) {

        String path = pkg.replace('.', '/') + "/" + fname;
        String pathVariant = pkg.replace('.', '/') + "/" + variant + "/" + fname;
        if (TEMPLATES.containsValue(path)) {
            return TEMPLATES.get(path);
        }
        // org\fm\app\geoevent\dao\impl\templates
        // org/fm/store/impl/jdbc/templates/dao-events-find.sql

        InputStream input = null;
        try {
            input = new FileInputStream(pathVariant);
        } catch (FileNotFoundException ex) {
            input = JdbcDataStoreConnection.class.getClassLoader().getResourceAsStream(pathVariant);
        }

        if (input == null) {
            try {
                input = new FileInputStream(path);
            } catch (FileNotFoundException ex) {
                input = JdbcDataStoreConnection.class.getClassLoader().getResourceAsStream(path);
            }

        }

        try {
            final char[] buffer = new char[1024];
            final StringBuilder out = new StringBuilder();
            try {
                Reader in = new InputStreamReader(input, "UTF-8");
                for (;;) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0) {
                        break;
                    }
                    out.append(buffer, 0, rsz);
                }
                input.close();
                return out.toString();
            } catch (Exception ex) {
                input.close();
                throw new FmException("FE0001", "Error loading properties from [" + path + "].", ex);
            }
        } catch (Exception e) {
            throw new FmException("FE0001", "Error loading properties from [" + path + "].", e);
        }
    }
}
