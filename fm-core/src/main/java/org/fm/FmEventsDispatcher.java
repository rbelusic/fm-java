package org.fm;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rbelusic
 */
public class FmEventsDispatcher extends FmObjectImpl implements FmDispatcher {
    private List<Object> listeners;
    private Object owner;

    public FmEventsDispatcher() {
        owner = null;
        listeners = new ArrayList<Object>();
    }

    public FmEventsDispatcher(Object o) {
        owner = o;
        listeners = new ArrayList<Object>();
    }
    
    @Override
    public <T> boolean addListener(T receiver) {
        return listeners.contains(receiver) ? false : listeners.add(receiver);
    }

    @Override
    public <T> boolean removeListener(T receiver) {
        return listeners.remove(receiver);
    }

    @Override
    public void removeAllListeners() {
        listeners.clear();
    }

    @Override 
    public <T> void fireEvent(String ev, T evData) {
        if (getOwner() == null) {
            return;
        }
        List<T> lstarr = getListeners();
        
        for (T l : lstarr) {
            fireEvent(getOwner(), l, ev, evData);
        }
    }

    public int getListenersCount() {
        return listeners.size();
    }

    public <T> List<T> getListeners() {
        return new ArrayList(listeners);
    }

    public <T> boolean isListener(T receiver) {
        return listeners.contains(receiver);
    }

    /**
     * @return the owner
     */
    public Object getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Object owner) {
        this.owner = owner;
    }

    
    public static <T> boolean fireEvent(Object owner, Object receiver, String ev, T evData) {
        if (owner == null || receiver == null) {
            return false;
        }

        Method method;
        Class[] args = {Object.class, Object.class};

        method = null;
        try {
            method = receiver.getClass().getMethod(ev, args);
        } catch (Exception e) {
            // Logger.getLogger(FmEventsDispatcher.class.getName()).log(Level.SEVERE, null, e);
        }
        if (method != null) {
            try {
                method.invoke(receiver, new Object[]{owner, evData});
            } catch (Exception ex) {
                //Logger.getLogger(FmEventsDispatcher.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                return false;
            }
            return true;
        }

        return true;
    }
}
