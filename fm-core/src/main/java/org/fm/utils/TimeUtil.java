package org.fm.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/* sve int se vraca u sekundama */
public class TimeUtil {

    public static final long ONE_DAY = 1000 * 60 * 60 * 24;
    public static final long ONE_HOUR = 1000 * 60 * 60;
    public static final long ONE_MINUTE = 1000 * 60;
    public static final long ONE_SEC = 1000;

    /* zonsko vrijeme u UTC */
    public static Date toUTC(TimeZone tz, Date in) {
        return (new Date(
                (in.getTime() / 1000
                - TimeUtil.getUTCOffset(tz)
                - (TimeUtil.inDaylightTime(in) ? TimeUtil.getDSTOffset(tz) : 0)) * 1000));
    }

    public static Date toUTC(String zone, Date in) {
        return (TimeUtil.toUTC(TimeZone.getTimeZone(zone), in));
    }

    public static Date toUTC(Date in) {
        return (TimeUtil.toUTC(TimeZone.getDefault(), in));
    }

    /* UTC vrijeme u zonsko */
    public static Date fromUTC(TimeZone tz, Date in) {
        return (new Date(
                (in.getTime() / 1000
                + TimeUtil.getUTCOffset(tz)
                + (TimeUtil.inDaylightTime(in) ? TimeUtil.getDSTOffset(tz) : 0)) * 1000));
    }

    public static Date fromUTC(String zone, Date in) {
        return (TimeUtil.fromUTC(TimeZone.getTimeZone(zone), in));
    }

    public static Date fromUTC(Date in) {
        return (TimeUtil.fromUTC(TimeZone.getDefault(), in));
    }

    // vraca razliku od UTC-a za trazenu zonu
    public static int getUTCOffset(TimeZone tz) {
        return (tz.getRawOffset() / 1000);
    }

    public static int getUTCOffset(String zone) {
        return (TimeUtil.getUTCOffset(TimeZone.getTimeZone(zone)));
    }

    public static int getUTCOffset() {
        return (TimeUtil.getUTCOffset(TimeZone.getDefault()));
    }

    // vraca DST za trazenu zonu
    public static int getDSTOffset(TimeZone tz) {
        return (tz.getDSTSavings() / 1000);
    }

    public static int getDSTOffset(String zone) {
        return (TimeUtil.getDSTOffset(TimeZone.getTimeZone(zone)));
    }

    public static int getDSTOffset() {
        return (TimeUtil.getDSTOffset(TimeZone.getDefault()));
    }

    // da li je DST u primjeni u toj zoni    
    public static boolean useDaylightTime(TimeZone tz) {
        return (tz.useDaylightTime());
    }

    public static boolean useDaylightTime(String zone) {
        return (TimeUtil.useDaylightTime(TimeZone.getTimeZone(zone)));
    }

    public static boolean useDaylightTime() {
        return (TimeUtil.useDaylightTime(TimeZone.getDefault()));
    }

    // vraca dali je datum u DST-u
    public static boolean inDaylightTime(TimeZone tz, Date date) {
        return (tz.inDaylightTime(date));
    }

    public static boolean inDaylightTime(String zone, Date date) {
        return (TimeUtil.inDaylightTime(TimeZone.getTimeZone(zone), date));
    }

    public static boolean inDaylightTime(Date date) {
        return (TimeUtil.inDaylightTime(TimeZone.getDefault(), date));
    }

    
    // util
    public static int daysBetween(Date d1, Date d2) {
        if(d1 == null || d2 == null) return 0;
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();        
        cal1.setTime(d1);
        cal2.setTime(d2);
        return cal2.get(Calendar.DAY_OF_YEAR) - cal1.get(Calendar.DAY_OF_YEAR);  
    }
    
    public static String strTimeBetween(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            return "";
        }

        long dif;
        String ret;

        // Convert both dates to milliseconds
        long date1_ms = d1.getTime();
        long date2_ms = d2.getTime();

        // Calculate the difference in milliseconds
        long difference_ms = Math.abs(date1_ms - date2_ms);

        // ONE_SEC
        dif = Math.round(difference_ms / ONE_SEC);

        if (dif < 60) {
            ret = date1_ms < date2_ms
                    ? dif + " seconds ago"
                    : "In " + dif + " seconds";
        } else { // ONE_MINUTE
            dif = Math.round(difference_ms / ONE_MINUTE);
            if (dif < 60) {
                ret = date1_ms < date2_ms
                        ? dif + " minutes ago"
                        : "In " + dif + " minutes";
            } else { // ONE_HOUR
                dif = Math.round(difference_ms / ONE_HOUR);
                if (dif < 24) {
                    ret = date1_ms < date2_ms ? dif + " hours ago" : "In " + dif + " hours";
                } else { // ONE_DAY
                    dif = Math.round(difference_ms / ONE_DAY);

                    if (dif == 1) {
                        ret = date1_ms < date2_ms ? "Yesterday" : "Tomorow";
                    } else {
                        ret = date1_ms < date2_ms ? dif + " days ago" : "In " + dif + " days";
                    }
                }
            }
        }

        return (ret);
    }
}
