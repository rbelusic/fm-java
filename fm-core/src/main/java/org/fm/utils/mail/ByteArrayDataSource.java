package org.fm.utils.mail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import javax.activation.DataSource;

public class ByteArrayDataSource implements DataSource {
  private byte[] data;	// data
  private String type;	// content-type
  
  /* Create a DataSource from an input stream */
  public ByteArrayDataSource(InputStream is, String type) throws IOException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    int ch;    

    this.type = type;
    while ((ch = is.read()) != -1) os.write(ch);
    data = os.toByteArray();    
  }
  
  /* Create a DataSource from a byte array */
  public ByteArrayDataSource(byte[] data, String type) {
    this.data = data;
    this.type = type;
  }
  
  /* Create a DataSource from a String */
  public ByteArrayDataSource(String data, String type) {
    try {
      // Assumption that the string contains only ASCII
      // characters!  Otherwise just pass a charset into this
      // constructor and use it in getBytes()
      this.data = data.getBytes("iso-8859-1");
    } catch (UnsupportedEncodingException uex) { }
    this.type = type;
  }

  public ByteArrayDataSource(String data, String type, String charset) 
  throws UnsupportedEncodingException{
    // Assumption that the string contains only ASCII
    // characters!  Otherwise just pass a charset into this
    // constructor and use it in getBytes()
    this.data = data.getBytes(charset);
    this.type = type;
  }
  
  /**
  * Return an InputStream for the data.
  * Note - a new stream must be returned each time.
  */
  public InputStream getInputStream() throws IOException {
    if (data == null)  throw new IOException("No data");
    return new ByteArrayInputStream(data);
  }
  
  public OutputStream getOutputStream() throws IOException {
    throw new IOException("Cannot do this");
  }
  
  public String getContentType() {
    return type;
  }
  
  public String getName() {
    return getClass().getName();
  }
}
