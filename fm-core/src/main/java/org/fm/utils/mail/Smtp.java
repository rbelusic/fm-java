/*
 * U PropertyNode msgParts moraju biti podnodovi za svaki part sa slijedecim attr:
 * SRC_TYPE = file | bytes | stream | text. default text
 * CONTENT_TYPE = napr. text/html, text/plain, image/jpg ... default text/plain
 * SRC_NAME = <ime datoteke> ili null defgault null
 * SRC = Object ovisno o SRC_TYPE:
 *  file,text -> String
 *  bytes -> byte[]
 *  stream -> InputStream
 */
package org.fm.utils.mail;

import java.io.IOException;

import java.net.UnknownHostException;
import org.fm.utils.StringUtil;
import org.fm.utils.TypeUtil;

public class Smtp {
    public final static String X_MAILER = "Laser*MAIL";

    // file I/O
    public final static int ERR_OK = 0;
    public final static int ERR_INVMODE = -1;
    public final static int ERR_NOTFOUND = -2;
    public final static int ERR_ACCESS = -3;
    public final static int ERR_INVHANDLE = -4;
    public final static int ERR_IO = -5;
    public final static int ERR_EOF = -6;
    public final static int ERR_SECURITY = -7;
    public final static int ERR_INVFILEOP = -8;
    public final static int ERR_OUTOFMEM = -9;

    // Dir FN
    public final static int ERR_INVDIR = -100;
    public final static int ERR_INVFILE = -101;

    // TCP/IP
    public final static int ERR_UNKNOWNHOST = -1000;
    public final static int ERR_TIMEOUT = -1001;

    // XML
    public final static int ERR_PARSEXML = -2000;
    public final static int ERR_PARSERINIT = -2001;
    public final static int ERR_XMLTRANSFORM = -2002;

    // GATWRITER
    public final static int ERR_GW_OK = ERR_OK;
    public final static int ERR_GW_ERR = -3000;

    private final static String CRLF =
        new Character((char)13).toString() + new Character((char)10).toString();

    private ConnectionHnd conHnd = null;

    public Smtp() {
    }
    
    // -- novi smtp ------------------------------------------------------------
    public void send(
        String smtpHost,String smtpUser,String smtpPassword,boolean authRequired,
        String fromAddress,String replyAddress,
        String toAddresses,String ccAddresses,String bccAddresses, // comma separated
        String msgSubject,
        String msgText,
        boolean htmlMessage
        ) throws Exception {
        int retc;

        // otvori
        this.open(smtpHost,25,smtpUser,smtpPassword);

        // hello
        retc = this.helo(fromAddress);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[HELO], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        // mail
        retc = this.mail("<" + fromAddress + ">");
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[MAIL], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        // RCPT
        retc = this.rcpt("<" + toAddresses + ">");
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[RCPT], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        if(!TypeUtil.isNullOrNullString(ccAddresses)) {
            Object [] ccadrs = StringUtil.tokenizeString(ccAddresses,',','\"');
            for(int i = 0; i < ccadrs.length; i++) {
                retc = this.rcpt("<" + (String)ccadrs[i] + ">");
                if(retc < ERR_OK || retc >= 400) {
                    this.quit();
                    throw new IOException(
                        "cmd=[RCPT(CC)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                        );
                }
            }
        }

        if(!TypeUtil.isNullOrNullString(bccAddresses)) {
            String [] bcadrs = (String [])StringUtil.tokenizeString(bccAddresses,',','\"');
            for(int i = 0; i < bcadrs.length; i++) {
                retc = this.rcpt("<" + bcadrs[i] + ">");
                if(retc < ERR_OK || retc >= 400) {
                    this.quit();
                    throw new IOException(
                        "cmd=[RCPT(BCC)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                        );
                }
            }
        }


        // data - header
        retc = this.openData();
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(open)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("From: <" + fromAddress + ">" + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(From)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("Reply-To: <" + replyAddress + ">" + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(Reply-To)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("X-Mailer: " + X_MAILER + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(X-Mailer)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("To: <" + toAddresses + ">" + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(To)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        if(!TypeUtil.isNullOrNullString(ccAddresses)) {
            retc = this.writeData("CC: " + ccAddresses + CRLF);
            if(retc < ERR_OK || retc >= 400) {
                this.quit();
                throw new IOException(
                    "cmd=[DATA(CC)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                    );
            }
        }

        if(!TypeUtil.isNullOrNullString(bccAddresses)) {
            retc = this.writeData("BCC: " + bccAddresses + CRLF);
            if(retc < ERR_OK || retc >= 400) {
                this.quit();
                throw new IOException(
                    "cmd=[DATA(BCC)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                    );
            }
        }

        retc = this.writeData("Subject: " + msgSubject + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(Subject)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("MIME-Version: 1.0" + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(MIME-Version)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("Content-Type: "+ (htmlMessage ? "text/html;charset=\"iso-8859-2\"" : "text/plain;charset=\"iso-8859-2\"") + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(Content-Type)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData("Status: 0" + CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(Status)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        retc = this.writeData(CRLF);
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(CRLF)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        // data - poruka
        if(msgText.equals("." + CRLF) || msgText.equals(".")) {
            retc = this.writeData("." + msgText);
        } else {
            retc = this.writeData(msgText);
        }
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(msg)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        // data - kraj
        retc = this.closeData();
        if(retc < ERR_OK || retc >= 400) {
            this.quit();
            throw new IOException(
                "cmd=[DATA(close)], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

        // kraj slanja
        retc = this.quit();
        if(retc < ERR_OK || retc >= 400) {
            throw new IOException(
                "cmd=[QUIT], err=[" + retc + "], msg=[" + this.getSmtpResponse() + "]"
                );
        }

    }

    // -- protocol procs -------------------------------------------------------
    public void open(String host, int port, String user,String password)
    throws IOException {
        try {
            this.conHnd = new ConnectionHnd(host,port);
            this.conHnd.setResponseLine(_read(true));
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
            this.conHnd = null;
            throw new IOException(ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
            this.conHnd = null;
            throw new IOException(ex.getMessage());
        }
    }


    public  int helo(String arg) {
        return(_send("HELO " + arg + Smtp.CRLF,true));
    }

    public int mail(String arg) {
        return(_send("MAIL FROM: " + arg + Smtp.CRLF,true));
    }

    public int rcpt(String arg) {
        return(_send("RCPT TO: " + arg + Smtp.CRLF,true));
    }

    public int openData() {
        return(_send("DATA" + Smtp.CRLF,true));
    }

    public int writeData(String arg) {
        // imamo problem sa slanjem u DATA bloku.
        // nakon odredjenog broja byteova ulijece CRLF (SMTP server?)
        // koji ne mozemo kontrolirati
        // provjereno preko UTL_SMTP-a i ovdje
        // provjereno u kombinacijama laserline/t-com/gmail serverima
        return(_send(arg,false));
    }

    public int closeData() {
        return(_send(Smtp.CRLF + "." + Smtp.CRLF,true));
    }

    public int quit() {
        int retc = _send("QUIT" + Smtp.CRLF,true);
        try {
            this.conHnd.socket.close();
        } catch (IOException ex) { // samo ispisi stacktrace
            ex.printStackTrace();
        }
        this.conHnd = null;
        return(retc);
    }

    public String getSmtpResponse() {
        return(this.conHnd == null ? "" : this.conHnd.getResponseLine());
    }


    // -- private --------------------------------------------------------------
    private int _send(String str,boolean waitResponse) {
        if(this.conHnd == null) return(ERR_INVHANDLE);
        try {
            //System.out.print("[-->]" + str);
            this.conHnd.outs.write(str.getBytes());
        } catch (IOException ex) { // IO error
            ex.printStackTrace();
            return(ERR_IO);
        }

        this.conHnd.setResponseLine(_read(waitResponse));
        return(_smtpRetcCode(this.conHnd.getResponseLine()));
    }

    private String _read(boolean waitResponse) {
        char[] b = new char[1024];
        String rstr = "";
        boolean isAv;
        int retc;

        if(waitResponse) {
            try {
                for(int i = 0; i < 3; i++) {
                    isAv = false;
                    while(this.conHnd.ins.ready()) {
                        isAv = true;
                        retc = this.conHnd.ins.read(b,0,b.length);
                        rstr +=new String(b, 0, retc);
                    }
                    if(!isAv) try {
                        Thread.currentThread().sleep(100);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            } catch(Exception e2) {
                e2.printStackTrace();
            }
        } else {
            try {
                while(this.conHnd.ins.ready()) {
                    retc = this.conHnd.ins.read(b,0,b.length);
                    rstr +=new String(b, 0, retc);
                }
            } catch(Exception e2) {
                e2.printStackTrace();
            }
        }

        return(rstr);
    }


    //  izdvaja smtp return code iz linije i pretvori u int
    private int _smtpRetcCode(String s) {
        if(s.length() < 3) return(0);

        try {
            return(Integer.parseInt(s.substring(0,3)));
        } catch(Exception e) {
            e.printStackTrace();
        }
        return(0);
    }

}