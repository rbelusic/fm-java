package org.fm.utils.mail.pop3;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Pop3 {

    private static ArrayList fHandles = new ArrayList();
    // end of line
    private final static String CRLF =
            new Character((char) 13).toString()
            + new Character((char) 10).toString();
    private static String lastRead = "";

    public static int open(
            String host, int port,
            String user, String password) throws UnknownHostException, IOException {
        ConnectionHnd s;
        int retc;

        // povezi se

        s = new ConnectionHnd(host, port);
        s.setResponseLine(Pop3._read(s, true));
        retc = Pop3._pop3RetcCode(s.getResponseLine());

        // autoriziraj se
        // posalji username
        retc = Pop3._send(s, "USER " + user + Pop3.CRLF, true);
        if (retc < 0 || retc >= 400) {
            try {
                s.socket.close();
            } catch (IOException ex) {}
            return (retc);
        }

        // posalji password
        retc = Pop3._send(s, "PASS " + password + Pop3.CRLF, true);
        if (retc < 0 || retc >= 400) {
            try {
                s.socket.close(); // zatvori socket
            } catch (IOException ex) {}
            return (retc);
        }

        Pop3.fHandles.add(s);
        return (Pop3.fHandles.size());
    }

    public static int rset(int handle) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        return (Pop3._send(s, "RSET" + Pop3.CRLF, true));
    }

    public static int list(int handle) throws IOException {
        int retc;
        String rl;
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        // init
        s.conDataClear();

        // posalji list
        retc = Pop3._send(s, "LIST" + Pop3.CRLF, true);
        if (retc != 0) {
            return (retc);
        }

        // pokupi listu poruka
        rl = ""; // postavi ga na null
        while (!rl.equals(".")) {
            rl = Pop3._read(s, true);
            if (!rl.equals(".")) {
                s.addConData(new Pop3MsgInfo(rl));
            }

        }

        // vrati return code
        return (retc);
    }

    public static int dele(int handle, int msgNum) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }
        return (Pop3._send(s, "DELE " + msgNum + Pop3.CRLF, true)); // delete
    }

    public static void retr(int handle, int msgNum) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        Pop3MsgInfo mi;
        int retc;

        // init
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        // nadji info poruke
        mi = Pop3._findMsg(s, msgNum);
        if (mi == null) {
            throw new IOException("Invalid handle");
        }

        // posalji RETR
        retc = Pop3._send(s, "RETR " + msgNum + Pop3.CRLF, true);
        if (retc != 0) {
            return;
        }

        // pokupi tekst poruke
        Pop3._readMsgData(s, mi);
    }

    // kraj rada (ERR_OK)
    public static int quit(int handle) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        // kraj
        int retc = Pop3._send(s, "QUIT" + Pop3.CRLF, true);
        try {
            s.socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        // izbaci handle
        Pop3.fHandles.remove(handle);
        return (retc);
    }

    public static int getMsgListSize(int handle) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        return (s.getConDataCount());
    }

    public static int getMsgSize(int handle, int msgNum) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        Pop3MsgInfo mi = Pop3._findMsg(s, msgNum);
        if (mi == null) {
            throw new IOException("Not found");
        }

        return (mi.getMsgSize());
    }

    public static int getReadedMsgSize(int handle, int msgNum) throws IOException {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        Pop3MsgInfo mi = Pop3._findMsg(s, msgNum);
        if (mi == null) {
            throw new IOException("Not found");
        }

        return (mi.getMsgTextSize());
    }

    public static String getMsgText(int handle, int msgNum, int pos, int len) {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        if (s == null) {
            return (null);
        }

        Pop3MsgInfo mi = Pop3._findMsg(s, msgNum);
        if (mi == null) {
            return (null);
        }

        try {
            return ((String) mi.getMsgText(pos, len));
        } catch (Exception e) {
            e.printStackTrace();
            return (null);
        }
    }

    // vrati pop3 response line
    public static String getPop3Response(int handle) {
        ConnectionHnd s = (ConnectionHnd) Pop3.fHandles.get(handle);
        String respline = s.getResponseLine();
        return (s == null ? ""
                : (respline== null || respline.isEmpty()
                ? Pop3.lastRead : s.getResponseLine()));
    }

    // -- private --------------------------------------------------------------
    private static Pop3MsgInfo _findMsg(ConnectionHnd s, int n) {
        Pop3MsgInfo mi;

        for (int i = 0; i < s.getConDataCount(); i++) {
            mi = (Pop3MsgInfo) s.getConData(i);
            if (mi.getMsgNum() == n) {
                return (mi);
            }
        }
        return (null);
    }

    private static int _send(ConnectionHnd s, String str, boolean waitResponse) throws IOException {
        // provjeri handle da li je valjan
        System.out.println("SEND:" + str);
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        
        s.outs.write(str.getBytes());

        s.setResponseLine(Pop3._read(s, waitResponse));
        return (Pop3._pop3RetcCode(s.getResponseLine()));
    }

    private static String _read(ConnectionHnd s, boolean waitResponse) {
        int cnt = 200;
        String rstr = "";
        int pauseMs = 100;

        try {
            // dok ne naletis na CRLF
            while (rstr.indexOf(Pop3.CRLF) < 0) {
                if (s.ins.ready()) {
                    rstr += new Character((char) s.ins.read()).toString();
                    cnt = 200;
                } else {
                    if (cnt < 1) {
                        Pop3.lastRead = rstr;
                        System.out.println("LASTREAD1:[" + Pop3.lastRead + "]");
                        return (null);
                    }
                    if (waitResponse) {
                        try {
                            Thread.currentThread().sleep(pauseMs);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    cnt--;
                    // povecaj pauzu za duplo
                    pauseMs = pauseMs * 2;
                }
            }

            // ako je CRLF na kraju sve OK
            Pop3.lastRead = rstr;
            System.out.println("LASTREAD2:[" + Pop3.lastRead + "]");
            if (rstr.indexOf(Pop3.CRLF) < 0) {
                return (null);
            }
            return (rstr.substring(0, rstr.indexOf(Pop3.CRLF)));
        } catch (Exception e) {
            Pop3.lastRead = rstr;
            e.printStackTrace();
            return (null);
        }
    }

    // procitaj test poruke s socketa i upis u pop2msginfo
    private static void _readMsgData(ConnectionHnd s, Pop3MsgInfo mi) throws IOException {
        int cnt = 20, retc = 0;
        char[] b = new char[1024];

        // provjeri handle da li je valjan
        if (s == null) {
            throw new IOException("Invalid handle");
        }

        mi.clearMsgText();
        
            // do kraja poruke
            while (retc > -1 && !mi.isMsgEof()) {
                if (s.ins.ready()) {
                    retc = s.ins.read(b, 0, b.length);
                    if (retc > 0) {
                        mi.addMsgText(new String(b, 0, retc));
                    }
                    System.out.println("_readMsgData: " + retc);
                    cnt = 20;
                } else {
                    if (cnt < 1) {
                        System.out.println("_readMsgData: TIMEOUT");
                        throw new IOException("Timeout");
                    }
                    try {
                        System.out.println("_readMsgData: SLEEP");
                        Thread.currentThread().sleep(500);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    cnt--;
                }
            }
            if (!mi.cutMsgText()) {
                throw new IOException("EOF");
            }
        
    }

    // vraca error code ovisno o POP3 odgovoru
    private static int _pop3RetcCode(String s) {
        if (s == null) {
            return (600);
        }
        if (s.length() < 3) {
            return (600);
        }
        if (s.substring(0, 3).equals("+OK")) {
            return (0);
        }
        if (s.substring(0, 4).equals("-ERR")) {
            return (500);
        }
        System.out.println("_pop3RetcCode=999: [" + s + "]");
        return (999);
    }
}
