/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.utils.mail.encodings;

import java.io.UnsupportedEncodingException;

public class Mime {
    public static String decode(String ms) throws UnsupportedEncodingException {
        String procs = ms == null ? "" : ms;
        String decs;
        
        int poc = procs.indexOf("=?");
        int end = procs.indexOf("?=");        
        
        while(poc >= 0 && end >= 0 && poc < end) {
            decs = decodeText(procs.substring(poc+2,end));
            procs = 
                procs.substring(0, poc) +
                decs +
                procs.substring(end+2)
            ;
            poc = procs.indexOf("=?");
            end = procs.indexOf("?=");        
        }
        
        return procs;
    }
        
    private static String decodeText(String s) throws UnsupportedEncodingException {
        String [] spls = s.split("\\?"); // podijeli string na komponente
        String charset = spls.length > 0 ? spls[0] : null;
        String enctype = spls.length > 1 ? spls[1] : null;
        String enctext = spls.length > 2 ? spls[2] : null;
        byte [] decb;
        String dectext;
        
        System.out.println("decodeText <= [" + charset + "," +  enctype + ","+ enctext + "]");
        if(charset != null && enctype != null && enctext != null) {
            if(enctype.equalsIgnoreCase("B")) {                
                decb = Base64.decode(enctext); 
            } else if(enctype.equalsIgnoreCase("Q")) {
                QuotedPrintable.startDecoding();
                QuotedPrintable.addDataForDecoding(enctext);
                QuotedPrintable.endDecoding();                
                decb = QuotedPrintable.getDecodedBytes(0,QuotedPrintable.getDecodedSize());
            } else {
                decb = s.getBytes();
            }
        } else {
            decb = s.getBytes();
        }
        
        // vrati u string
        dectext = charset == null ? new String(decb) : new String(decb,charset);
        
        return dectext;
        
    }
}
