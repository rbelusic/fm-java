// ERRORS - Ok, 24.02.2010.
// PREBACITI IZ STATIC
package org.fm.utils.mail.encodings;

import java.util.ArrayList;


public class QuotedPrintable {
   private final static String pseudoHex[] = {
        "0", "1", "2",
        "3", "4", "5", "6", "7", "8",
        "9", "A", "B", "C", "D", "E",
        "F"
        };
    private final static byte CR = 0x0D;
    private final static byte LF = 0x0A;
    private final static byte EQUAL = 0x3D;
    
    
    private QuotedPrintable(){}
    
    private static String byteArrayToHexString(byte in[],int pos,int len) {
        byte ch = 0x00;
        int i = 0; 
        String out = "";
        
        if (in == null || in.length <= 0) return(out);

        while (i < len) {            
            ch = in[i+pos];
            out = 
                out + 
                QuotedPrintable.pseudoHex[(((ch & 0xF0) >>> 4) & 0x0F)] + 
                QuotedPrintable.pseudoHex[(ch & 0x0F)];
            i++;
        }
        return(out);
    }    

    // prebaci iz hex reprezentacije u byte (binarno)
    private static byte hexStringToByte(String in) {
        return((byte)Integer.parseInt(in, 16));
    }
    
    private static ArrayList<String> linesQP = null; // sve enkodirane linije
    private static byte [] lineQP; // trenutna linija koja se dekodira
    private static int lineQPpos; // velicina dekodiranog buffera
    private static int fpos = 0;

    // -- encoding -------------------------------------------------------------

    // pocetak kodiranja
    public static void startEncoding() {
        // inicijalizacija
        QuotedPrintable.linesQP = new ArrayList<String>();
        QuotedPrintable.lineQP = new byte[80];
        QuotedPrintable.lineQPpos = 0;
        QuotedPrintable.fpos = 0;
        
    }

    // kodiraj podatke (raw podatak) 
    public static void encodeData(byte [] inb,int pos, int len) {
        byte [] tmpbytes;
        int dlen;
        String stmp;

        // kodiranje
        for(int i = pos; i < pos + len; i++) {
            // da li linija prelazi 71 sa novim podatkom                
            if(QuotedPrintable.lineQPpos > 71 || QuotedPrintable.fpos  == 16) {
                QuotedPrintable.lineQP[QuotedPrintable.lineQPpos] = '=';
                QuotedPrintable.lineQPpos++;
                stmp = new String(
                    QuotedPrintable.lineQP,0,
                    QuotedPrintable.lineQPpos
                    );
                QuotedPrintable.linesQP.add(stmp);                
                QuotedPrintable.lineQPpos = 0; 
                QuotedPrintable.fpos = 0;
            }
                        
            // duzina novog podatka
            if(
                ((inb[i] >= 32 && inb[i] <= 60) || (inb[i] >= 62 && inb[i] <= 126)) &&
                inb[i] != 46
                )
            {
                dlen = 1;
            } else {
                dlen = 3;
            }
            // upisi podatak u listu
            if(dlen == 1)  {
                QuotedPrintable.lineQP[QuotedPrintable.lineQPpos] = inb[i];
                QuotedPrintable.lineQPpos++;
            } else {
                stmp = QuotedPrintable.byteArrayToHexString(inb,i, 1);
                tmpbytes = stmp.getBytes();                
                QuotedPrintable.lineQP[QuotedPrintable.lineQPpos] = '=';
                QuotedPrintable.lineQPpos++;
                QuotedPrintable.lineQP[QuotedPrintable.lineQPpos] = tmpbytes[0];
                QuotedPrintable.lineQPpos++;
                QuotedPrintable.lineQP[QuotedPrintable.lineQPpos] = tmpbytes[1];
                QuotedPrintable.lineQPpos++;
            }
            
            QuotedPrintable.fpos++;
        }
        
    }

    // kraj kodiranja, rijesi ono sto imas u bufferu
    public static void endEncoding() {
        String stmp;
        if(QuotedPrintable.lineQPpos > 0) { // ako je unutar linije
            stmp = new String(
                QuotedPrintable.lineQP,0,QuotedPrintable.lineQPpos
                );
            QuotedPrintable.linesQP.add(stmp);
            QuotedPrintable.lineQPpos = 0;
        }
    }

    // vrati velicinu buffera s kodiranim podacima

    public static int getEncodedCount() {
        return(QuotedPrintable.linesQP.size());
    }

    // vrati kodirani string
    public static String getEncodedLine(int pos) {
        String stmp = (String)QuotedPrintable.linesQP.get(pos);
        return(stmp);
    }
    

    // -- decoding -------------------------------------------------------------
    // buffer varijable
    private static String encodedStringQP = ""; // enkodirani podaci
    private static byte [] decodedQPData = null; // dekodirani podaci

    // pocni dekodirati
    public static void startDecoding() {
        // inicijalizacija
        QuotedPrintable.encodedStringQP = "";
        QuotedPrintable.decodedQPData = null;
    }

    // dodaj podatke u buffer za dekodiranje
    public static void addDataForDecoding(String s) {
        QuotedPrintable.encodedStringQP += s;
    }

    // kraj dekodiranja
    public static void endDecoding() {
        byte [] encData = QuotedPrintable.encodedStringQP.getBytes();
        byte [] decData = new byte [encData.length];
        int dpos=0;
        
        // ako je \n (LF) na razlomljenoj liniji (ona koja zavrsava sa =), ignoriramo ga
        for(int i=0; i < encData.length; i++) {
            if(encData[i] == QuotedPrintable.EQUAL) { // =
                if( // ako je kraj linije
                    encData[i+1] ==  QuotedPrintable.LF ||
                    encData[i+1] ==  QuotedPrintable.CR
                    )
                {
                    i++;
                    if(
                        encData[i+1] ==  QuotedPrintable.LF ||
                        encData[i+1] ==  QuotedPrintable.CR
                        )
                    {
                        i++;
                    }
                } else { // nije kraj linije, dekodiramo                    
                    // dekodiraj hex u byte
                    decData[dpos] = QuotedPrintable.hexStringToByte(
                        new String(encData,i+1,2)
                        );
                    i+=2;
                    dpos++; // uvecaj brojac
                }
            } else { // nije =
                decData[dpos] = encData[i];
                dpos++;
            }
        }

        // kopiraj dekodirane podatke
        QuotedPrintable.decodedQPData = new byte [dpos];
        System.arraycopy(decData, 0, QuotedPrintable.decodedQPData, 0, dpos);
    }

    // vrati velicinu dekodiranog buffera
    public static int getDecodedSize() {
        return(QuotedPrintable.decodedQPData.length);
    }

    public static byte [] getDecodedBytes(int pos,int len) {
        int blen = 
            (len > QuotedPrintable.getDecodedSize() - pos) ? 
            QuotedPrintable.getDecodedSize() - pos : len;        
        byte[] buff = new byte[blen];
        System.arraycopy(QuotedPrintable.decodedQPData, pos, buff, 0, blen);

        // vrati buffer sa dekodiranom porukom
        return(buff);
    }
    
    
    // -- util -----------------------------------------------------------------
    // ocisti sve radne podatke
    public static void clearBuffers() {
        QuotedPrintable.linesQP = null;        
        QuotedPrintable.lineQP = null;
        QuotedPrintable.lineQPpos = 0;
        QuotedPrintable.encodedStringQP = "";
        QuotedPrintable.decodedQPData = null;

    }

} 

