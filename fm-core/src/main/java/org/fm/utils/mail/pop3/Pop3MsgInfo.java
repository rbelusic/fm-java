// ERRORS - Ok, 24.02.2010.
package org.fm.utils.mail.pop3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.fm.utils.ByteArrayUtil;


// NIJE FUNKCIONALNA!!

public class Pop3MsgInfo {
    private final static String LF =
        new Character((char)10).toString()
        ;
    private final static String CRLF = 
        new Character((char)13).toString() +
        new Character((char)10).toString()
        ;
    private final static String msgEOF = Pop3MsgInfo.LF + "." + Pop3MsgInfo.CRLF;


    private int msgNum;
    private int msgSize;
    private File msgFile = null;
    private int msgFileSize = 0;
    private RandomAccessFile msgFileObj = null;

    
    public Pop3MsgInfo(String inp) throws IOException {
        int sppos,retc;

        inp = inp.trim();
        sppos = inp.indexOf(" ");

        this.msgNum = Integer.parseInt(inp.substring(0, sppos).trim());
        this.msgSize = Integer.parseInt(inp.substring(sppos+1).trim());


        this.createBuffFile();
    }

    public int getMsgNum() {
        return(this.msgNum);
    }

    public int getMsgSize() {
        return(this.msgSize);
    }

    public int getMsgTextSize() {
        int len = 0;
        if(this.msgFileObj == null) return(0);
        try {
            len = (this.msgFileSize == -1 ? (int) this.msgFileObj.length() : this.msgFileSize);
        } catch (IOException ex) {
            len = 0;
            ex.printStackTrace();
        }
        return(len);
        //return(this.msgText.length());
    }

    public String getMsgText() {        
        return(this.getMsgText(0,this.getMsgTextSize()));
    }

    public String getMsgText(int pos, int len) {
        byte [] ar;

        if(this.msgFileObj == null) return(null);
        if(pos >= this.getMsgTextSize()) return(null);
        if(pos+len > this.getMsgTextSize()) len = this.getMsgTextSize() - pos;

        try {
            this.msgFileObj.seek(pos);
            ar = new byte[len];
            this.msgFileObj.readFully(ar);
            return(new String(ar));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return(null);
    }

    public void setMsgText(String s) throws IOException {
        this.clearMsgText();
        this.addMsgText(s);
    }

    public void addMsgText(String s) {
        if(this.msgFileObj == null) return;
        try {
            this.msgFileObj.seek(this.msgFileObj.length());
            this.msgFileObj.write(s.getBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    

    public void clearMsgText() throws IOException {
        this.closeBuffFile();
        this.createBuffFile();
    }

    public boolean isMsgEof() {
        int br;
        int pos = 0;

        byte[] eofb = Pop3MsgInfo.msgEOF.getBytes();        

        try {
            if (this.msgFileObj.length() < eofb.length) {
                System.out.println("isMsgEof1: false");
                return false;
            }
            
            // dosli smo do kraja, mozda file zavrsava s točkom        
            this.msgFileObj.seek(this.msgFileObj.length() - eofb.length);
            for(int i = 0; i < eofb.length; i++) {
                if(this.msgFileObj.read() != eofb[i]) {
                    System.out.println("isMsgEof2: false");
                    return false;
                }
            }
        } catch (IOException ex) {
            System.out.println("isMsgEof3: EXC");
            ex.printStackTrace();
        }
            
        System.out.println("isMsgEof4: true");
        return true;
    }
    
    // izbaci kraj poruke (LF.CRLF)
    public boolean cutMsgText() {
        int br;
        int pos = 0;

        byte[] eofb = Pop3MsgInfo.msgEOF.getBytes();
        byte[] rb = new byte[eofb.length];
        this.msgFileSize = -1;

        try {
            if (this.msgFileObj.length() < eofb.length) {
                return false;
            }
            this.msgFileObj.seek(0);
            br = this.msgFileObj.read(rb);
            if (br < rb.length) {
                return false;
            }
            if (ByteArrayUtil.compare(eofb, rb)) {
                this.msgFileSize = 0;
                return true;
            }
            for (pos = 1; pos < this.msgFileObj.length() - rb.length; pos++) {
                // ByteArrayUtil.shiftLeft(rb, 1);
                rb[rb.length - 1] = (byte) this.msgFileObj.read();
                if (ByteArrayUtil.compare(eofb, rb)) {
                    this.msgFileSize = pos;
                    return true;
                }
            }

            // dosli smo do kraja, mozda file zavrsava s točkom
            if(this.msgFileObj.length() > 1) {
                this.msgFileObj.seek(this.msgFileObj.length()-2);
                if(this.msgFileObj.read() == eofb[0]) {
                    if(this.msgFileObj.read() == eofb[1]) {
                        this.msgFileSize = (int)this.msgFileObj.length()-2;
                        return true;
                    }
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return(false);
    }

    // -- buffer funkcije ------------------------------------------------------
    // otvori
    private void createBuffFile() throws IOException {
        this.msgFile = null;
        this.msgFileObj = null;
        this.msgFileSize = -1;

        try {
            String tmpDir = System.getProperty("java.io.tmpdir");
            System.out.println("tmpdir: [" + tmpDir + "]");
            this.msgFile = File.createTempFile("pop3mi", ".msg",new File(tmpDir));
            this.msgFile.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
            this.msgFile = null;
            throw e;
        }
        try {
            this.msgFileObj = new RandomAccessFile(this.msgFile, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            this.msgFileObj = null;
            this.msgFile.delete();
            this.msgFile = null;
            throw e;
        }
    }

    // zatvori
    private void closeBuffFile() {
        if(this.msgFileObj != null) try {
            this.msgFileObj.close();
        } catch (IOException ex) {}
        this.msgFileObj = null;
        if(this.msgFile != null) this.msgFile.delete();
        this.msgFile = null;
        this.msgFileSize = -1;
    }


}
