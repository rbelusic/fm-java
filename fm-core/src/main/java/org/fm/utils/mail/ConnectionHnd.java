// ERRORS - Ok, 24.02.2010.
package org.fm.utils.mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class ConnectionHnd {
    public Socket socket;
    public OutputStream outs;
    public BufferedReader ins;
    
    private String responseLine = "";
    private ArrayList conData = new ArrayList();
    

    public ConnectionHnd(String host,int port) throws UnknownHostException, IOException {
        this.socket = new Socket(host, port == 0 ? 25 : port);
        this.ins = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.outs = this.socket.getOutputStream();
    }

    public void addConData(Object obj) {
        this.conData.add(obj);
    }

    public Object getConData(int pos) {
        return(this.conData.get(pos));
    }

    public int getConDataCount() {
        return(this.conData.size());
    }

    public void conDataClear() {
        this.conData.clear();
    }

    public void setResponseLine(String s) {
        this.responseLine = s;
    }

    public String getResponseLine() {
        return(this.responseLine);
    }

}
