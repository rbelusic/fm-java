package org.fm.utils;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import org.fm.FmEventsDispatcher;

/**
 *
 * @author rbelusic
 * 
 * TODO: cancel download ne mogu impl sa EV mehanizmom
 */
public class TransferUtil {

    // download datoteke sa pg homepagea
    public static boolean downloadFile(String urladr, String fname, Object eventsReceiver) throws IOException {
        byte[] buffer;
        int numRead;
        long numWritten = 0;
        OutputStream out = null;
        URLConnection conn = null;
        InputStream in = null;
        URL url;

        try {
            url = new URL(urladr);
            out = new BufferedOutputStream(new FileOutputStream(fname));
            conn = url.openConnection();
            in = conn.getInputStream();
            buffer = new byte[1024];
            if (eventsReceiver != null) {
                FmEventsDispatcher.fireEvent(
                        null, eventsReceiver,
                        "onDownloadStarted",
                        new Long(conn.getContentLength()));
            }
            while ((numRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, numRead);
                numWritten += numRead;
                if (eventsReceiver != null) {
                    FmEventsDispatcher.fireEvent(
                        null,eventsReceiver, 
                        "onDownloadProgress", 
                    new Long(numWritten));
                    /*
                    if (!ntf.downloadProgress(numWritten)) {
                        in.close();
                        out.close();
                        ntf.downloadCanceled();
                        return (false);
                    }
                    */
                }
            }
            in.close();
            out.close();
            if (eventsReceiver != null) {
                    FmEventsDispatcher.fireEvent(
                        null,eventsReceiver, 
                        "onDownloadEnd",new Long(numWritten)); 
            }
        } catch (IOException exception) {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ioe) {
            }
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ioe) {
            }
            throw exception;
        }
        return (true);
    }
}
