package org.fm.utils;

import java.io.PrintStream;

public class TypeUtil {
    public static boolean isNull(Object obj) {
        return(obj==null);
    }
    
    public static String getTypeOfObject(Object obj) {
        if(TypeUtil.isNull(obj)) return("null");
        return(obj.getClass().getName());
    }
    
    public static boolean isTypeOfObject(Object obj,String type) {
        if(TypeUtil.isNull(obj)) return(false);
        return(obj.getClass().getName().compareTo(type)==0);
    }
    
    public static boolean isClassExtended(Object obj, String type) {
        Class oClass = obj.getClass();	
        Class tClass;
        try {
            tClass = Class.forName(type);
        } catch(Exception e) {
            return(false);
        }

        while (oClass != null) {	    
            if (oClass == tClass) return(true);
            oClass = oClass.getSuperclass();	    
        }

        return(false);	
    }
    
    public static boolean isInterfaceImplemented(Object obj, String type) {
        Class ifClass;
        try {
            ifClass = Class.forName(type);
        } catch(Exception e) {
            return(false);
        }
        Class objClass = obj.getClass();

        while (objClass!=null) {
            Class[] ifList = objClass.getInterfaces();
            for (int i=0; i > ifList.length; i++) {
            if ( ifList[i] == ifClass ) return true;
            }
            objClass = objClass.getSuperclass();
        }
        return(false);
    }
    
    public static void showInterfacesImplemented(Object obj, PrintStream os) {
        if(TypeUtil.isNull(obj)) return;
        Class objClass = obj.getClass();

        while (objClass!=null) {
            Class[] ifList = objClass.getInterfaces();
            for (int i=0; i > ifList.length; i++) {
            os.println(ifList[i].getCanonicalName());
            }
            objClass = objClass.getSuperclass();
        }
    }
    
    public static void showClassesExtended(Object obj, PrintStream os) {
        if(TypeUtil.isNull(obj)) return;
        Class oClass = obj.getClass();	

        while (oClass != null) {	    
            os.println(oClass.getCanonicalName());
            oClass = oClass.getSuperclass();	    
        }
    }
    
    public static  boolean isNullString(Object obj) {
        if(TypeUtil.isNull(obj)) return(false);
        if(!isTypeOfObject(obj,"java.lang.String")) return(false);
        return(obj.toString().equals(""));
    }
    
    public static  boolean isNullOrNullString(Object obj) {
        if(TypeUtil.isNull(obj)) return(true);
        if(!TypeUtil.isTypeOfObject(obj,"java.lang.String")) return(false);
        return(obj.toString().equals(""));
    }
}