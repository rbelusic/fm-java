/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author robertob
 */
public class FileUtils {
    public final static int READ_BUFF_SIZE = 4096;
    
    public final static byte[] getBytes(InputStream ins) throws IOException {
        if (ins == null) {
            return null;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[READ_BUFF_SIZE];
        int procitano = -1;
        try {
            while ((procitano = ins.read(buf, 0, READ_BUFF_SIZE)) > 0) {
                baos.write(buf, 0, procitano);
            }
            return baos.toByteArray();            
        } finally {
            try {
                baos.close();
            } catch (IOException ioe) {}
        }
    }
}
