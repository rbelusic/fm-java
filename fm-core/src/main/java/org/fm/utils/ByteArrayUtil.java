package org.fm.utils;

public class ByteArrayUtil {
    public static boolean compare(byte[] byteArray1,byte[] byteArray2) {
	boolean ret;
	
	if (byteArray1.length != byteArray2.length) {
	    ret = false;
	} else {
	    int i = 0;
	    ret = true;
	    while (i < byteArray1.length && (ret == true) ) {
		ret = (byteArray1[i] == byteArray2[i]);
		i++;
	    }
	}
	
	return(ret);
    }
    
    
}