package org.fm.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SystemUtil {

    public SystemUtil() {
    }

    public static boolean isMsWindowsOS() {
        if (System.getProperty("os.name").toUpperCase().indexOf("WINDOWS") == -1) {
            return (false);
        }
        return (true);
    }

    public static HashMap<String, String> cmdArgumentsParser(String[] args) {
        int i, pos;
        String arg;
        String name, value;

        HashMap<String, String> pn = new HashMap<String, String>();
        for (i = 0; i < args.length; i++) {
            arg = args[i];
            if (arg.startsWith("--")) { // --xxxx=value
                pos = arg.indexOf("=");
                if (pos < 0) {
                    name = arg.substring(2);
                    value = "";
                } else {
                    name = arg.substring(2, pos);
                    value = arg.substring(pos + 1);
                }
            } else if (arg.startsWith("-")) { // -x value
                if (i + 1 == args.length) {
                    name = arg.substring(1);
                    value = "";
                } else if (args[i + 1].startsWith("-")) {
                    name = arg.substring(1);
                    value = "";
                } else {
                    name = arg.substring(1);
                    value = args[i + 1];
                    i++;
                }
            } else { // value
                name = "@a@" + i;
                value = arg;
            }
            pn.put(name, value);
        }

        return (pn);
    }

    public static List<Class> getImplInterfaces(Class c, final List<Class> lst) {
        Class[] ifcs = c.getInterfaces();
        for(Class ifc: ifcs) {
            if(lst.contains(ifc)) continue;            
            lst.add(ifc);
            getImplInterfaces(ifc, lst);
        }              
        return lst;
    }

    public static List<Class>  getExtendedClasses(Class c, final List<Class> lst) {
        Class[] allcls = c.getClasses();        
        for(Class cls: allcls) {
            if(lst.contains(cls)) continue;            
            lst.add(cls);
        }              
        return lst;
    }
    
    public static List<Class> getClassesFromPackage(String packageNameOrig,Class fndCls) throws IOException, URISyntaxException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL packageURL;
        List<String> names = new ArrayList<String>();;
        System.out.println("getClassesFromPackage:" + packageNameOrig);
        
        String packageName = packageNameOrig.replace(".", "/");
        Enumeration<URL> urls = classLoader.getResources(packageName);
        
        while(urls.hasMoreElements()) {                    
            packageURL = urls.nextElement();
            System.out.println("  >> packageURL:" + packageURL);
            if (packageURL.getProtocol().equals("jar")) {
                String jarFileName;
                JarFile jf;
                Enumeration<JarEntry> jarEntries;
                String entryName;

                // build jar file name, then loop through zipped entries
                jarFileName = URLDecoder.decode(packageURL.getFile(), "UTF-8");
                jarFileName = jarFileName.substring(5, jarFileName.indexOf("!"));
                System.out.println("  >" + jarFileName);
                jf = new JarFile(jarFileName);
                jarEntries = jf.entries();
                while (jarEntries.hasMoreElements()) {
                    JarEntry jarElement = jarEntries.nextElement();
                    entryName = jarElement.getName();

                    if (!jarElement.isDirectory() && entryName.startsWith(packageName) && entryName.length() > packageName.length() + 5) {
                        //entryName = entryName.substring(packageName.length(), entryName.lastIndexOf('.'));
                        String clsname = entryName.replace('/', '.').substring(0, entryName.lastIndexOf('.'));
                        System.out.println("  >>  found!:" + clsname);
                        names.add(clsname);
                    }
                }

                // loop through files in classpath
            } else {
                URI uri = new URI(packageURL.toString());
                File folder = new File(uri.getPath());
            // won't work with path which contains blank (%20)
                // File folder = new File(packageURL.getFile()); 

                //C:\Users\RobertoB\AppData\Roaming\NetBeans\8.0\apache-tomcat-7.0.41.0_base\work\Catalina\localhost\GeoEvent\loader
                //C:\Users\RobertoB\AppData\Roaming\NetBeans\8.0\apache-tomcat-7.0.41.0_base\work\Catalina\localhost\GeoEvent\loader\org\fm\app\geoevent\dm

                File[] contenuti = folder.listFiles();
                String entryName;
                if(contenuti != null) for (File actual : contenuti) {
                    if(actual.isDirectory()) continue;
                    entryName = actual.getName();
                    System.out.println("  >" + entryName);
                    entryName = entryName.substring(0, entryName.lastIndexOf('.'));
                    String clsname = packageNameOrig + "." + entryName;
                    System.out.println("  >>  found!:" + clsname);                    
                    names.add(clsname);
                }
            }
        }
        
        List<Class> classes = new ArrayList<Class>();
        for(String name: names) {
            try {
                Class cls = Class.forName(name); 
                
                ArrayList<Class> clsList = new ArrayList<Class>();
                ArrayList<Class> ifcList = new ArrayList<Class>();
                
                if(
                    getExtendedClasses(cls,clsList).contains(fndCls) ||
                    getImplInterfaces(cls, ifcList).contains(fndCls)
                ) {
                    classes.add(cls);                    
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SystemUtil.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return classes;
    }
}
