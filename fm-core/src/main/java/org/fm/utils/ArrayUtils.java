package org.fm.utils;

public class ArrayUtils {
    public enum ResampleMode {

        RESAMPLE_MODE_NONE, RESAMPLE_MODE_MEAN
    }

    public static int[] resample(int[] arr, int newlen, ResampleMode mode) {
        
        double sc = (double)arr.length/(double)newlen;
        
        int distance = (int)Math.round(
                ((sc) -1) / 2.
        );
        int[] newArr = new int[newlen];
        
                
        for(int i=0; i< newArr.length; i++) {
            int oindex = (int)(sc * (double)i);
            switch(mode) {
                case RESAMPLE_MODE_MEAN:                    
                    double sum = 0;
                    int count=0;
                    int rbegin = oindex - distance;
                    if(rbegin < 0) rbegin = 0;
                    int rend = oindex + distance;
                    if(rend >= arr.length) rend = arr.length -1;
                    for(int r = rbegin;r <= rend; r++) {                        
                        sum += arr[r];
                        count++;
                    }
                    newArr[i] = (int) Math.round(sum / count);
                    break;
                default:
                    newArr[i] = arr[oindex];
            }
        }
        return newArr;
    }

    public static double[] resample(double[] arr, int newlen, ResampleMode mode) {
        
        double sc = (double)arr.length/(double)newlen;
        
        int distance = (int)Math.round(
                ((sc) -1) / 2.
        );
        double[] newArr = new double[newlen];
        
                
        for(int i=0; i< newArr.length; i++) {
            int oindex = (int)(sc * (double)i);
            switch(mode) {
                case RESAMPLE_MODE_MEAN:                    
                    double sum = 0;
                    int count=0;
                    int rbegin = oindex - distance;
                    if(rbegin < 0) rbegin = 0;
                    int rend = oindex + distance;
                    if(rend >= arr.length) rend = arr.length -1;
                    for(int r = rbegin;r <= rend; r++) {                        
                        sum += arr[r];
                        count++;
                    }
                    newArr[i] = sum / count;
                    break;
                default:
                    newArr[i] = arr[oindex];
            }
        }
        return newArr;
    }


    public static void main(String[] args)  {
        double arr[] = new double[100];
        for(int i=0; i < arr.length; i++) {
            arr[i] = Math.random()*100;
        }
        
        System.out.println("In arr:");        
        dumpArr(arr);
        
        long start = System.currentTimeMillis();
        double [] arrRes = ArrayUtils.resample(arr, 10, ResampleMode.RESAMPLE_MODE_MEAN);
        System.out.println("" + (System.currentTimeMillis() - start) + "ms");
        System.out.println("Out arr:");
        dumpArr(arrRes);
    }
    
    private static void dumpArr(double [] arr) {
        for(int i=0; i < arr.length; i++) {
            if(i % 5 == 0) System.out.println();
            System.out.printf(" %5.4f",arr[i]);            
        }
        System.out.println();
    }

}
