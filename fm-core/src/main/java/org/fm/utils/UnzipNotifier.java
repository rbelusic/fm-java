package org.fm.utils;

/**
 *
 * @author rbelusic
 */
public interface UnzipNotifier {
    public void unzipStart(String name,int numEntr);
    public void unzipElementStart(String entryName,long entrySize,boolean isDir);
    public boolean unzipElementProgress(long lenf);
    public void unzipElementEnd();
    public void unzipEnd();
    public void unzipCanceled();
}
