package org.fm.utils;

import java.io.File;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fm.FmException;

public class StringUtil {
    private final static Pattern regexPattern = Pattern.compile("\\[:(.*?)\\]");
    
    static final char hexDigit[] = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    
    // -- byte to hex ----------------------------------------------------------
    public static String byteToHexString(byte b) {
        char[] a = {hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f]};
        return new String(a);
    }

    public static String byteToHexStringReverse(byte b) {
        char[] a = {hexDigit[b & 0x0f], hexDigit[(b >> 4) & 0x0f]};
        return new String(a);
    }

    public static String byteArrayToHexString(byte[] byteArray) {
        String word = "";
        if (byteArray.length > 0) {
            for (int i = 0; i < byteArray.length; i++) {
                word = word + byteToHexString(byteArray[i]);
            }
        }
        return word;
    }

    public static String byteArrayToHexStringReverse(byte[] byteArray) {
        String word = "";
        if (byteArray.length > 0) {
            for (int i = byteArray.length - 1; i >= 0; i--) {
                word = word + byteToHexString(byteArray[i]);
            }
        }
        return word;
    }

    public static byte hexStringToByte(String chars) {
        return ((byte) Integer.parseInt(chars, 16));
    }

    public static byte[] hexStringToByteArray(String hexStr) {
        byte bArray[] = new byte[hexStr.length() / 2];
        byte firstNibble, secondNibble;
        int finalByte;

        for (int i = 0; i < (hexStr.length() / 2); i++) {
            firstNibble = Byte.parseByte(hexStr.substring(2 * i, 2 * i + 1), 16); // [x,y)
            secondNibble = Byte.parseByte(hexStr.substring(2 * i + 1, 2 * i + 2), 16);
            finalByte = (secondNibble) | (firstNibble << 4); // bit-operations only with numbers, not bytes.
            bArray[i] = (byte) finalByte;
        }
        return (bArray);
    }

    public static String[] tokenizeString(String in, char token, char strdelim) {
        List<String> v = new ArrayList<String>();
        char c;
        int i, poc = 0;
        boolean instr = false;

        for (i = 0; i < in.length(); i++) {
            c = in.charAt(i);
            if (c == strdelim) {
                instr = (instr ? false : true);
            }
            if (!instr && c == token) {
                v.add(in.substring(poc, i));
                poc = i + 1;
            }
        }
        if (i != poc) {
            v.add(in.substring(poc));
        }

        return (String[]) (v.toArray(new String[v.size()]));
    }    
    
    public static List<String> tokenizeStringToList(String in, char token, char strdelim) {
        List<String> v = new ArrayList<String>();
        char c;
        int i, poc = 0;
        boolean instr = false;

        for (i = 0; i < in.length(); i++) {
            c = in.charAt(i);
            if (c == strdelim) {
                instr = (instr ? false : true);
            }
            if (!instr && c == token) {
                v.add(in.substring(poc, i));
                poc = i + 1;
            }
        }
        if (i != poc) {
            v.add(in.substring(poc));
        }

        return v;
    }
    

    public static int indexOfFilename(final String filename) {
        if (filename == null) {
            return -1;
        }
        return filename.lastIndexOf(File.pathSeparator + 1);
    }

    public static int indexOfExtension(final String filename) {
        if (filename == null) {
            return -1;
        }
        final int extensionPos = filename.lastIndexOf(".");
        final int fnamePos = indexOfFilename(filename);
        return fnamePos > extensionPos ? -1 : extensionPos;
    }
    
    public static String getExtension(final String filename) {
        if (filename == null) {
            return null;
        }
        final int index = indexOfExtension(filename);
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }
    public static String replaceExtension(final String filename,String newExt) {
        return removeExtension(filename) + newExt;
    }
    
    public static String removeExtension(final String filename) {
        if (filename == null) {
            return null;
        }
        final int index = indexOfExtension(filename);
        if (index == -1) {
            return filename;
        } else {
            return filename.substring(0, index);
        }
    }

    public static String applyTemplate(String templateStr, Map<String, Object> args) {
        ArrayList<String> newVars = new ArrayList<String>();
        String parsedStr = "";
        int last = 0;

        Matcher m = regexPattern.matcher(templateStr);

        while (m.find()) {
            String bv = m.group(1);
            int mstart = m.start(1) - 2;
            int mend = m.end(1) + 1;

            newVars.add(bv);
            Object val = args.get(bv);
            parsedStr += templateStr.substring(last, mstart);
            if (val != null && val instanceof List) {
                for (int i = 0; i < ((List) val).size(); i++) {
                    parsedStr += (i == 0 ? "" : ",") + ((List) val).get(i).toString();
                }
            } else if (val != null && val.getClass().isArray()) {
                for (int i = 0; i < ((Object[]) val).length; i++) {
                    parsedStr += (i == 0 ? "" : ",") + ((Object[]) val)[i].toString();
                }
            } else {
                parsedStr += val.toString();
            }
            last = mend;
        }
        parsedStr += templateStr.substring(last);

        return parsedStr;
    }
    
}