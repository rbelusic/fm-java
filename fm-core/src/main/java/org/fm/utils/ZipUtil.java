/*
 * ZipUtil.java
 *
 * Created on 2007. studeni 15, 10:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.fm.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipUtil {
    public static boolean unzipFile(String fname, String destDir,UnzipNotifier ntf) 
    throws IOException 
    {
        byte[] buffer = new byte[1024];
        int len;
        long lenf;
        InputStream is;
        OutputStream os;
        File dir;
        ZipFile zipFile = new ZipFile(fname);
        Enumeration entries = zipFile.entries();

        if(ntf != null) ntf.unzipStart(fname,zipFile.size());
        while(entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry)entries.nextElement();
            if(ntf != null) ntf.unzipElementStart(entry.getName(),entry.getSize(),entry.isDirectory());
            if(entry.isDirectory()) {
                dir = new File(destDir + File.separator + entry.getName());
                if(!dir.exists()) dir.mkdirs();
            } else {
                // dir
                dir = new File(destDir + File.separator + entry.getName()).getParentFile();
                if(!dir.exists()) dir.mkdirs();
                // file
                lenf = 0;
                is = zipFile.getInputStream(entry);
                os = new BufferedOutputStream(new FileOutputStream(destDir + File.separator + entry.getName()));
                while((len = is.read(buffer)) >= 0) {		    
                    os.write(buffer, 0, len);
                    lenf += len;
                    if(ntf != null) if(!ntf.unzipElementProgress(lenf)) {
                        is.close();
                        os.close();
                        zipFile.close();
                        ntf.unzipCanceled();
                        return(false);
                    }
                }
                is.close();
                os.close();
            }
            if(ntf != null) ntf.unzipElementEnd();
        }

        zipFile.close();
        if(ntf != null) ntf.unzipEnd();
        return(true);
        }    
}
