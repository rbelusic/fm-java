package org.fm.utils;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.MemoryCacheImageOutputStream;

public class ImageUtils {
    
    public ImageUtils() {
    }
    
    // snima JPG u output stream
    public static boolean saveBufferedImage(BufferedImage bi, String suffix, OutputStream os)
    throws IOException, FileNotFoundException {
	// nadji writer za suffix
	Iterator iter = ImageIO.getImageWritersBySuffix(suffix);
	if(!iter.hasNext() ) {
	    return(false);
	}
	ImageWriter writer = (ImageWriter)iter.next();
	
	// kreiraj output stream
	MemoryCacheImageOutputStream mos = new MemoryCacheImageOutputStream(os);
	writer.setOutput(mos);
	
	// kreiraj image iz buffered image
	IIOImage image = new IIOImage(bi,null,null);
	writer.write(image);
	
	// kraj
	return(true);
    }
    
    // snima buffered image u JPG
    public static boolean saveBufferedImage(BufferedImage bi, String suffix, String destFilename)
    throws IOException, FileNotFoundException {
	saveBufferedImage(bi, suffix,new FileOutputStream(destFilename));
	// kraj
	return(true);
    }
}
