package org.fm.dao;

import org.fm.proxy.GenericProxyClass;

public class DaoProxyClass<T extends DaoObject> extends GenericProxyClass {

    public DaoProxyClass(T daoImpl) {
        super(daoImpl);
    }
    
}
