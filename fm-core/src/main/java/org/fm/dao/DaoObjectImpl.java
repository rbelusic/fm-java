
package org.fm.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmObject;
import org.fm.store.StorageConnection;

public class  DaoObjectImpl implements DaoObject {     
    @Override
    public String getID() {
        return(getClass().getSimpleName() + "-ID:" + Thread.currentThread().getId());
        
    }
    
    public <T extends FmSessionContext> T ctx() {
        return (T) FmContext.getSessionBean(FmSessionContext.class);
    }
    
    public StorageConnection store() {
        FmSessionContext ctx = ctx();
        return ctx == null ? null : ctx.getStorageConnection();
    }    
    
    public <T extends DmObject> T get(Class<T> dmObjectClass,String id) {
        return store().get(dmObjectClass, id);
    }
    
    public <T extends DmObject> List<T> get(Class<T> dmObjectClass,String [] ids) {
        return store().get(dmObjectClass, ids);
    }
    
    public <T extends DmObject> List<T> getBy(Class<T> dmObjectClass,String attrName, Object attrValue) {
        return store().findBy(dmObjectClass, attrName, attrValue);
    }

    public <T extends DmObject> void put(T [] dms) {
        store().put(dms);
    }    
    
    public <T extends DmObject> void update(T [] dms, Set<String> columns) {
        store().update(dms,columns);
    }    
    
   
    public <T extends DmObject> List<T> find(Class<T> dmObjectClass,String sql,Map<String, Object> params) {
        List<T> dmList = ctx().getStorageConnection().find(dmObjectClass,sql, params);
        return dmList;        
    }
    
    public void execute(String sql,Map<String, Object> params) {
        ctx().getStorageConnection().execute(sql, params);
    }    
}
