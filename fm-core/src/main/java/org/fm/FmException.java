package org.fm;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * FM exception class. Predefined error can be loaded using 
 *  addErrorTypes(Properties props)
 *  or
 *  addErrorTypes(Properties props)
 *  addErrorTypes(Map<String, String> errMap)
 * 
 * There are two predefined errors noErrorCode = "FE0000" and unknownErrorCode = "FE9999"
 * 
 * Example (1):
 *      
        FmException.addErrorTypes(new HashMap<String,String>() {{
            put("FET001","First test exception");
            put("FET003","Third test exception");
        }});

 * Example (2):
 *
        Properties prop = new Properties();
        prop.load(new FileInputStream("FmException.properties"));
        FmException.addErrorTypes(prop);

* Usage example:
*       try {
            throw new FmException("FET003","This is User Message");
        } catch(FmException e) {
            System.out.println(e.getMessage());
        }

 */

public class FmException extends RuntimeException {
    private final static String unknownErrorCodeMessage = "Unknown error code";
    private final static String noErrorCode = "FE0000";
    private final static String unknownErrorCode = "FE9999";
    private static HashMap<String,String> errors = new HashMap<String,String>() {{
        put("FE0000","No Error"); // no error (format of errorCode = FE[layerChar(1)]NNN 0 - generic, 9 - unspecified, A - application ...
        put("FE0001","Context configuration error");
        put("FE0002","Authentication error");
        put("FE9999","Unspecified error"); // generic error        
        
        put("FEM001","Store connection error"); // DM errors        
        put("FEM002","Entity not found"); 
        put("FEM003","Fetch error"); 
        put("FEM004","Store entity deserialization error"); 
        put("FEM010","Invalid attribute name"); 

        put("FEF010","Observer run error"); 
        put("FEF011","Observer verification error"); 
    }};
    
    private final String fmMessage; 
    private final String fmErrorCode; 
    private Object fmSource; 
    
    public FmException() {
        super(formatMessageString(noErrorCode,""));
        this.fmErrorCode = noErrorCode;
        this.fmMessage = "";
    }
    
    public FmException(String message) {
        super(formatMessageString(unknownErrorCode,message));
        this.fmErrorCode = unknownErrorCode;
        this.fmMessage = message;
    }
    
    public FmException(String errorCode,String message) {
        super(formatMessageString(errorCode,message));
        this.fmErrorCode = errorCode;
        this.fmMessage = message;
    }
    
    public FmException(String errorCode,Throwable cause) {
        super(formatMessageString(errorCode,""), cause);
        this.fmErrorCode = errorCode;
        this.fmMessage = "";
    }
    
    public FmException(String errorCode,String message,Throwable cause) {
        super(formatMessageString(errorCode,message),cause);
        this.fmErrorCode = errorCode;
        this.fmMessage = message;
    }

    public FmException(String errorCode,String message,Exception ex) {
        super(formatMessageString(errorCode,message),ex);
        this.fmErrorCode = errorCode;
        this.fmMessage = message;
    }


    /**
     * @return the fmMessage
     */
    public String getUserMessage() {
        return fmMessage;
    }

    /**
     * @return the fmErrorCode
     */
    public String getErrorCode() {
        return fmErrorCode;
    }
        
    // populate list of known errors
    public static void addErrorTypes(Map<String, String> errMap) {
        for (Map.Entry<String, String> entry : errMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            errors.put(key, value);
        }
    }
    
    public static void addErrorTypes(Properties props) {
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            errors.put(key, value);
        }
    }
    
    // static utl
    private static String getMessageForCode(String code) {
        return errors.get(code);
    }

    private static String formatMessageString(String code,String userMessage) {
        String sysMessage = getMessageForCode(code);
        if(sysMessage == null) {
            sysMessage = unknownErrorCodeMessage;
        }
        //return  code + " - " + sysMessage + (userMessage == null || userMessage.isEmpty() ? "" : "\n" + userMessage);        
        return 
            (userMessage == null || userMessage.isEmpty() ? "" : userMessage + "\n") + 
            code + " - " + sysMessage
        ;
    }    

    /**
     * @return the source
     */
    public Object getSource() {
        return fmSource;
    }

    /**
     * @param source the source to set
     */
    public void setSource(Object source) {
        this.fmSource = source;
    }
}