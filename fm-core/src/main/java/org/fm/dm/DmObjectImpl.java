package org.fm.dm;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fm.FmDispatcher;
import org.fm.FmEventsDispatcher;
import org.fm.FmException;
import org.fm.FmObjectImpl;

class DmObjectImpl<S, T extends DmObject> extends FmObjectImpl implements DmObject {
    /**
     * Metadata
     * 
     */
    private static Map<Class,List<Annotation>> subClassAnnotations = 
        new HashMap<Class,List<Annotation>>();

    /**
     * Dm object class.
     */
    private boolean changed = false;
    private long updateTimestamp;
    
    private final Class<T> subClass;
    private DmObject dmProxy;
    private final HashMap<String, S> attributes = new HashMap<String, S>();
    private final HashMap<String, S> instanceAttributes = new HashMap<String, S>();
    private String dataIdAttr = "";
    private final FmDispatcher listener;

    public DmObjectImpl(Class<T> dmObjectSubClass) {
        this.subClass = dmObjectSubClass;
        listener = new FmEventsDispatcher(this);
    }

    @Override
    public boolean equals(Object obj) {
        return getProxy() == obj;
    }

    @Override
    public Class getSubClass() {
        return this.subClass;
    }

    @Override
    public String getSubClassName() {
        return this.subClass.getName();
    }

    @Override
    public String getDataIDAttribute() {
        if (dataIdAttr.isEmpty()) {
            dataIdAttr = DmFactory.getDmDataIdAttribute(this.getSubClass());
        }
        return dataIdAttr;
    }
    

    @Override
    public String getDataID() {
        if (dataIdAttr.isEmpty()) {
            dataIdAttr = DmFactory.getDmDataIdAttribute(this.getSubClass());
        }
        return ".".equals(dataIdAttr) ? this.getID()
                : (String) (this.attributes.containsKey(dataIdAttr.toLowerCase()) ? this.attributes.get(dataIdAttr.toLowerCase()) : this.getID());
    }

    @Override
    public boolean isAttr(String attrName) {
        return DmFactory.getDmAttributeMetadata(getSubClass(), attrName) == null;
    }

    @Override
    public <V> void setAttr(String attrName, V value) {
        setAttr(attrName, value, true);
    }
    
    @Override
    public <V> void setAttr(String attrName, V value, boolean fireEv) {
        if (attrName == null || attrName.isEmpty()) {
            throw new FmException("FEM010", "Unable to set attribute value");
        }
        DmAttribute meta = DmFactory.getDmAttributeMetadata(getSubClass(), attrName);
        Class attrType = meta == null ? String.class : meta.getType();
        if(value != null && !attrType.isAssignableFrom(value.getClass())) {
            try {                
                Constructor constructor = attrType.getConstructor(value.getClass());
                Object tvalue = constructor.newInstance(value);                
                this.attributes.put(attrName.toLowerCase(), (S)tvalue);
            } catch (InstantiationException ex) {
                throw new ClassCastException(ex.getMessage());
            } catch (IllegalAccessException ex) {
                throw new ClassCastException(ex.getMessage());
            } catch (NoSuchMethodException ex) {
                throw new ClassCastException(ex.getMessage());
            } catch (SecurityException ex) {
                throw new ClassCastException(ex.getMessage());
            } catch (IllegalArgumentException ex) {
                throw new ClassCastException(ex.getMessage());
            } catch (InvocationTargetException ex) {
                throw new ClassCastException(ex.getMessage());
            }
            
        } else {
            this.attributes.put(attrName.toLowerCase(), (S) value);
        }
        
        
        setChanged(true,fireEv);
    }

    @Override
    public <S> S getAttr(String attrName, S defValue) {
        
        if (attrName == null) {
            return defValue;
        }        
        return (S) (this.attributes.containsKey(attrName.toLowerCase()) && this.attributes.get(attrName.toLowerCase()) != null ? 
                this.attributes.get(attrName.toLowerCase()) : defValue);
    }

    @Override
    public Map<String, S> getAttr() {
        HashMap<String, S> response = new HashMap<String, S>();
        Map<String, DmAttribute> attrs = DmFactory.getDmClassAttributes(this.getSubClass());
        
        for(String attrName: attrs.keySet()) {
            response.put(attrName, (S)getAttr(attrName,null));
        }
/*
        for(Map.Entry<String, DmAttribute> a: attrs.entrySet()) {
            response.put(a.getKey(), (S)getAttr(a.getKey(),null));
        }
*/
        return response;
    }

    @Override
    public <V> void setInsAttr(String attrName, V value) {
        if (attrName == null || attrName.isEmpty()) {
            throw new FmException("FEM010", "Unable to set class instance attribute value");
        }
        this.instanceAttributes.put(attrName, (S) value);
    }

    @Override
    public <V> V getInsAttr(String attrName, V defValue) {
        return attrName == null
                ? defValue
                : (V) (this.instanceAttributes.containsKey(attrName) && this.instanceAttributes.get(attrName) != null
                ? this.instanceAttributes.get(attrName) : defValue);
    }

    @Override
    public boolean isChanged() {
        return changed;
    }
    
    @Override
    public void setChanged(boolean ch,boolean fireEv) {
        changed = ch;
        updateTimestamp = System.currentTimeMillis();
        if(fireEv){
            fireEvent("onChange",null);
        }
    }
    
    /**
     * @return the updateTimestamp
     */
    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    // private
    public final void setProxy(DmObject proxy) {
        this.dmProxy = proxy;
    }

    public final DmObject getProxy() {
        return this.dmProxy;
    }

    @Override
    public Set<String> getAttributeNames() {
        Set<String> response = new HashSet<String>();
        Map<String, DmAttribute> attrs = DmFactory.getDmClassAttributes(this.getSubClass());
        
        for(Map.Entry<String, DmAttribute> a: attrs.entrySet()) {
            response.add(a.getKey());
        }

        return response;
    }

    // listeners 
    @Override
    public boolean addListener(Object receiver) {
        return listener.addListener(receiver);
    }

    @Override
    public boolean removeListener(Object receiver) {
        return listener.removeListener(receiver);
    }

    @Override
    public void removeAllListeners() {
        listener.removeAllListeners();
    }

    @Override
    public int getListenersCount() {
        return listener.getListenersCount();
    }

    @Override
    public List<T> getListeners() {
        return listener.getListeners();
    }

    @Override
    public boolean isListener(Object receiver) {
        return listener.isListener(receiver);
    }
    
    @Override
    public void fireEvent(String ev, Object evData) {
        listener.fireEvent(ev, evData);
    }


    /**
     * @param dmCls
     * @return 
     */    
    public List<Annotation> getDmClassAnnotations() {        
        List<Annotation> annLst = subClassAnnotations.get(getSubClass());
        
        // fill annotations
        if(annLst == null) {
            annLst = _getClassAnnotations(getSubClass());
            subClassAnnotations.put(getSubClass(),annLst);
        }
        
        return annLst;        
    }
        
    
    private static List<Annotation> _getClassAnnotations(Class cls) {  
        List<Annotation> annLst = Arrays.asList(cls.getAnnotations());
        
        // add annotations of implemented ifaces        
        for (Class<?> ifc : cls.getInterfaces()) {
            List<Annotation> ifcAnnLst = _getClassAnnotations(ifc);   // recursion!             
            for(Annotation anno: ifcAnnLst) {
                if(!annLst.contains(anno)) {
                    annLst.add(anno);                        
                }
            }
        }
        
        return annLst;
    }

    @Override
    public String toString() {
        return DmFactory.getDmKind(getSubClass()) +"(ID=" + getID() + ")";
    }
}
