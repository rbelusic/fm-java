package org.fm.dm;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author RobertoB
 */
public class DmProxyClass<V> implements InvocationHandler, Serializable {

    private DmObjectImpl object;
    private final List<Class> objectInterfaces = new ArrayList();

    public DmProxyClass(DmObjectImpl dmImpl) {
        this.object = dmImpl;
        addImplInterfaces(this.object.getClass(), objectInterfaces);
    }

    DmProxyClass(DmObjectImpl dmImpl, Map<String, V> populateWith) {
        this.object = dmImpl;
        addImplInterfaces(this.object.getClass(), objectInterfaces);
        if (populateWith != null) {
            for (Object key : populateWith.keySet()) {
                // TODO: check for getter existence
                this.object.setAttr(key.toString().toLowerCase(), populateWith.get(key.toString()));
            }
            //dump(this.object.getAttr());
        }
    }

    private void addImplInterfaces(Class c, final List<Class> lst) {        
        Class[] ifcs = c.getInterfaces();
        for(Class ifc: ifcs) {
            if(lst.contains(ifc)) continue;
            
            lst.add(ifc);
            addImplInterfaces(ifc, lst);
        }        
    }
    
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();

        if ("toString".equals(methodName)) {
            return this.object.toString();
        } else if ("equals".equals(methodName)) {
            return this.object.equals(args[0]);
        }


        // if dmobject is source
        if( objectInterfaces.contains(method.getDeclaringClass())){
        /* 
        
        if (method.getDeclaringClass().equals(DmObject.class) || 
                Arrays.asList(DmObject.class.getGenericInterfaces()).contains(method.getDeclaringClass())        
        ) {*/
            try {
                return method.invoke(this.object, args);
            } catch(InvocationTargetException e) {
                throw e.getCause();
            }
        }

        // getters & setters: get(defval); set(value)
        if (args != null && args.length == 1) {
            String attrName = DmAttribute.propertyNameFromMethodName(methodName);
            DmAttribute attrDef = DmFactory.getDmAttributeMetadata(this.object.getSubClass(), attrName);
            if (attrDef != null) {
                Object arg0 = args.length > 0 ? args[0] : null;
                if (methodName.startsWith("get") || methodName.startsWith("is")) {
                    if(attrDef.getStaticGetMethod() != null) {
                        try {
                        	return attrDef.getStaticGetMethod().invoke(null, object.getProxy(),arg0);
                            //return attrDef.getStaticGetMethod().invoke(null, object.getProxy());
                        } catch(InvocationTargetException e){
                            throw e.getCause();
                        }
                    } else {
                        return this.object.getAttr(attrName, arg0);
                    }
                } else if (methodName.startsWith("set") && method.getParameterTypes().length == 1) {
                    if(attrDef.getStaticSetMethod() != null) {
                        try {
                            attrDef.getStaticSetMethod().invoke(null, object.getProxy(),arg0);
                        } catch(InvocationTargetException e){
                            throw e.getCause();
                        }
                    } else {
                        this.object.setAttr(attrName, arg0);
                    }
                }
            }
        }

        // notfound exception!
        return null;
    }
}
