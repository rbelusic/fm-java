package org.fm.dm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author robertob
 */
public class DmUtil {
    public static <T extends DmObject> void listSort(List<T> list,Comparator<T> comparator) {
        Collections.sort(list, comparator);
    }
    

    public static <T extends DmObject> List<T> listNotIn(List<T> list, List<T> cmpList) {
        List<T> diffList = new ArrayList<T>();
        Map<Object, Object> cmpIdsMap = new HashMap<Object, Object>();

        for (T dm : cmpList) {
            cmpIdsMap.put(dm.getDataID(), "");
        }

        for (T dm : list) {
            if (!cmpIdsMap.containsKey(dm.getDataID())) {
                diffList.add(dm);
            }
        }

        return diffList;
    }

   public static <T extends DmObject> List<T> listIn(List<T> list, List<T> cmpList) {
        List<T> commonList = new ArrayList<T>();
        Map<Object, Object> cmpIdsMap = new HashMap<Object, Object>();

        for (T dm : cmpList) {
            cmpIdsMap.put(dm.getDataID(), "");
        }

        for (T dm : list) {
            if (cmpIdsMap.containsKey(dm.getDataID())) {
                commonList.add(dm);
            }
        }

        return commonList;
    }
    
    public static <T extends DmObject> HashMap<String, Object> dmToMap(T dm, ClassLoader classLoader) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.putAll(dm.getAttr());

        for (String attr : map.keySet()) {
            Object value = map.get(attr);
            if (value != null && DmObject.class.isAssignableFrom(value.getClass())) {
                map.put(attr, dmToMap((T) value, classLoader));
            } else {
                map.put(attr, value);
            }
        }
        map.put("@dmKindLong", DmFactory.getDmKindLong(dm.getSubClass()));
        return (map);
    }

    public static <T extends DmObject> T mapToDm(Map<String, Object> map, ClassLoader classLoader) throws ClassNotFoundException {

        Class<DmObject> cls = (Class<DmObject>) classLoader.loadClass((String) map.get("@dmKindLong"));
        T dm = dm = (T) DmFactory.create(cls);

        for (String attr : map.keySet()) {
            Object value = map.get(attr);
            if (value != null
                    && Map.class.isAssignableFrom(value.getClass())
                    && ((Map) value).get("@dmKindLong") != null) {
                dm.setAttr(attr, mapToDm((Map) value, classLoader));
            } else {
                dm.setAttr(attr, value);
            }
        }
        return dm;
    }

    
}
