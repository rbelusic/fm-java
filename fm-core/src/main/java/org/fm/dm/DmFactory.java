package org.fm.dm;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.dm.annotations.DmAttributeDef;
import org.fm.dm.annotations.DmDataIdAttribute;


/**
 *
 * @author RobertoB
 */
public final class DmFactory {

    private static HashMap<Class, HashMap<String, DmAttribute>> attrDef
            = new HashMap();

    public static <T extends DmObject> DmAttribute getDmAttributeMetadata(Class<T> cls, String name) {
        return getDmAttributeMetadata(cls, name, true);
    }

    public static <T extends DmObject> Map<String, DmAttribute> getDmClassAttributes(Class<T> cls) {
        HashMap<String, DmAttribute> attrs = attrDef.get(cls);

        if (attrs == null) {
            attrs = enumerateDmAttributes(cls);
            if (attrs != null) {
                attrDef.put(cls, attrs);
            }
        }

        return attrs;
    }

    public static <T extends DmObject> DmAttribute getDmAttributeMetadata(Class<T> cls, String name, boolean ignoreCase) {
        Map<String, DmAttribute> attrs = getDmClassAttributes(cls);

        // get
        DmAttribute attr = null;
        if (ignoreCase) {
            for (String key : attrs.keySet()) {
                if (key.equalsIgnoreCase(name)) {
                    attr = attrs.get(key);
                    break;
                }
            }
        } else {
            attr = attrs.get(name);
        }

        return attr;
    }

    private static void checkProxyAttributeMethods(DmAttribute attr, Method method) {
        DmAttributeDef attrann = getDmAttributeMethodAnnotation(method);
        if (attrann != null) {
            String getterName = attrann.getter();
            if (!getterName.isEmpty()) {
                String mClsName = getterName.substring(0, getterName.lastIndexOf("."));
                String mMethodName = getterName.substring(getterName.lastIndexOf(".") + 1);
                try {
                    Class mCls = Class.forName(mClsName);
                    for (Method sm : mCls.getDeclaredMethods()) {
                        if (sm.getName().equals(mMethodName) && sm.getParameterTypes().length == 2) {
                            //System.out.println("get static method found!");
                            attr.setStaticGetMethod(sm);
                        }
                    }
                } catch (ClassNotFoundException ex) {
                }
            }
            String setterName = attrann.setter();
            if (!setterName.isEmpty()) {
                String mClsName = setterName.substring(0, setterName.lastIndexOf("."));
                String mMethodName = setterName.substring(setterName.lastIndexOf(".") + 1);
                try {
                    Class mCls = Class.forName(mClsName);
                    for (Method sm : mCls.getDeclaredMethods()) {
                        if (sm.getName().equals(mMethodName) && sm.getParameterTypes().length == 2) {
                            //System.out.println("set static method found!");
                            attr.setStaticSetMethod(sm);
                        }
                    }
                } catch (ClassNotFoundException ex) {
                }
            }
        }
    }

    private static <T extends DmObject> HashMap<String, DmAttribute> enumerateDmAttributes(Class<T> cls) {
        HashMap<String, DmAttribute> results = new HashMap<String, DmAttribute>();

        Method[] methods = cls.getMethods();
        for (Method method : methods) {
            String mname = method.getName();
            Method dmObjectMethod = null;
            Class dmObjectAttrType = null;
            try {
                dmObjectMethod = DmObject.class.getMethod(mname, method.getParameterTypes());
            } catch (Exception ex) {
            }

            if (dmObjectMethod != null) {
                continue;
            }

            if ((mname.startsWith("get") || mname.startsWith("is") || mname.startsWith("set"))
                    && (method.getParameterTypes().length == 1)) {
                // System.out.println(" method: found!");

                if (dmObjectMethod == null) {
                    String attrname = DmAttribute.propertyNameFromMethodName(mname);
                    // System.out.println(" method: valid attribute " + attrname);
                    DmAttribute attr = results.get(attrname);
                    if (attr == null) {
                        // System.out.println(" method: new attribute " + attrname);
                        attr = new DmAttribute(attrname);
                        results.put(attrname, attr);
                    }
                    // System.out.println(" method: new add method " + mname);
                    attr.addMethod(mname);
                    attr.setMethodAnnotations(mname,method.getAnnotations());
                    
                    dmObjectAttrType = method.getParameterTypes()[0];
                    attr.setType(dmObjectAttrType);
                    
                    if(List.class.isAssignableFrom(dmObjectAttrType)) {
                        attr.setComponentType(method.getParameterTypes()[0].getComponentType());
                    }
                    checkProxyAttributeMethods(attr, method);                    
                }
            }
        }

        return results;
    }

    public static <T extends DmObject> T create(Class<T> dmObjectClass) {
        return create(dmObjectClass, null);
    }

    public static <T extends DmObject, V> T create(
            Class<T> dmObjectClass,
            Map<String, V> populateWith) {
        DmObjectImpl dmImpl = new DmObjectImpl(dmObjectClass);

        T proxy = (T) Proxy.newProxyInstance(
                DmFactory.class.getClassLoader(),
                new Class[]{dmObjectClass},
                new DmProxyClass(dmImpl, populateWith));

        dmImpl.setProxy(proxy);

        return proxy;
    }

    public static DmObject create() {
        return create(DmObject.class);
    }

    // static
    public static <T extends DmObject> String getDmKind(Class<T> dmObjectClass) {
        // TODO:dodati i anotaciju
        return dmObjectClass.getSimpleName();
    }

    public static <T extends DmObject> String getDmKindLong(Class<T> dmObjectClass) {
        // TODO:dodati i anotaciju
        return dmObjectClass.getName();
    }


    // static
    public static <T, A extends Annotation> A getDmAnnotation(
            Class<T> dmObjectClass,
            Class<A> annCls
    ) {
        A ann = dmObjectClass.getAnnotation(annCls);

        if (!annCls.isInstance(ann)) {
            for (Class<?> i : dmObjectClass.getInterfaces()) {
                ann = getDmAnnotation(i, annCls);
                if (annCls.isInstance(ann)) {
                    break;
                }
            }

        }
        return ann;
    }

    public static <T extends DmObject> String getDmDataIdAttribute(Class<T> dmObjectClass) {
        Annotation ann = getDmAnnotation(dmObjectClass, DmDataIdAttribute.class);

        if (!(ann instanceof DmDataIdAttribute)) {
            return ".";
        } else {
            return ((DmDataIdAttribute) ann).name();
        }
    }

    public static DmAttributeDef getDmAttributeMethodAnnotation(Method m) {
        Annotation annStoreCol = m.getAnnotation(DmAttributeDef.class);
        if (!(annStoreCol instanceof DmAttributeDef)) {
            return null;
        }

        return ((DmAttributeDef) annStoreCol);
    }

    public static String getAttrNameFromMethodName(String mname) {
        if (mname.startsWith("get") || mname.startsWith("is") || mname.startsWith("set")) {
            String attrName = mname.substring(mname.startsWith("is") ? 2 : 3);
            attrName = attrName.substring(0, 1).toLowerCase() + attrName.substring(1);
            return attrName;
        }
        return null;
    }
}
