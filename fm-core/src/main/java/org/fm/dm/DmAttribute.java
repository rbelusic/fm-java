package org.fm.dm;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author rbelusic
 */
public class DmAttribute {

    private String name;
    private Class type;
    private Map<String,Annotation[]> methodAnnotations = new HashMap<String,Annotation[]>();
    private Method staticGetMethod = null;
    private Method staticSetMethod = null;
    private boolean getMehodDefined=false;
    private boolean setMehodDefined=false;
    private boolean isMehodDefined=false;
    private Class<?> componentType;

    public DmAttribute(String aname) {
        name = aname;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the getMehodDefined
     */
    public boolean isGetMehodDefined() {
        return getMehodDefined;
    }

    /**
     * @param getMehodDefined the getMehodDefined to set
     */
    public void setGetMehodDefined(boolean getMehodDefined) {
        this.getMehodDefined = getMehodDefined;
    }

    /**
     * @return the setMehodDefined
     */
    public boolean isSetMehodDefined() {
        return setMehodDefined;
    }

    /**
     * @param setMehodDefined the setMehodDefined to set
     */
    public void setSetMehodDefined(boolean setMehodDefined) {
        this.setMehodDefined = setMehodDefined;
    }

    /**
     * @return the isMehodDefined
     */
    public boolean isIsMehodDefined() {
        return isMehodDefined;
    }

    /**
     * @param isMehodDefined the isMehodDefined to set
     */
    public void setIsMehodDefined(boolean isMehodDefined) {
        this.isMehodDefined = isMehodDefined;
    }
    
    
    
    public static String propertyNameFromMethodName(String methodName) {
        String name = methodName.substring(
                methodName.startsWith("get") || methodName.startsWith("set") ? 3
                : (methodName.startsWith("is") ? 2 : methodName.length()));
        
        return name.length() > 1
                ? name.substring(0, 1).toLowerCase() + name.substring(1)
                : name;
    }

    public void addMethod(String mname) {
        if(mname.startsWith("is")){
            this.setIsMehodDefined(true);
        } else if(mname.startsWith("get")){
            this.setGetMehodDefined(true);
        } else if(mname.startsWith("set")){
            this.setSetMehodDefined(true);
        }
    }

    /**
     * @return the staticGetMethod
     */
    public Method getStaticGetMethod() {
        return staticGetMethod;
    }

    /**
     * @param staticGetMethod the staticGetMethod to set
     */
    public void setStaticGetMethod(Method staticGetMethod) {
        this.staticGetMethod = staticGetMethod;
    }

    /**
     * @return the staticSetMethod
     */
    public Method getStaticSetMethod() {
        return staticSetMethod;
    }

    /**
     * @param staticSetMethod the staticSetMethod to set
     */
    public void setStaticSetMethod(Method staticSetMethod) {
        this.staticSetMethod = staticSetMethod;
    }

    /**
     * @return the type
     */
    public Class getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Class type) {
        this.type = type;
    }

    public Class<?> getComponentType() {
        return this.componentType;
    }
    
    public void setComponentType(Class<?> componentType) {
        this.componentType = componentType;
    }
    
    public void setMethodAnnotations(String mname, Annotation[] annotations) {
        this.methodAnnotations.put(mname.toLowerCase(), annotations);
    }

    public Annotation[] getMethodAnnotations(String mname) {
        return this.methodAnnotations.get(mname.toLowerCase());
    }
    
    public <T extends Annotation> T getMethodAnnotation(String mname, Class<T> annCls) {
        Annotation [] lst = getMethodAnnotations(mname);
        if(lst == null) {
            return null;
        }
        for(Annotation ann: lst) {
            if (annCls.isInstance(ann)) {            
                return (T)ann;
            }
        }
        
        return null;
    }

}
