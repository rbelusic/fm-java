package org.fm.dm;

import java.util.Map;
import java.util.Set;
import org.fm.FmDispatcher;

/**
 *
 * @author RobertoB
 */
public interface DmObject extends FmDispatcher {

    public Class getSubClass();

    public String getSubClassName();

    public long getUpdateTimestamp();
    
    /**
     * Sets dm object attribute value.
     */
    public <V> void setAttr(String attrName, V value);

    public <V> void setAttr(String attrName, V value, boolean fireEv);

    /**
     * Gets dm object attribute value.
     */
    public <V> V getAttr(String attrName, V defValue);

     public <V> Map<String, V> getAttr();

    public void setChanged(boolean ch,boolean fireEv);
    public boolean isChanged();

    /**
     * Gets all dm object attribute names.
     */
    public Set<String> getAttributeNames();

    /**
     * Check dm object attribute existance.
     */
    public boolean isAttr(String attrName);

    /**
     * Gets dm object id attribute value.
     */
    public String getDataID();

    /**
     * Gets dm object id attribute name.
     */
    public String getDataIDAttribute();

    /**
     * Set object instance attribute value
     * @param <V>
     * @param attrName
     * @param value 
     */
    public <V> void setInsAttr(String attrName, V value);

    /**
     * Set object instance attribute value
     * @param <V>
     * @param attrName
     * @param value 
     */    
    public <V> V getInsAttr(String attrName, V defValue);
}
