package org.fm.http;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.fm.FmObjectImpl;

/**
 *
 * @author rbelusic
 */
public class FmHttpService extends FmObjectImpl {
    public final static String APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8";
    public final static String APPLICATION_XML_UTF8 = "application/xml;charset=UTF-8";
    public final static String MULTIPART_FORM_DATA = "multipart/form-data";

    
    public String[] getIdsArray(String ids) {
        String[] idList = ids.split(",");
        ArrayList<String> retc = new ArrayList<String>();

        Set<String> lt = new HashSet<String>();
        for (String s : idList) {
            if (!lt.contains(s)) {
                lt.add(s);
                retc.add(s);
            }
        }

        return retc.toArray(new String[retc.size()]);
    }

}
