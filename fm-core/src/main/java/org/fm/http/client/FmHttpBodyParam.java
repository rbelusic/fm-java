package org.fm.http.client;


public class FmHttpBodyParam  extends FmHttpParam {
   public FmHttpBodyParam(
         String name,
         Object content,
         String contentType
         ) {
      super(name, content, contentType);
   }

   public FmHttpBodyParam(
         String name,
         String content,
         String contentType
         ) {
      super(name, content,contentType);
   }

}
