package org.fm.http.client;

import java.util.Map;

public interface FmHttpClient {

    public static enum REQUEST_TYPE {

        GET, POST, PUT, DELETE, OPTIONS, HEAD
    }

    public FmHttpResponse invoke(REQUEST_TYPE requestType, // GET, POST, PUT,DELETE,OPTIONS, HEAD
            String endpoint,
            String contentType,
            FmHttpParamList args,
            Map<String, String> headers
    ) throws Exception;

    public FmHttpResponse invoke(
            REQUEST_TYPE requestType,
            String endpoint, String contentType,
            FmHttpParamList args,
            Map<String, String> headers, Map<String, Object> cookies
    ) throws Exception;

    public <P extends FmHttpParam> FmHttpResponse put(
            String endpoint,
            String contentType,
            FmHttpParamList args,
            Map<String, String> headers
    ) throws Exception;

    public <P extends FmHttpParam> FmHttpResponse get(
            String endpoint,
            String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception;

    public <P extends FmHttpParam> FmHttpResponse post(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception;

    public <P extends FmHttpParam> FmHttpResponse delete(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception;

    public <P extends FmHttpParam> FmHttpResponse options(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception;

    public <P extends FmHttpParam> FmHttpResponse head(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception;
}
