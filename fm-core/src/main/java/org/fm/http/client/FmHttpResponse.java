package org.fm.http.client;

import java.util.Map;

/**
 *
 * @author RobertoB
 */
public class FmHttpResponse {
   private int responseCode;
   private byte[] response;
   private Map<String, Object> cookies;

   public FmHttpResponse(int code, byte[] rbody) {
	   this(code,rbody, null);
   }

   public FmHttpResponse(int code, byte[] rbody, Map<String, Object> clist) {
	      responseCode = code;
	      response = rbody;
	      cookies = clist;
	   }
   
   public int getResponseCode() {
      return responseCode;
   }

   public void setResponseCode( int responseCode ) {
      this.responseCode = responseCode;
   }

   public byte[] getResponse() {
      return response;
   }

   public void setResponse( byte[] response ) {
      this.response = response;
   }
   
   public Map<String, Object> getCookies() {
	      return cookies;
	   }

   public void setCookies( Map<String, Object> clist ) {
	   cookies = clist;
   }
}
