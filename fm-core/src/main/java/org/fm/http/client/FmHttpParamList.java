package org.fm.http.client;

import java.util.ArrayList;
import java.util.Map;

public class FmHttpParamList extends ArrayList<FmHttpParam> {

    public FmHttpParamList(Map<String, Object> params) {
        this();

        for (Map.Entry p : params.entrySet()) {
            add(new FmHttpParam((String) p.getKey(), p.getValue()));
        }
    }

    public FmHttpParamList() {
        super();
    }

}
