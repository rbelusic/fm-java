package org.fm.http.client;

import java.io.File;
import java.io.InputStream;
import org.fm.io.IoUtils;

/**
 *
 * @author RobertoB
 */
public class FmHttpParam {
   private String name;
   private Object content;
   String contentType;

   public FmHttpParam(
         String name,
         Object content,
         String contentType
         ) {
      this.name = name;
      this.content = content;
      this.contentType = contentType;
   }
   
   public FmHttpParam(
         String name,
         Object content
         ) {
       this(name, content, (content instanceof String ? "text/*" : "application/octet-stream"));
   }

   
   public  String getName() {
      return name;
   }

   public void setName( String name ) {
      this.name = name;
   }

   public Object getContent() {
      return content;
   }

   public void setContent( Object content ) {
      this.content = content;
   }

   public void setContent( String strContent ) {
      setContent( strContent.getBytes());
   }

   public void setContent( InputStream streamContent ) throws Exception {
      setContent( IoUtils.getInputStreamAsByteArray( streamContent ));
   }

   public void setContent( File fileContent ) throws Exception {
      setContent( IoUtils.getFileAsByteArray( fileContent ));
   }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }


    public boolean isStream() {
        return content instanceof InputStream;
    }

    public boolean isByteArray() {
        return content instanceof byte[];
    }
    
    public boolean isString() {
        return content instanceof String;
    }
    
    public boolean isTypeOf(Class c) {
        return c.isAssignableFrom(content.getClass());
    }
    
}
