/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fm.http.client;

import java.io.File;
import java.io.InputStream;



public class FmHttpFileParam extends FmHttpParam {
   private String fileName;

   public FmHttpFileParam(
         String name,
         String fileName,
         String contentType,
         byte [] content
         ) {
      super(name, content, contentType);
      this.fileName = fileName;
   }
   
   public FmHttpFileParam(
         String name,
         String fileName,
         String contentType,
            InputStream content
         ) {       
      super(name, content, contentType);
      this.fileName = fileName;
   }   

   public FmHttpFileParam(
         String name,
         String fileName,
         String contentType,
         File content
    ) {
      super(name, content,contentType);
      this.fileName = fileName;
   }
   
   public String getFileName() {
      return fileName;
   }

   public void setFileName( String fileName ) {
      this.fileName = fileName;
   }

   public String getContentType() {
      return contentType;
   }

   public void setContentType( String contentType ) {
      this.contentType = contentType;
   }      
}
