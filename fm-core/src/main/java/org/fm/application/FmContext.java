package org.fm.application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.FmException;
import org.fm.context.FmBean;
import org.fm.dm.DmObject;
import org.fm.proxy.GenericFactory;
import org.fm.proxy.GenericInterceptor;
import org.fm.utils.SystemUtil;

/**
 *
 * @author rbelusic
 */
public class FmContext {
    // -- logger ---------------------------------------------------------------
    private final static Logger L = getLogger(FmContext.class);  
    public static Logger getLogger(Class cls) {
        return  Logger.getLogger(cls.getName());
    }    

    // -- session beans --------------------------------------------------------
    private static class ThreadSession extends ThreadLocal<Map<Class<FmBean>, FmBean>> {
        @Override
        protected Map<Class<FmBean>, FmBean> initialValue() {
            return new HashMap<Class<FmBean>, FmBean>();
        }        
        
        public FmBean getBean(Class<FmBean> c) {
            if(get() == null) return null;
            return (FmBean) ((Map)get()).get(c);
        }
        public FmBean removeBean(Class<FmBean> c) {
            if(get() == null) return null;
            return (FmBean) ((Map)get()).remove(c);
        }
        
        public FmBean addBean(Class<FmBean> c, FmBean o) {
            if(get() == null) return null;
            return (FmBean) ((Map)get()).put(c,o);
        }        
    }
    
    private final static ThreadSession sessionBeans = new ThreadSession();
    
    public static <T extends FmBean> void addSessionBean(Class<T> c, T o) {
        sessionBeans.addBean((Class<FmBean>) c, o);
    }

    public static <T extends FmBean, I extends GenericInterceptor> void addSessionBean(Class<T> c, T o, I ic) {
        T proxy = GenericFactory.create(c, o, ic);
        sessionBeans.addBean((Class<FmBean>) c, proxy);
    }

    public static <T extends FmBean> T getSessionBean(Class<T> c) {
        return (T) sessionBeans.getBean((Class<FmBean>) c);
    }
    
    public static <T extends FmBean> T removeSessionBean(Class<T> c) {
        return (T) sessionBeans.removeBean((Class<FmBean>) c);
    }
    
    
    // -- application beans ----------------------------------------------------
    private static final Map<Class<FmBean>, FmBean> appBeans 
            = new HashMap<Class<FmBean>, FmBean>();    
    
    public static <T extends FmBean> void addAppBean(Class<T> c, T o) {
        appBeans.put((Class<FmBean>) c, o);
    }

    public static <T extends FmBean, I extends GenericInterceptor> void addAppBean(Class<T> c, T o,I ic) {
        T proxy = GenericFactory.create(c, o, ic);
        appBeans.put((Class<FmBean>) c, proxy);
    }

    public static <T extends FmBean> T getAppBean(Class<T> c) {
        return (T) appBeans.get(c);
    }
    
    public static <T extends FmBean> T removeAppBean(Class<T> c) {
        return (T) appBeans.remove(c);
    }
    
    // -- Dm -------------------------------------------------------------------
    private static final Map<String, Class<DmObject>> DM = new HashMap();
    
    public static <T extends DmObject> void addDmClassType(Map<String, Class<T>> map) {
        for (Map.Entry<String, Class<T>> e : map.entrySet()) {
            addDmClassType(e.getKey(), e.getValue());
        }
    }

    public static  <T extends DmObject> void addDmClassType(String type, Class<T> cls) {
        DM.put(type, (Class<DmObject>) cls);
    }

    public static <T extends DmObject> void addDmClassTypesFromPackages(String[] pkgs, Class<T> dmcls) {
        for(String pkg: pkgs) {            
            try {
                for(Class cls: SystemUtil.getClassesFromPackage(pkg, dmcls)) {
                        addDmClassType(cls.getSimpleName(), cls);
                }        
            } catch (Exception ex) {
                L.log(Level.SEVERE, null, ex);
            }
        }
    }

    public static <T extends DmObject> List<Class<T>> getDmClassTypes() {
        List<Class<T>> lst = new ArrayList<Class<T>>();

        for (Map.Entry<String, Class<DmObject>> prop : DM.entrySet()) {
            lst.add((Class<T>) prop.getValue());
        }
        return lst;
    }

    public static <T extends DmObject> Class<T> getDmClassOfType(String type) {
        return (Class<T>) DM.get(type);
    }
    
    // -- properties -----------------------------------------------------------
    public static Properties loadProperties(String name) {
        Properties props = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(name);
        } catch (FileNotFoundException ex) {
            input = FmContext.class.getClassLoader().getResourceAsStream(name);
        }

        try {
            props.load(input);
            return props;
        } catch (Exception e) {
            throw new FmException("FE0001", "Error loading properties from [" + name + "].", e);
        }
    }
    
}
