package org.fm.application;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.FmEventsDispatcher;
import org.fm.FmException;
import org.fm.bo.BoFactory;
import org.fm.bo.BoObject;
import org.fm.context.FmBean;
import org.fm.dao.DaoFactory;
import org.fm.dao.DaoObject;
import org.fm.dm.DmObject;
import org.fm.proxy.GenericInterceptor;
import org.fm.utils.StringUtil;
import org.fm.utils.SystemUtil;

/**
 *
 * @author rbelusic
 */
public class FmApplication extends FmEventsDispatcher {

    private final static Logger L = Logger.getLogger(FmApplication.class.getName());
    private String applicationName="fmapp-" + Thread.currentThread().getName();
       

    static FmApplication instanced = null;
    public static <A extends FmApplication> FmApplication instance() {
        return instanced;
    }
    
    public static <A extends FmApplication> FmApplication instance(Class<A> cls, String name) throws InstantiationException, IllegalAccessException {
        if(instanced == null) {
            instanced = cls.newInstance();
            instanced.setApplicationName(name);
            instanced.init();
        }
        return instanced;
    }
    
    public FmApplication() {
        super();
    }
    
    public void setApplicationName(String appName) {
        applicationName = appName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    // --DM, DAO, BO beans (APP) -----------------------------------------------
    private  <D extends DaoObject, B extends BoObject, I extends GenericInterceptor> void init() {
        // dm
        String dmPkgsList = getConfigProperty("fm.dm.packages");
        if (dmPkgsList != null && !dmPkgsList.isEmpty()) {
            String[] pkgs = (String[]) StringUtil.tokenizeString(dmPkgsList, ';', '"');
            FmContext.addDmClassTypesFromPackages(pkgs, DmObject.class);
        }

        // dao
        String icDaoClsName = getConfigProperty("fm.dao.interceptor");
        I icDaoCls;
        try {
            icDaoCls = (I) ((icDaoClsName != null
                    && !icDaoClsName.isEmpty()) ? Class.forName(icDaoClsName).newInstance()
                            : null);
        } catch (Exception ex) {
            icDaoCls = null;
            L.log(Level.SEVERE, null, ex);
        }

        String daoPkgsList = getConfigProperty("fm.dao.packages");
        if (daoPkgsList != null && !daoPkgsList.isEmpty()) {
            String[] pkgs = (String[]) StringUtil.tokenizeString(daoPkgsList, ';', '"');
            addDaoFromPackage(pkgs, icDaoCls);
        }

        // bo
        String icBoClsName = getConfigProperty("fm.bo.interceptor");
        I icBoCls;
        try {
            icBoCls = (I) ((icBoClsName != null
                    && !icBoClsName.isEmpty()) ? Class.forName(icBoClsName).newInstance()
                            : null);
        } catch (Exception ex) {
            Logger.getLogger(FmApplication.class.getName()).log(Level.SEVERE, null, ex);
            icBoCls = null;
        }

        String boPkgsList = getConfigProperty("fm.bo.packages");
        if (boPkgsList != null && !boPkgsList.isEmpty()) {
            String[] pkgs = (String[]) StringUtil.tokenizeString(boPkgsList, ';', '"');
            addBoFromPackage(pkgs, icBoCls);            
        }
    }

    public <B extends BoObject, I extends GenericInterceptor> void addBoFromPackage(String[] pkgs, I boInterceptor) {
        for (String pkg : pkgs) {
            try {
                for (Class<B> cls : SystemUtil.getClassesFromPackage(pkg, BoObject.class)) {
                    if (!cls.isInterface()) {
                        for (Class<?> ifc : cls.getInterfaces()) {
                            if (ifc.equals(BoObject.class) || Arrays.asList(ifc.getInterfaces()).contains(BoObject.class)) {
                                setBo(
                                        (Class<B>) ifc,
                                        cls.newInstance(),
                                        boInterceptor
                                );
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                L.log(Level.SEVERE, null, ex);
            }
        }
    }              
        
    public <T extends BoObject, I extends GenericInterceptor> void setBo(Class<T> c, T o, I ic) {
        FmContext.addAppBean(c, BoFactory.create(c, o, ic));
    }

    public <T extends BoObject, I extends GenericInterceptor> void setBo(Class<T> c, T o) {
        FmContext.addAppBean(c, BoFactory.create(c, o));
    }

    public <T extends BoObject> T getBo(Class<T> c) {
        return FmContext.getAppBean(c);
    }
    
    public <D extends DaoObject, I extends GenericInterceptor> void addDaoFromPackage(String[] pkgs, I daoInterceptor) {
        for (String pkg : pkgs) {
            try {
                for (Class<D> cls : SystemUtil.getClassesFromPackage(pkg, DaoObject.class)) {
                    if (!cls.isInterface()) {
                        for (Class<?> ifc : cls.getInterfaces()) {
                            if (ifc.equals(DaoObject.class) || Arrays.asList(ifc.getInterfaces()).contains(DaoObject.class)) {
                                setDao(
                                        (Class<D>) ifc,
                                        cls.newInstance(),
                                        daoInterceptor
                                );
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                L.log(Level.SEVERE, null, ex);
            }
        }
    }    

    public <T extends DaoObject, I extends GenericInterceptor> void setDao(Class<T> c, T o, I ic) {
        FmContext.addAppBean(c, DaoFactory.create(c, o, ic));
    }

    public <T extends DaoObject, I extends GenericInterceptor> void setDao(Class<T> c, T o) {
        FmContext.addAppBean(c, DaoFactory.create(c, o));
    }

    public <T extends DaoObject> T getDao(Class<T> c) {
        return (T) FmContext.getAppBean(c);
    }

    // --beans (SESSION) -------------------------------------------------------
    public <T extends FmBean> void setCtx(Class<T> c, T o) {
        FmContext.addSessionBean(c, o);
    }

    public <T extends FmBean> T getCtx(Class<T> c) {
        return (T) FmContext.getSessionBean((Class<FmBean>) c);
    }
    
    public <T extends FmBean> T removeCtx(Class<T> c) {
        return (T) FmContext.removeSessionBean((Class<FmBean>) c);
    }
    

    // -- properties -----------------------------------------------------------
    private static Properties appProperties = null;
    private static Properties sysProperties = null;
    private static Properties buildProperties = null;

    public String getApplicationProperty(String key) {
        return _getProperty(getApplicationConfig(), key);
    }
    
    private void setApplicationProperty(String key, String value) {
        getApplicationConfig().setProperty(key, value);
    }
    
    public String getConfigProperty(String key) {
        return _getProperty(getSystemConfig(), key);
    }
    
    private void setConfigProperty(String key, String value) {
        getSystemConfig().setProperty(key, value);
    }
    
    public String getBuildProperty(String key) {
        return _getProperty(getBuildInfo(), key);
    }
        
    private void setBuildProperty(String key, String value) {
        getBuildInfo().setProperty(key, value);
    }
        
    private String _getProperty(Properties p, String key) {
        return (
            !p.containsKey(key) ? 
            System.getProperty(key, "") : 
            (String)p.get(key)
        );
    }
    
    public Properties getBuildInfo() {
        if (buildProperties == null) {
            // prop file name
            String propFileName = "build.properties";
            try {
                L.log(Level.INFO, "Loading build info from [{0}] ...", propFileName);
                buildProperties = FmContext.loadProperties(propFileName);
                L.log(Level.INFO, "Build info from [{0}] loaded.", propFileName);
            } catch (FmException e) {
                L.throwing(FmApplication.class.getName(), "getBuildInfo", e);
                L.severe("Build info load error!");
                buildProperties = new Properties();
            }
        }

        return buildProperties;        
    }
    
    public Properties getSystemConfig() {
        if (sysProperties == null) {
            // prop file name
            String propFileName = "fm.properties";
            try {
                L.log(Level.INFO, "Loading system properties from [{0}] ...", propFileName);
                sysProperties = FmContext.loadProperties(propFileName);
                L.log(Level.INFO, "Application properties from [{0}] loaded.", propFileName);
            } catch (FmException e) {
                L.throwing(FmApplication.class.getName(), "getSystemConfig", e);
                L.severe("System properties load error!");
                sysProperties = new Properties();
            }
        }

        return sysProperties;
        
    }
    
    public Properties getApplicationConfig() {
        if (appProperties == null) {
            // prop file name
            String propFileName = getApplicationName() + ".properties";

            // prop file path candidates
            String homePropFile = System.getProperty("user.home")
                    + File.separator
                    + ".fm"
                    + File.separator
                    + propFileName;
            String etcPropFile = File.separator
                    + "etc"
                    + File.separator
                    + "fm"
                    + File.separator
                    + propFileName;

            String propPath = new File(homePropFile).isFile() ? homePropFile
                    : (new File(etcPropFile).isFile() ? etcPropFile
                            : (new File(propFileName).isFile() ? propFileName
                                    : "fm.properties"));

            try {
                L.log(Level.INFO, "Loading application properties from [{0}] ...", propPath);
                appProperties = FmContext.loadProperties(propPath);
                L.log(Level.INFO, "Application properties from [{0}] loaded.", propPath);
            } catch (FmException e) {
                L.throwing(FmApplication.class.getName(), "getApplicationConfig", e);
                L.log(Level.SEVERE,"Application properties load error!");
                appProperties = new Properties();
            }
        }

        return appProperties;
    }    
}
