package org.fm.application;

import java.util.HashMap;
import java.util.Map;
import org.fm.FmEventsDispatcher;
import org.fm.FmException;
import org.fm.context.FmBean;
import org.fm.http.client.FmHttpClient;
import org.fm.store.Storage;
import org.fm.store.StorageConnection;

public class FmSessionContext<V> extends FmEventsDispatcher implements FmBean {
    private Map<String, V> attributes = new HashMap<String, V>();
    private StorageConnection storageConnection;
    private boolean storageConnectionLoaded = false;

    public void destroy() {
        closeStorageConnection();
    }
    
    public boolean isAttr(String attrName) {
        return attributes.containsKey(attrName);
    }

    public void setAttr(String attrName, V value) {
        setAttr(attrName, value, true);
    }
    
    public void setAttr(String attrName, V value, boolean fireEv) {
        if (attrName == null || attrName.isEmpty()) {
            throw new FmException("FEM010", "Unable to set attribute value, invalid attribute name (" + attrName + ")");
        }
        this.attributes.put(attrName, value);        
        if(fireEv){
            fireEvent("onChange",attrName);
        }
    }

    public V getAttr(String attrName, V defValue) {        
        if (attrName == null) {
            return defValue;
        }        
        return (V) (this.attributes.containsKey(attrName) ? this.attributes.get(attrName) : defValue);
    }

    public Map<String, V> getAttr() {
        return attributes;
    }
    
    public <T extends StorageConnection> StorageConnection getStorageConnection() {
        FmApplication app = FmApplication.instance();

        if (!storageConnectionLoaded) {
            storageConnectionLoaded = true;
            Class<T> conClass = null;
            String driver = app == null ? "" : app.getApplicationProperty("fm.store.driver");
            try {
                conClass = (Class<T>) Class.forName(driver);
            } catch (Exception ex) {
                throw new FmException("FEM001", "Driver [" + driver + "] not found.", ex);
            }
            storageConnection = Storage.instance(conClass);
        }

        return storageConnection;
    }

    public void closeStorageConnection() {
        if (storageConnectionLoaded && storageConnection != null) {
            storageConnectionLoaded = false;
            storageConnection.dispose();
        }

    }

    public <T extends FmHttpClient> T getHttpClientInstance() {
        FmApplication app = FmApplication.instance();
        Class<T> httpClass = null;
        String driver = app == null ? "" : app.getConfigProperty("fm.http.driver");
        if (driver == null || driver.isEmpty()) {
            driver = "org.fm.http.client.impl.FmApacheHttpClient";
        }

        try {
            httpClass = (Class<T>) Class.forName(driver);

        } catch (Exception ex) {
            throw new FmException("FEM001", "Driver [" + driver + "] not found.", ex);
        }
        try {
            return httpClass.newInstance();
        } catch (InstantiationException ex) {
            throw new FmException("FEM001", "Unable to create driver instance.", ex);
        } catch (IllegalAccessException ex) {
            throw new FmException("FEM001", "Unable to create driver instance.", ex);
        }
    }



}
