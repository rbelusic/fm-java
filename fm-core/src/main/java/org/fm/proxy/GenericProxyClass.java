package org.fm.proxy;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


public class GenericProxyClass implements InvocationHandler, Serializable {        
    private final Object object;
    private final List<GenericInterceptor> interceptors = new ArrayList<GenericInterceptor>();
    
    public GenericProxyClass(Object oImpl) {
        this.object = oImpl;
    }
    
    public <T extends GenericInterceptor> GenericProxyClass(Object oImpl, T ic) {
        this.object = oImpl;
        if(ic != null) this.interceptors.add(ic);
    }

    public <T extends GenericInterceptor> GenericProxyClass(Object oImpl, List<T> ics) {
        this.object = oImpl;
        if(ics != null) this.interceptors.addAll(ics);
    }

    public <T extends GenericInterceptor> void addInterceptor(T ic) {
        interceptors.add(ic);
    }
    
    public <T extends GenericInterceptor> void removeInterceptor(T ic) {
        interceptors.remove(ic);
    }
    
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable  {
        for(GenericInterceptor ic: interceptors) {
            ic.before(method, args);
        }
        Object result;
        try {
            result = method.invoke(this.object, args);
        } catch(InvocationTargetException e) {
            result = e.getCause();
        }
        
        for(GenericInterceptor ic: interceptors) {
            ic.after(method, args,result);
        }
        if(result instanceof Throwable) {
            throw (Throwable)result;
        }
        return result;
    }
}
