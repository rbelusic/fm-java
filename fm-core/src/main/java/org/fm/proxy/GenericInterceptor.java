package org.fm.proxy;

import java.lang.reflect.Method;

public interface GenericInterceptor {
    public void before(Method method, Object[] args);
    public void after(Method method, Object[] args, Object response);
    
}
