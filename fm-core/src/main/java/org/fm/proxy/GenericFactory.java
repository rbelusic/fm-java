package org.fm.proxy;

import java.lang.reflect.Proxy;

/**
 *
 * @author robertob
 */
public class GenericFactory {

    public static <T> T create(Class<T> boIfc, T boImpl) {

        T proxy = (T) Proxy.newProxyInstance(GenericFactory.class.getClassLoader(),
                new Class[]{boIfc},
                new GenericProxyClass(boImpl));

        return proxy;
    }

    public static <T> T create(Class<T> boIfc, T boImpl, GenericInterceptor ic) {

        T proxy = (T) Proxy.newProxyInstance(GenericFactory.class.getClassLoader(),
                new Class[]{boIfc},
                new GenericProxyClass(boImpl,ic));

        return proxy;
    }

}
