package org.fm.io;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import org.fm.FmEventsDispatcher;

public class FilesystemMonitor extends FmEventsDispatcher {

    private final Timer mTimer;
    private final HashMap<File,Long> mFiles = new HashMap<File,Long>();

    //
    public FilesystemMonitor(long pollingInterval) {
        mTimer = new Timer(true);
        mTimer.schedule(new FileMonitorNotifier(), 0, pollingInterval);
    }

    public void stop() {
        mTimer.cancel();
    }

    public void addFsObject(File file) {
        if (!mFiles.containsKey(file)) {
            long modifiedTime = file.exists() ? file.lastModified() : -1;
            mFiles.put(file,modifiedTime);
        }
    }

    public void removeFile(File file) {
        mFiles.remove(file);
    }

    // -------------------------------------------------------------------------
    private class FileMonitorNotifier extends TimerTask {

        public void run() {
            ArrayList<File> aFiles = new ArrayList<File>(mFiles.keySet());
            for (File file: aFiles) {
                long lastModifiedTime = mFiles.get(file);
                long newModifiedTime = file.exists() ? file.lastModified() : -1;

                if (newModifiedTime != lastModifiedTime) {
                    mFiles.put(file, newModifiedTime);
                    fireEvent("onFileChanged", file);
                }
            }
        }
    }
}
