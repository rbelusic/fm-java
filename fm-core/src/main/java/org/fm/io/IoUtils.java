package org.fm.io;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author RobertoB
 */
public class IoUtils {

    public static byte[] getInputStreamAsByteArray(InputStream ins) throws Exception {
        if (ins == null) {
            return null;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int chunk = 4096;
        byte[] buf = new byte[chunk];
        int procitano;
        try {
            while ((procitano = ins.read(buf, 0, chunk)) > 0) {
                baos.write(buf, 0, procitano);
            }
            return baos.toByteArray();
        } finally {
            try {
                baos.close();
            } catch (IOException ioe) {
            }
        }
    }

    public static byte[] getFileAsByteArray(File dat) throws Exception {
        if (dat == null) {
            return null;
        }
        FileInputStream fins = null;

        try {
            fins = new FileInputStream(dat);
            return getInputStreamAsByteArray(fins);
        } finally {
            try {
                if (fins != null) {
                    fins.close();
                }
            } catch (IOException ioe) {
            }
        }
    }

}
