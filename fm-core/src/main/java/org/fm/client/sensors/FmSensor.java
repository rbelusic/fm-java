/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.client.sensors;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import org.fm.FmEventsDispatcher;
import org.fm.filters.FmFilter;

/**
 *
 * @author rbelusic
 */
public abstract class FmSensor extends FmEventsDispatcher {
    public enum SENSOR_TYPE {
        ACCELEROMETER,GRAVITY,MAGNETIC_FIELD, CUSTOM;
    };
    public static final int SENSOR_STATUS_UNRELIABLE = 0;
    public static final int SENSOR_STATUS_ACCURACY_LOW = 1;
    public static final int SENSOR_STATUS_ACCURACY_MEDIUM = 2;
    public static final int SENSOR_STATUS_ACCURACY_HIGH = 3;
    
    
    public final static String EVENT = "onSensorChanged";

    private final ArrayList<FmFilter> filters = new ArrayList<FmFilter>();    
    private final SENSOR_TYPE sensorType;    

    public static <T extends FmSensor>  T instance(Class<T> sensorClass,Object owner,FmSensor.SENSOR_TYPE stype) {
        try {
            Constructor c = sensorClass.getConstructor(Object.class,FmSensor.SENSOR_TYPE.class);
            T sensor = (T) c.newInstance(owner,stype);
            return sensor;
        } catch (Exception ex) {
            return null;
        }
        
    }
    public FmSensor(Object owner,FmSensor.SENSOR_TYPE stype) {
        super(owner);
        sensorType = stype;
    }

    /**
     * @return the sensorType
     */
    public SENSOR_TYPE getSensorType() {
        return sensorType;
    }
    
    
    public void addFilter(FmFilter f) {
        filters.add(f);
    }
    public void removeFilter(FmFilter f) {
        filters.remove(f);
    }
    
    public double [] filterSensorValues(double [] svalues) {
        double[] values = svalues.clone();
        
        for(FmFilter f: filters) {
            values = f.filter(values, values.length);
        }
        return values;
    }

    public abstract void start();
    public abstract void stop();
}
