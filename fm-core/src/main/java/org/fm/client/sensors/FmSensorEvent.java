/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.client.sensors;

import org.fm.FmObjectImpl;

/**
 *
 * @author rbelusic
 */
public class FmSensorEvent extends FmObjectImpl {

    public final String eventType;
    public final float[] values;
    public final int accuracy;
    public final long timestamp;

    public FmSensorEvent(String et, float[] v, int acc, long ts) {
        eventType = et;
        values = v;
        accuracy = acc;
        timestamp = ts;
    }
}
