/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.filters;

import org.fm.FmObjectImpl;

public abstract class FmFilter extends FmObjectImpl {
    public abstract double filter(double v, long tstamp);
    public abstract double [] filter(double [] v, long tstamp);    
}