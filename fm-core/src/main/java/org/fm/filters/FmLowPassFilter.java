package org.fm.filters;

public class FmLowPassFilter extends FmFilter {

    private final double[] currentValues;
    private long lastUpdate;
    private double smoothing = 1D;
    private final long maxDeltaTime;

    public FmLowPassFilter(double smfactor, int ssize, long maxdt) { // 1 - nista, > 0
        smoothing = (double) (smfactor == 0.0 ? 1.0 : smfactor);
        maxDeltaTime = maxdt;

        currentValues = new double[ssize];
        lastUpdate = -1;
    }

    @Override
    public double filter(double v, long tstamp) {
        if (lastUpdate == -1) {
            currentValues[0] = v;
        } else {
            currentValues[0] =
                    tstamp - lastUpdate > maxDeltaTime
                    ? v
                    : currentValues[0] + smoothing * (v - currentValues[0]);
        }
        lastUpdate = tstamp;
        return currentValues[0];
    }

    @Override
    public double[] filter(double[] v, long tstamp) {
        int len = v.length > currentValues.length ? currentValues.length : v.length;
        for (int i = 0; i < len; i++) {
            if (lastUpdate == -1) {
                currentValues[i] = v[i];
            } else {
                currentValues[i] =
                        tstamp - lastUpdate > maxDeltaTime
                        ? v[i]
                        : currentValues[i] + smoothing * (v[i] - currentValues[i]);
            }
        }
        lastUpdate = tstamp;

        return currentValues.clone();
    }
}