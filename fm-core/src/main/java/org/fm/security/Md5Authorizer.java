package org.fm.security;

import java.security.MessageDigest;
import org.fm.utils.ByteArrayUtil;

public class Md5Authorizer implements Authorizer {
    public boolean authorize(byte[] passwordDigest,String userPassword) {
        return(ByteArrayUtil.compare(passwordDigest,createMd5sum(userPassword)));
    }
    
    
    public static byte[] createMd5sum(String password) {
        byte[] digest;
        MessageDigest algorithmEnc;
        try{
            algorithmEnc= MessageDigest.getInstance("MD5");
            // ispraznimo algorithmEnc
            algorithmEnc.reset();
            // updateamo
            algorithmEnc.update(password.getBytes());
            // upisujemo md5
            digest = algorithmEnc.digest();
            // vracamo vrijednost
            return digest;
        } catch (Exception e) {
            return null;
        }
    }
}