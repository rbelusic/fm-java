package org.fm.security;

public interface Authorizer {
    public boolean authorize(byte[] passwordDigest,String userPassword);
}

