package org.fm.ml.observers;

import org.fm.FmException;
import org.fm.FmObject;
import org.fm.ml.hosts.MlHost;

public interface MlObserver extends FmObject {
    // call after ui component is added to gui container
    public void init();
    
    // called from mlhost after run
    public <H extends MlHost> void run(H host);
    
    // called from mlhost on dmobject change event
    public  <H extends MlHost> void update(H caller);

    // called from mlhost after observer is added
    public <H extends MlHost> void setHost(H h);

    public String getAttributeName();
    
    public boolean isExecuted();
    
    public void dispose();

    public <H extends MlHost> H getHost();
    
    public  Object getValue();

    public void verify() throws FmException;
    
    public void sendEventToHost(String ev,Object evdata);
    
    public Object getNode();
}
