package org.fm.ml.hosts;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import org.fm.FmEventsDispatcher;
import org.fm.FmException;
import org.fm.application.FmApplication;
import org.fm.dm.DmObject;
import org.fm.ml.observers.MlObserver;

public class MlHost extends FmEventsDispatcher {
    private final FmApplication application;
    private DmObject dmObject = null;
    private final Object node;
    
    private MlHost masterHost = null;
    private final ArrayList<MlObserver> observers = new ArrayList<MlObserver>();

    public static <H extends MlHost> MlHost instance(Class<H> hostCls, FmApplication app, DmObject dm, Object node)                 
        throws InstantiationException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Constructor c = hostCls.getConstructor(FmApplication.class, DmObject.class, Object.class);
        H h = (H) c.newInstance(app,dm,node);
        return h;
    }

    public MlHost(FmApplication app, DmObject obj, Object component) {
        application = app;
        node = component;
        chooseDmObject(obj);        
    }

    public void run() {
        addListener(getApplication());
        getApplication().addListener(this);
        setOwner(this); // event owner

        
        runAllObservers();
    }

    private void chooseDmObject(DmObject obj) {
        if(obj == null && getMasterHost() != null) {
            obj = (DmObject)getMasterHost().getDmObject();
        }
        
        setDmObject(obj);
    }

    private ArrayList<FmException> runAllObservers() {
        ArrayList<FmException> errors = null;

        for (MlObserver o : observers) {
            try {
                o.run(this);
            } catch (Exception e) {
                if (errors == null) {
                    errors = new ArrayList<FmException>();
                }
                FmException fme = e instanceof FmException
                        ? (FmException) e : new FmException("FEF010", "Error running observer", e);
                fme.setSource(o);
                errors.add(fme);
            }

        }

        return errors;
    }

    public boolean isExecuted() {
        return getOwner() != null;
    }

    public <O extends MlObserver> boolean addObserver(O o) {
        if (!observers.contains(o)) {
            observers.add(o);
            if (isExecuted()) {
                o.run(this);
            } else {
                o.setHost(this);
            }
            return true;
        }
        return false;
    }

    public <O extends MlObserver> void removeObserver(O o) {
        if (observers.contains(o)) {
            observers.remove(o);
        }
    }

    public <O extends MlObserver> void updateObserver(O o) {
        if (isExecuted() && o.isExecuted()) {
            o.update(this);
        }
    }

    public void updateAllObservers() {
        for (MlObserver o : observers) {
            updateObserver(o);
        }
    }

    public FmException verifyObserver(MlObserver o) {
        try {
            o.verify();
        } catch (FmException e) {
            e.setSource(o);
            return e;
        }

        return null;
    }

    public ArrayList<FmException> verifyAllObservers(boolean force) {
        ArrayList<FmException> errors = null;

        for (MlObserver o : observers) {
            FmException e = verifyObserver(o);
            if (e != null) {
                e.setSource(o);
                if (errors == null) {
                    errors = new ArrayList<FmException>();
                    errors.add(e);
                }
                if (!force) {
                    return errors;
                }
            }
        }

        return errors;
    }

    public void sendEventToObservers(Object eventSource, String ev, Object data) {
        for (MlObserver o : observers) {
            FmEventsDispatcher.fireEvent(eventSource, o, ev, data);
        }
    }
    
    public void onObserverEvent(MlObserver obs,String ev,Object evdata) {

        // find ev method in host node (swing class)
        try {
            Class[] args = {Object.class, Object.class};
            Method method = getNode().getClass().getMethod(ev, args);
            method.invoke(getNode(), new Object[]{obs, evdata});
            return;
        } catch (Exception e) {
            //Logger.getLogger(FmEventsDispatcher.class.getName()).log(Level.SEVERE, null, e);
        } 
        
        // find ev method in host
        try {
            Class[] args = {Object.class, Object.class};
            Method method = getClass().getMethod(ev, args);
            method.invoke(this, new Object[]{obs, evdata});
            return;
        } catch (Exception e) {
            //Logger.getLogger(FmEventsDispatcher.class.getName()).log(Level.SEVERE, null, e);
        }

        // fire event otherwise
        fireEvent(ev, evdata);
    }

    public void dispose() {
        ArrayList<MlObserver> olst = (ArrayList<MlObserver>) observers.clone();

        for (MlObserver o : olst) {
            o.dispose();
        }
        fireEvent("onDispose", null);
        removeAllListeners();
        getApplication().removeListener(this);
        setOwner(null); // event owner
        if (getMasterHost() != null) {
            getMasterHost().removeListener(this);
            setMasterHost(null);
        }
        setDmObject(null);
    }

    public DmObject getDmObject() {
        return dmObject;
    }

    public void setDmObject(DmObject o) {
        if (dmObject != null) {
            dmObject.removeListener(this);
        }
        dmObject = o;
        if(dmObject != null) dmObject.addListener(this);
        updateAllObservers();
        fireEvent("onSetDmObject", dmObject);
    }

    public void onSetDmObject(Object sender, Object args) {
        if (sender == getMasterHost()) {
            chooseDmObject((DmObject) args);
        }
    }

    public void onChange(Object sender, Object args) {
        if (sender.equals(getDmObject())) {
            updateAllObservers();
        }
    }

    /**
     * @return the application
     */
    public FmApplication getApplication() {
        return application;
    }
    
    /**
     * @return the node
     */
    public Object getNode() {
        return node;
    }
    

    /**
     * @return the masterHost
     */
    public MlHost getMasterHost() {
        return masterHost;
    }

    /**
     * @param mHost
     * @param masterHost the masterHost to set
     */
    public void setMasterHost(MlHost mHost) {
        if (getMasterHost() != null) {
            masterHost.removeListener(this);
        }

        masterHost = mHost;
        masterHost.addListener(this);
        chooseDmObject(null);
    }
}
