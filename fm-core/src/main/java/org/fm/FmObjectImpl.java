package org.fm;

import java.util.UUID;

public class FmObjectImpl implements FmObject {
    private String id = null;
    
    @Override
    public String getID() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
        return (id);

    }    
}
