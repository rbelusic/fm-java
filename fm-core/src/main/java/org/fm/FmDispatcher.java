package org.fm;

import java.util.List;

public interface FmDispatcher extends FmObject {
    public <T> boolean addListener(T receiver);
    public <T> void fireEvent(String ev, T evData);
    public void removeAllListeners();
    public <T> boolean removeListener(T receiver);
    public int getListenersCount();
    public <T> List<T> getListeners();
    public <T> boolean isListener(T receiver);
}
