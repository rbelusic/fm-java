package org.fm.impl.swing.ml.observers;

import org.fm.impl.swing.ml.MlObserverSwingComponent;
import org.fm.impl.swing.ml.MlObserverSwing;
import java.awt.Container;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;

public class MlJButton extends JButton implements MlObserverSwingComponent, ActionListener {
    final Border raisedBevelBorder = BorderFactory.createLineBorder(getForeground());
    final Insets insets = raisedBevelBorder.getBorderInsets(this);
    final EmptyBorder emptyBorder = new EmptyBorder(insets);

    private final MlObserverSwing observer;
    private String attributeName;
    private String eventName;
    private long updateTimestamp = 0;

    public MlJButton(String text, Icon icon) {
        super(text, icon);
        _swing_init();
        setActionCommand("click");
        addActionListener(this);
        observer = new MlObserverSwing(this);
    }

    public MlJButton(Icon icon) {
        super(icon);
        _swing_init();
        setActionCommand("click");
        addActionListener(this);
        observer = new MlObserverSwing(this);
    }

    public MlJButton(String text) {
        super(text);
        _swing_init();
        setActionCommand("click");
        addActionListener(this);
        observer = new MlObserverSwing(this);
    }

    public MlJButton() {
        super();
        _swing_init();
        setActionCommand("click");
        addActionListener(this);
        observer = new MlObserverSwing(this);
    }

    private void _swing_init() {
    getModel().addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            ButtonModel model = (ButtonModel) e.getSource();
            if (model.isRollover()) {
                setBorder(raisedBevelBorder);
            } else {
                setBorder(emptyBorder);
            }
        }
    });
    setBorder(emptyBorder);
    }
    public void init() {
        observer.init();
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }

    // MlObserverSwingComponent
    public void update(MlHost caller) {
        DmObject dm = observer.getHost().getDmObject();
        if (getAttributeName() != null && (getUpdateTimestamp() == 0 || getUpdateTimestamp() != dm.getUpdateTimestamp())) {
            Object value = dm.getAttr(getAttributeName(), "");
            setText(value.toString());

            setUpdateTimestamp(dm.getUpdateTimestamp());
        }
    }

    public void verify() throws FmException {
    }

    public MlObserver getObserver() {
        return observer;
    }

    public Container getNode() {
        return this;
    }

    // obs props
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * @return the eventName
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * @param eventName the eventName to set
     */
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    // ActionListener
    public void actionPerformed(ActionEvent e) {
        if (getEventName() != null) {
            MlHost h = observer.getHost();
            DmObject dm = h == null ? null : h.getDmObject();
            observer.sendEventToHost(eventName, dm);
        }
    }

}
