package org.fm.impl.swing.ml.observers;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;
import org.fm.impl.swing.ml.MlObserverSwing;
import org.fm.impl.swing.ml.MlObserverSwingComponent;

public class MlJComboBox<E> extends JComboBox<E> implements MlObserverSwingComponent {

    private boolean changed;
    private final MlObserverSwing observer;
    private String attributeName;
    private long updateTimestamp = 0;
    
    private boolean collectValues = false;
    private boolean restrictToValues = false;

    public MlJComboBox(E[] items) {
        super(items);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJComboBox(ComboBoxModel<E> aModel) {
        super(aModel);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJComboBox(Vector<E> items) {
        super(items);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJComboBox() {
        super();
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public void init() {
        getObserver().init();
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }

    // MlObserverSwingComponent
    public void update(MlHost caller) {
        DmObject dm = observer.getHost().getDmObject();
        if (observer.isExecuted() && dm != null && getAttributeName() != null && getUpdateTimestamp() < dm.getUpdateTimestamp()) {
            setChanged(false);

            Object value = dm.getAttr(getAttributeName(), null);
            if (value.equals(getSelectedItem())) {
                return;
            }

            if (_itemIndex((E) value) == -1 && isCollectValues()) {
                _addItem((E) value);
            }
            
            if (_itemIndex((E) value) != -1) {
                setSelectedItem(value);
                setUpdateTimestamp(dm.getUpdateTimestamp());
            } else if(!isRestrictToValues()){
                ComboBoxEditor cbeditor = getEditor();
                Component editorCmp = cbeditor == null ? null : cbeditor.getEditorComponent();

                if (editorCmp != null && editorCmp instanceof JTextField) {
                    ((JTextField)editorCmp).setText(value.toString());
                }
            }
        }
    }

    public void verify() throws FmException {
    }

    public MlObserver getObserver() {
        return observer;
    }

    public Container getNode() {
        return this;
    }

    // obs props
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    // private
    private void addComponenetListener() {
        ComboBoxEditor cbeditor = getEditor();
        Component editorCmp = cbeditor == null ? null : cbeditor.getEditorComponent();

        if (editorCmp != null) {
            editorCmp.addKeyListener(
                    new KeyListener() { 
                        public void keyTyped(KeyEvent e) {
                        }

                        public void keyPressed(KeyEvent e) {
                            if (e.getKeyCode()==e.VK_ENTER) {
                                _updateDm();
                            }
                        }

                        public void keyReleased(KeyEvent e) {
                        }
                    }
            );
        }

        addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent arg0) {
                _updateDm();
            }
        });
    }

    private void _updateDm() {
        setChanged(true);
        if (getAttributeName() != null) {
            Object value = getSelectedItem();
            if (isCollectValues() && _itemIndex((E) value) == -1) {
                _addItem((E) value);
            }
            if(_itemIndex((E) value) == -1 && isRestrictToValues()) {
                return;
            }
            
            DmObject dm = getObserver().getHost().getDmObject();
            dm.setAttr(getAttributeName(), value);
            setUpdateTimestamp(dm.getUpdateTimestamp());
            setChanged(false);
        }
    }
    
    /**
     * @return the changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    // private
    private int _itemIndex(E o) {
        ComboBoxModel model = getModel();
        int size = model.getSize();
        for (int i = 0; i < size; i++) {
            Object element = model.getElementAt(i);
            if (element.equals(o)) {
                return i;
            }
        }

        return (-1);
    }

    private int _addItem(E o) {
        addItem(o);
        return _itemIndex(o);
    }

    /**
     * @return the collectValues
     */
    public boolean isCollectValues() {
        return collectValues;
    }

    /**
     * @param collectValues the collectValues to set
     */
    public void setCollectValues(boolean collectValues) {
        this.collectValues = collectValues;
    }

    /**
     * @return the restrictToValues
     */
    public boolean isRestrictToValues() {
        return restrictToValues;
    }

    /**
     * @param restrictToValues the restrictToValues to set
     */
    public void setRestrictToValues(boolean restrictToValues) {
        this.restrictToValues = restrictToValues;
    }
}
