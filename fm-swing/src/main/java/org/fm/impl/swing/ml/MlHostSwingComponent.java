package org.fm.impl.swing.ml;

import org.fm.ml.hosts.MlHost;

/**
 *
 * @author RobertoB
 */
public interface MlHostSwingComponent {
    public MlHost getHost();
}
