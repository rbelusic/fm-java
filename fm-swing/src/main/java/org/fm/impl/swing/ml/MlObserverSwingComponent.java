package org.fm.impl.swing.ml;

import java.awt.Container;
import org.fm.FmException;
import org.fm.ml.hosts.MlHost;

public interface MlObserverSwingComponent {
    public void init();
    public Container getNode();
    public void update(MlHost caller);
    public void verify() throws FmException;    
    public String getAttributeName();
}
