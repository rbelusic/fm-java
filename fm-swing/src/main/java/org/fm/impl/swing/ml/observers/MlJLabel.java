package org.fm.impl.swing.ml.observers;

import org.fm.impl.swing.ml.MlObserverSwingComponent;
import org.fm.impl.swing.ml.MlObserverSwing;
import java.awt.Container;
import javax.swing.Icon;
import javax.swing.JLabel;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;


public class MlJLabel extends JLabel implements MlObserverSwingComponent {
    private final MlObserverSwing observer;
    private String attributeName;
    private long updateTimestamp=0;
    
    public MlJLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        observer = new MlObserverSwing(this);
    }

    public MlJLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        observer = new MlObserverSwing(this);
    }

    public MlJLabel(String text) {
        super(text);
        observer = new MlObserverSwing(this);
    }
    
    public MlJLabel() {
        super("");
        observer = new MlObserverSwing(this);
    }

    public void init() {
        getObserver().init();
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }
    
    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }
    
    
    // MlObserverSwingComponent
    public void update(MlHost caller) {
        DmObject dm = observer.getHost().getDmObject();
        
        if(getAttributeName() != null && getUpdateTimestamp() < dm.getUpdateTimestamp()) {
            Object value = dm.getAttr(getAttributeName(), "");
            setText(value.toString());
            setUpdateTimestamp(dm.getUpdateTimestamp());
        }
    }

    public void verify() throws FmException {
    }

    public MlObserver getObserver() {
        return observer;
    }

    public Container getNode() {
        return this;
    }

    // obs props
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
