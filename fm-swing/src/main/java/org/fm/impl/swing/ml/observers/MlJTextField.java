/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.impl.swing.ml.observers;

import java.awt.Container;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;
import org.fm.impl.swing.ml.MlObserverSwing;
import org.fm.impl.swing.ml.MlObserverSwingComponent;

/**
 *
 * @author RobertoB
 */
public class MlJTextField extends JTextField implements MlObserverSwingComponent {

    private boolean changed;
    private final MlObserverSwing observer;
    private String attributeName;
    private String updateOn; // CHNAGE, BLUR
    private long updateTimestamp = 0;
    private boolean skipEvent=false;

    public MlJTextField(Document doc, String text, int columns) {
        super(doc, text, columns);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJTextField(String text, int columns) {
        super(text, columns);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJTextField(int columns) {
        super(columns);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJTextField(String text) {
        super(text);
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public MlJTextField() {
        super();
        addComponenetListener();
        observer = new MlObserverSwing(this);
    }

    public void init() {
        getObserver().init();
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }
    
    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }
    
    // MlObserverSwingComponent
    public void update(MlHost caller) {
        DmObject dm = observer.getHost().getDmObject();
        
        if (dm != null && getAttributeName() != null) {                        
            if (getUpdateTimestamp() < dm.getUpdateTimestamp()) {                
                Object dmValue = dm.getAttr(getAttributeName(), null);
                Object value = getText();
                setUpdateTimestamp(System.currentTimeMillis());
                setChanged(false);
                if(!value.equals(dmValue)) {
                    System.out.println(" ------ UPDATE()");
                    System.out.println("ts0:" + getUpdateTimestamp() + ", dmts0:" + dm.getUpdateTimestamp());                    
                    skipEvent = true;
                    setText(dmValue.toString());
                }            
            }
        }        
    }

    public void verify() throws FmException {
    }

    public MlObserver getObserver() {
        return observer;
    }

    public Container getNode() {
        return this;
    }

    // obs props
    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * @param attributeName the attributeName to set
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * @return the updateOn
     */
    public String getUpdateOn() {
        return updateOn == null || updateOn.isEmpty() ? "CHANGE" : updateOn;
    }

    /**
     * @param updateOn the updateOn to set
     */
    public void setUpdateOn(String updateOn) {
        this.updateOn = updateOn;
    }

    // private
    private void addComponenetListener() {
        this.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
            }

            public void focusLost(FocusEvent e) {
                if (getUpdateOn().equals("BLUR") && isChanged()) {
                    _updateDm();
                }
            }
        });

        getDocument().addDocumentListener(new DocumentListener() {            
            public void changedUpdate(DocumentEvent e) {
                setChanged(true);
                if(getUpdateOn().equals("CHANGE")) _updateDm();
                skipEvent = false;
            }

            public void removeUpdate(DocumentEvent e) {
                setChanged(true);
                if(getUpdateOn().equals("CHANGE")) _updateDm();
                skipEvent = false;
            }

            public void insertUpdate(DocumentEvent e) {
                setChanged(true);
                if(getUpdateOn().equals("CHANGE")) _updateDm();
                skipEvent = false;
            }

        });

    }

    private void _updateDm() {        
        if(!observer.isExecuted() || skipEvent) {
            skipEvent = false;
            return;
        }
        
        DmObject dm = getObserver().getHost().getDmObject();
        setChanged(false);
        if (dm != null && getAttributeName() != null) {
            String value = getText();            
            Object dmValue = dm.getAttr(getAttributeName(),null);
            if(!value.equals(dmValue)) {
                System.out.println(" ------ UPDATE DM()");
                System.out.println("utxt:" + value + ", udmv:" + dmValue);
                dm.setAttr(getAttributeName(), value);
            }            
        }        
    }

    /**
     * @return the changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }
}
