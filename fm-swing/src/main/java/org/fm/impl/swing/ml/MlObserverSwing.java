package org.fm.impl.swing.ml;

import java.awt.Container;
import java.util.UUID;
import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;

public class MlObserverSwing implements MlObserver {
    private final MlObserverSwingComponent uiComponent;
    
    private boolean executed;
    private MlHost host;
    private String id;
    
    public MlObserverSwing(MlObserverSwingComponent c) {
        uiComponent = c;    
    }

    public void init() {
        MlHost hst = findHost(uiComponent.getNode().getParent());
        if(hst != null) {
            hst.addObserver(this);
        }        
    }
    
    public <H extends MlHost> void run(H hst) {
        setHost(hst);        
        executed = true;
        update(hst);
    }

    public boolean isExecuted() {
        return executed;
    }

    public void dispose() {
        if(getHost() != null) {
            getHost().removeObserver(this);
        }
        setHost(null);
        executed = false;
    }

    public <H extends MlHost> void setHost(H h) {
        host =  h;
    }

    public <H extends MlHost> H getHost() {
        return (H) host;
    }


    @Override
    public String getID() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
        return (id);
    }
    

    // abs
    public Object getNode() {
        return uiComponent.getNode();
    }
    
    public <H extends MlHost> void update(H caller) {
        uiComponent.update(caller);
    }
    
    public Object getValue() {
        MlHost h = getHost();
        if(h == null) return null;
        DmObject dm = h.getDmObject();
        if(dm == null) return null;
        Object value = dm.getAttr(getAttributeName(), null);
        return value;
    }
    
    public void verify() throws FmException {
        uiComponent.verify();
    }
    
    public void sendEventToHost(String ev,Object evdata) {
        MlHost h = getHost();
        if(h != null) h.onObserverEvent(this, ev, evdata);
    }

    /**
     * @return the attributeName
     */
    public String getAttributeName() {
        return uiComponent.getAttributeName();
    }

    
   public static MlHost findHost(Container c) {
        while(c != null ) {            
            if(c instanceof MlHostSwingComponent) {
                return ((MlHostSwingComponent)c).getHost();
            }
            c = c.getParent();
        }
        
        return null;
    }
     
}
