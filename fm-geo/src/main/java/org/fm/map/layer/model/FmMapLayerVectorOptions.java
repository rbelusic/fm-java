package org.fm.map.layer.model;

import org.fm.map.model.FmMapSymbol;
import org.fm.map.model.FmMapSymbolset;

public class FmMapLayerVectorOptions extends FmMapLayerOptions {
    public FmMapSymbolset sym;
    public FmMapSymbol foregroundSymbol;
}
