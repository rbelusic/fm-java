package org.fm.map.layer.impl;

import org.fm.geo.dataset.model.DmGeoDatasetRasterInfo;
import org.fm.geo.dataset.model.FmGeoDatasetRaster;
import org.fm.geo.dataset.model.FmGeoDatasetTile;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoPoint;
import org.fm.geo.model.FmGeoReferenceWorldFile;
import org.fm.geo.model.FmLutTable;
import org.fm.geo.model.FmRangeLutTable;
import org.fm.geo.model.FmRgb;
import org.fm.map.layer.model.FmMapLayerRasterOptions;
import org.fm.map.layer.model.DmMapLayerRasterInfo;
import org.fm.map.layer.model.FmMapLayer;
import org.fm.map.layer.model.FmMapLayerOptions;
import org.fm.map.model.FmDeviceExtent;
import org.fm.map.model.FmDevicePoint;
import org.fm.map.model.FmMapDisplay;
import org.fm.map.model.FmMapDisplayDriver;
import org.fm.map.model.FmPalTable;
import org.fm.map.model.FmSimplePalTable;
import org.fm.utils.StringUtil;

/**
 *
 * @author rbelusic
 */
public class FmMapLayerRaster implements FmMapLayer {

    private DmMapLayerRasterInfo info;
    private FmMapLayerRasterOptions options;
    private FmMapDisplay map;
    private FmLutTable lutTable;
    private FmPalTable palette;
    private boolean visible = true;
    private String name;
    private int defaultColor;
    private int noDataColor;

    public DmMapLayerRasterInfo getInfo() {
        return info;
    }

    public <T extends FmMapLayerOptions> void init(T opts) throws Exception {
        String hdrFileLc, hdrFileUc;

        options = (FmMapLayerRasterOptions) opts;
        String demFile = options.dataset.getOptions().fileName;
        if (options.pal == null) {
            hdrFileLc = StringUtil.replaceExtension(demFile, ".pal");
            hdrFileUc = StringUtil.replaceExtension(demFile, ".PAL");
            try {
                palette = FmSimplePalTable.instance(hdrFileLc);
            } catch (Exception ex) {
                try {
                    palette = FmSimplePalTable.instance(hdrFileUc);
                } catch (Exception ex2) {
                }
            }
        } else {
            palette = options.pal;
        }

        if (options.lut == null) {
            hdrFileLc = StringUtil.replaceExtension(demFile, ".lut");
            hdrFileUc = StringUtil.replaceExtension(demFile, ".LUT");
            try {
                lutTable = FmRangeLutTable.instance(hdrFileLc);
            } catch (Exception ex) {
                try {
                    lutTable = FmRangeLutTable.instance(hdrFileUc);
                } catch (Exception ex2) {
                }
            }
        } else {
            lutTable = options.lut;
        }
        noDataColor = options.nodataColor == null
                ? FmRgb.RGBA(new int[]{128, 128, 128, 200}) : noDataColor;
        
        defaultColor = options.foregroundColor == null 
                ?  FmRgb.RGBA(new int[]{0, 0, 0, 255}) : options.foregroundColor;
    }

    public void dispose() {
        options = null;
    }

    public <T extends FmMapLayerOptions> T getOptions() {
        return (T) options;
    }

    public <T extends FmMapLayerOptions> void setOptions(T opts) {
        options = (FmMapLayerRasterOptions) opts;
    }

    public FmMapDisplay getMap() {
        return map;
    }

    public void setMap(FmMapDisplay m) {
        map = m;
    }

    private FmDeviceExtent extentToSize(FmGeoExtent myext) {
        FmMapLayerRasterOptions opts = (FmMapLayerRasterOptions) getOptions();
        FmGeoReferenceWorldFile ref = opts.dataset.getReference();
        FmDevicePoint pt1 = ref.toLocalPoint(new FmGeoPoint(myext.getXmin(), myext.getYmin()));
        FmDevicePoint pt2 = ref.toLocalPoint(new FmGeoPoint(myext.getXmax(), myext.getYmax()));

        return new FmDeviceExtent(pt1, pt2);
    }

    private FmGeoExtent sizeToExtent(FmDeviceExtent dsext) {
        FmMapLayerRasterOptions opts = (FmMapLayerRasterOptions) getOptions();
        FmGeoReferenceWorldFile ref = opts.dataset.getReference();
        FmGeoPoint pt1 = ref.toGeoPoint(new FmDevicePoint(dsext.getXmin(), dsext.getYmin()));
        FmGeoPoint pt2 = ref.toGeoPoint(new FmDevicePoint(dsext.getXmax(), dsext.getYmax()));

        return new FmGeoExtent(pt1, pt2);
    }

    public FmLutTable getLutTable() {
        return lutTable;
    }

    public void setLutTable(FmLutTable lut) {
        lutTable = lut;
    }

    /**
     * @return the palette
     */
    public FmPalTable getPalette() {
        return palette;
    }

    /**
     * @param palette the palette to set
     */
    public void setPalette(FmPalTable palette) {
        this.palette = palette;
    }

    public int getDefaultColor() {
        return this.defaultColor;
    }

    public void setDefaultColor(int rgba) {
        this.defaultColor = rgba;
    }

    public void redraw(FmMapDisplayDriver driver, Object canvas) throws Exception {

        FmGeoDatasetRaster ds = (FmGeoDatasetRaster) getOptions().dataset;
        DmGeoDatasetRasterInfo hdr = ds.getInfo();


        // get extent to redraw
        FmGeoExtent myext = new FmGeoExtent(ds.getExtent());
        myext.limitTo(getMap().getMapExtent());

        // select bitmap area
        FmDeviceExtent dsext = extentToSize(myext);


        int cols = dsext.getWidth();
        int rows = dsext.getHeight();
        int crow = dsext.getYmin();
        int ccol = dsext.getXmin();

        if (ccol + cols > hdr.getNcols(null)) {
            cols = hdr.getNcols(null) - ccol;
        }

        if (crow + rows > hdr.getNrows(null)) {
            rows = hdr.getNrows(null) - ccol;
        }

        if (cols < 1 || rows < 1) {
            return;
        }
        dsext.setXmax(ccol + cols - 1);
        dsext.setYmax(crow + rows - 1);

        // calculate back extent
        myext = sizeToExtent(dsext);

        // get the device box size
        FmDevicePoint scrPtUl = getMap().geo2device(new FmGeoPoint(
                myext.getXmin(),
                myext.getYmax()));

        FmDevicePoint scrPtLr = getMap().geo2device(new FmGeoPoint(
                myext.getXmax(),
                myext.getYmin()));

        int scrWidth = scrPtLr.getX() - scrPtUl.getX() + 1;
        int scrHeight = scrPtLr.getY() - scrPtUl.getY() + 1;
        int scrYStart = scrPtUl.getY();
        int scrXStart = scrPtUl.getX();

        int MAX_BUFF_SIZE = 32768;
        int stpPixY = scrWidth > MAX_BUFF_SIZE
                ? 1 : (MAX_BUFF_SIZE / scrWidth);
        if (stpPixY > scrHeight) {
            stpPixY = scrHeight;
        }

        double scx = (double) cols / (double) scrWidth;
        double scy = (double) rows / (double) scrHeight;

        int band = ((FmMapLayerRasterOptions) getOptions()).band;
        int lastRow = -1;
        int scrDataRowIndex = 0;
        FmGeoDatasetTile tile;
        int[] scrData = new int[stpPixY * scrWidth];
        System.err.println("*** Redraw raster ***");
        for (int scrY = 0; scrY < scrHeight; scrY++) {
            int row = (int) ((double) crow + (double) scrY * scy);
            if (row != lastRow) {
                lastRow = row;
                tile = ds.getTile(row, band, ccol, cols, 1);
                try {
                    _scaleAndRemapTileRow(
                            tile, 0,
                            scrData, scrDataRowIndex * scrWidth, scrWidth,
                            scx);
                } catch (Exception e) {
                    throw e;
                }
            } else {
                try {
                    System.arraycopy(
                            scrData, (scrDataRowIndex - 1) * scrWidth,
                            scrData, scrDataRowIndex * scrWidth, scrWidth);
                } catch (Exception e) {
                    throw e;
                }
            }

            scrDataRowIndex++;
            if (scrDataRowIndex >= stpPixY || scrY == scrHeight - 1) {
                driver.drawImage(
                        canvas, scrData,
                        scrWidth, scrDataRowIndex,
                        scrXStart,
                        scrYStart + scrY - (scrDataRowIndex - 1));
                System.out.println(
                        "draw:" + scrXStart + ","
                        + (scrYStart + scrY - (scrDataRowIndex - 1)
                        + " =="
                        + scrWidth + "," + scrDataRowIndex));
                scrDataRowIndex = 0;
                lastRow = -1;
            }
        }
    }

    private void _scaleAndRemapTileRow(
            FmGeoDatasetTile tile, int tileRow,
            int[] sampledArr, int sampledArrIndex, int sampledArrCount,
            double scale) throws Exception {
        FmLutTable<Number> lutTable = getLutTable();
        FmPalTable pallete = getPalette();
        FmGeoDatasetRaster ds = (FmGeoDatasetRaster) getOptions().dataset;
        Number nodataValue = ds.getNoDataValue();
        Number vRange;

        for (int i = 0; i < sampledArrCount; i++) {
            // get value
            int taIndex = (int) (i * scale);
            Number value = taIndex >= tile.width || taIndex < 0
                    ? nodataValue
                    : tile.get(tileRow, taIndex);

            // remap
            if (lutTable != null) {
                vRange = lutTable.mapValue(value.doubleValue());
            } else {
                vRange = value;
            }

            // pallete
            int palIndex = -1;
            if (vRange == null) {
                palIndex = getNoDataColor();
            } else {
                if (pallete == null) {
                    palIndex = getDefaultColor();
                } else {
                    palIndex = pallete.mapValue(vRange.intValue());
                }
            }
            sampledArr[sampledArrIndex + i] = palIndex;
        }
    }

    public void setVisible(boolean v) {
        visible = v;
    }

    public boolean isVisible() {
        return visible;
    }

    public FmGeoExtent getExtent() {
        return getOptions().dataset.getExtent();
    }

    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the noDataColor
     */
    public int getNoDataColor() {
        return noDataColor;
    }

    /**
     * @param noDataColor the noDataColor to set
     */
    public void setNoDataColor(int noDataColor) {
        this.noDataColor = noDataColor;
    }
}
