package org.fm.map.layer.impl;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoPoint;
import org.fm.geo.model.FmLutTable;
import org.fm.geo.utils.WmsClient;
import org.fm.map.layer.model.DmMapLayerInfo;
import org.fm.map.layer.model.FmMapLayerRasterOptions;
import org.fm.map.layer.model.FmMapLayer;
import org.fm.map.layer.model.FmMapLayerOptions;
import org.fm.map.model.FmDeviceExtent;
import org.fm.map.model.FmDevicePoint;
import org.fm.map.model.FmMapDisplay;
import org.fm.map.model.FmMapDisplayDriver;
import org.fm.map.model.FmPalTable;

public class FmMapLayerWms implements FmMapLayer {

    private DmMapLayerInfo info;
    private FmMapLayerRasterOptions options;
    private FmMapDisplay map;
    private boolean visible = true;
    private WmsClient wmsClient;
    private WmsClient.WmsLayer wmsLayer;
    private FmGeoExtent dsExtent;
    private String name;

  
    public DmMapLayerInfo getInfo() {
        return info;
    }

    public <T extends FmMapLayerOptions> void init(T opts) throws Exception {
        options = (FmMapLayerRasterOptions) opts;
        wmsClient = new WmsClient(options.datesetUrl);
        wmsLayer = wmsClient.getLayers().get(options.datesetLayer);
        dsExtent = wmsLayer.getExtent("");
    }

    public void dispose() {
        options = null;
    }

    public <T extends FmMapLayerOptions> T getOptions() {
        return (T) options;
    }

    public <T extends FmMapLayerOptions> void setOptions(T opts) {
        options = (FmMapLayerRasterOptions) opts;
    }

    public FmMapDisplay getMap() {
        return map;
    }

    public void setMap(FmMapDisplay m) {
        map = m;
    }

    public FmLutTable getLutTable() {
        return null;
    }

    public void setLutTable(FmLutTable lut) {
    }

    /**
     * @return the palette
     */
    public FmPalTable getPalette() {
        return null;
    }

    public FmGeoExtent getExtent() {
        return dsExtent;
    }
    
    /**
     * @param palette the palette to set
     */
    public void setPalette(FmPalTable palette) {
    }

    private BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public void redraw(FmMapDisplayDriver driver, Object canvas) throws Exception {
        // get extent to redraw
        FmMapDisplay mymap = getMap();

        FmGeoExtent myext = new FmGeoExtent(dsExtent);
        myext.limitTo(getMap().getMapExtent());
        FmDevicePoint pt1 = mymap.geo2device(new FmGeoPoint(myext.getXmin(), myext.getYmin()));
        FmDevicePoint pt2 = mymap.geo2device(new FmGeoPoint(myext.getXmax(), myext.getYmax()));
        FmDeviceExtent scrExt = new FmDeviceExtent(pt1, pt2);
        Image mapImage = wmsClient.getMap(wmsLayer.getName(), dsExtent.getCRS(),
                myext.getXmin(), myext.getYmin(), myext.getXmax(), myext.getYmax(),
                scrExt.getWidth(), scrExt.getHeight());
        int[] rgbArray = new int[scrExt.getWidth() * scrExt.getHeight()];
        driver.drawImage(
                canvas,
                toBufferedImage(mapImage)
                .getRGB(
                0, 0,
                scrExt.getWidth(), scrExt.getHeight(),
                rgbArray,
                0, scrExt.getWidth()),
                scrExt.getWidth(), scrExt.getHeight(),
                scrExt.getXmin(), scrExt.getYmin());
    }

    public void setVisible(boolean v) {
        visible = v;
    }

    public boolean isVisible() {
        return visible;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
