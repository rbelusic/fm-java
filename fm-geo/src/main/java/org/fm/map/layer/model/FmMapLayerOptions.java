/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.map.layer.model;

import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.geo.model.FmLutTable;
import org.fm.map.model.FmMapSymbolset;
import org.fm.map.model.FmSimplePalTable;

/**
 *
 * @author rbelusic
 */
public class FmMapLayerOptions {
    //  local

    public FmGeoDataset dataset;
    
    // remote
    public String datesetUrl;
    public String datesetLayer;
    public FmLutTable lut;
}
