package org.fm.map.layer.impl;

import java.util.ArrayList;
import java.util.List;
import org.fm.geo.dataset.model.FmGeoDatasetVector;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoFeature;
import org.fm.geo.model.FmGeoLine;
import org.fm.geo.model.FmGeoPoint;
import org.fm.geo.model.FmGeoPolygon;
import org.fm.geo.model.FmLutTable;
import org.fm.geo.model.FmRangeLutTable;
import org.fm.geo.model.FmRgb;
import org.fm.map.layer.model.FmMapLayerVectorOptions;
import org.fm.map.layer.model.DmMapLayerVectorInfo;
import org.fm.map.layer.model.FmMapLayer;
import org.fm.map.layer.model.FmMapLayerOptions;
import org.fm.map.model.FmDeviceExtent;
import org.fm.map.model.FmDevicePoint;
import org.fm.map.model.FmDevicePolygon;
import org.fm.map.model.FmMapDisplay;
import org.fm.map.model.FmMapDisplayDriver;
import org.fm.map.model.FmMapLineSymbol;
import org.fm.map.model.FmMapMarkerSymbol;
import org.fm.map.model.FmMapPolygonSymbol;
import org.fm.map.model.FmMapSymbol;
import org.fm.map.model.FmMapSymbolset;
import org.fm.utils.StringUtil;

public class FmMapLayerVector implements FmMapLayer {

    public final static FmMapLineSymbol DEF_LINE_SYMBOL =
            new FmMapLineSymbol(
            1,
            FmRgb.RGBA(new int[]{100, 100, 100, 255}),
            new float[0],
            FmMapLineSymbol.Rounding.NONE);
    private DmMapLayerVectorInfo info;
    private FmMapLayerVectorOptions options;
    private FmMapDisplay map;
    private boolean visible = true;
    private FmLutTable lutTable;
    private FmMapSymbolset symbols;
    private String name;
    private FmMapSymbol defaultSymbol;
    private FmMapSymbol noDataSymbol;

    public DmMapLayerVectorInfo getInfo() {
        return info;
    }

    public <T extends FmMapLayerOptions> void init(T opts) throws Exception {
        String hdrFileLc, hdrFileUc;
        options = (FmMapLayerVectorOptions) opts;
        String dsFile = options.dataset.getOptions().fileName;
        if (options.lut == null) {
            hdrFileLc = StringUtil.replaceExtension(dsFile, ".lut");
            hdrFileUc = StringUtil.replaceExtension(dsFile, ".LUT");
            try {
                lutTable = FmRangeLutTable.instance(hdrFileLc);
            } catch (Exception ex) {
                try {
                    lutTable = FmRangeLutTable.instance(hdrFileUc);
                } catch (Exception ex2) {
                }
            }
        } else {
            lutTable = options.lut;
        }

        if (options.sym == null) {
            hdrFileLc = StringUtil.replaceExtension(dsFile, ".sym");
            hdrFileUc = StringUtil.replaceExtension(dsFile, ".SYM");
            try {
                symbols = new FmMapSymbolset(hdrFileLc);
            } catch (Exception ex) {
                try {
                    symbols = new FmMapSymbolset(hdrFileUc);
                } catch (Exception ex2) {
                }
            }
        } else {
            symbols = options.sym;
        }

        defaultSymbol = options.foregroundSymbol == null
                ? null : options.foregroundSymbol;
    }

    public void dispose() {
        options = null;
    }

    public <T extends FmMapLayerOptions> T getOptions() {
        return (T) options;
    }

    public <T extends FmMapLayerOptions> void setOptions(T opts) {
        options = (FmMapLayerVectorOptions) opts;
    }

    public FmMapDisplay getMap() {
        return map;
    }

    public void setMap(FmMapDisplay m) {
        map = m;
    }

    public void setVisible(boolean v) {
        visible = v;
    }

    public boolean isVisible() {
        return visible;
    }

    public FmGeoExtent getExtent() {
        return getOptions().dataset.getExtent();
    }

    public void redraw(FmMapDisplayDriver driver, Object canvas) throws Exception {
        for (FmGeoFeature f : ((FmGeoDatasetVector) options.dataset).findFeatures(getMap().getMapExtent())) {
            _drawFeature(f, driver, canvas);
        }
    }

    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public FmLutTable getLutTable() {
        return lutTable;
    }

    public void setLutTable(FmLutTable lut) {
        lutTable = lut;
    }

    public FmMapSymbolset getSymbolset() {
        return symbols;
    }

    public void setSymbolset(FmMapSymbolset symb) {
        symbols = symb;
    }
    FmDeviceExtent devExt = null;

    private void _drawFeature(FmGeoFeature f, FmMapDisplayDriver driver, Object canvas) {
        FmMapSymbol sym = null;

        // symbolset        
        if (symbols != null) {
            Number value = f.getValue();
            Number vCategory =
                    lutTable == null
                    ? value
                    : (Number) lutTable.mapValue(value.doubleValue());
            if (vCategory != null) {
                sym = symbols.getSymbol(vCategory.intValue());
            }
        }

        if (sym == null) {
            sym = getDefaultSymbol(f);
        }

        // draw
        if (f instanceof FmGeoLine) {
            FmGeoLine lnf = (FmGeoLine) f;
            FmMapLineSymbol lsym = (FmMapLineSymbol) sym;

            //numpts = seg+1
            FmDevicePoint[] ptarr = new FmDevicePoint[lnf.getNumberOfSegments() + 1];
            for (int i = 0; i <= lnf.getNumberOfSegments(); i++) {
                ptarr[i] = map.geo2device(lnf.getPoint(i));
                if (devExt == null) {
                    devExt = new FmDeviceExtent(ptarr[i], ptarr[i]);
                } else {
                    devExt.addExtent(ptarr[i]);
                }
            }
            driver.drawLine(canvas, ptarr, lsym);
        } else if (f instanceof FmGeoPolygon) {
            FmGeoPolygon pnf = (FmGeoPolygon) f;
            FmMapPolygonSymbol psym = (FmMapPolygonSymbol) sym;
            if(psym==null) {
                psym = new FmMapPolygonSymbol(0, 0, 0);
            }
            List<List<FmGeoPoint>> outlines = pnf.getOutlines();
            if (outlines == null) {
                return;
            }
            
            List<List<FmDevicePoint>> outlinesDp = new ArrayList<List<FmDevicePoint>>();
            for(List<FmGeoPoint> outline: outlines) {
                List<FmDevicePoint> pts = new ArrayList<FmDevicePoint> (); 
                for (FmGeoPoint gp: outline) {
                    pts.add(map.geo2device(gp));
                }
                outlinesDp.add(pts);
            }
            FmDevicePolygon devicePolygon = new FmDevicePolygon(outlinesDp,null);

            FmGeoPolygon[] holes = pnf.getInnerPolygons();
            List<FmDevicePolygon> innerPolygons = new ArrayList<FmDevicePolygon>();            
            for (FmGeoPolygon hole : holes) {
                outlines = hole.getOutlines();
                if (outlines == null) {
                    continue;
                }
                outlinesDp = new ArrayList<List<FmDevicePoint>>();
                for(List<FmGeoPoint> outline: outlines) {
                    List<FmDevicePoint> pts = new ArrayList<FmDevicePoint> (); 
                    for (FmGeoPoint gp: outline) {
                        pts.add(map.geo2device(gp));
                    }
                    outlinesDp.add(pts);
                }
                innerPolygons.add(new FmDevicePolygon(outlinesDp,null));
            }
            
            if (innerPolygons.size() > 0) {
                devicePolygon.setInnerPolygons(
                        innerPolygons.toArray(new FmDevicePolygon[innerPolygons.size()]));
            }
            driver.drawPolygon(canvas, devicePolygon, psym);
            

        } else if (f instanceof FmGeoPoint) {
            FmGeoPoint ptf = (FmGeoPoint) f;
            sym = sym != null && sym instanceof FmMapMarkerSymbol
                    ? sym : null;
            FmMapMarkerSymbol msym = (FmMapMarkerSymbol) sym;
            driver.drawMarker(canvas, map.geo2device(ptf), msym);
        }
    }

    public FmMapSymbol getDefaultSymbol(FmGeoFeature f) {
        if (f instanceof FmGeoLine) {
            return DEF_LINE_SYMBOL;
        }
        return null;
    }
}
