package org.fm.map.layer.model;

import org.fm.geo.model.FmGeoExtent;
import org.fm.map.model.FmMapDisplay;
import org.fm.map.model.FmMapDisplayDriver;
import org.fm.map.model.FmMapSymbolset;

/**
 *
 * @author RobertoB
 */
public interface FmMapLayer {

    public FmGeoExtent getExtent();

    public static enum Type {
        IMAGE,POINT,LINE,POLYGON;
    } 

    public DmMapLayerInfo getInfo();

    public <T extends FmMapLayerOptions> void init(T options) throws Exception;
    
    public String getName();

    public void setName(String name);
    
    public void dispose();
    
    public <T extends FmMapLayerOptions> T getOptions();
    public <T extends FmMapLayerOptions> void setOptions( T opts);
    
    public FmMapDisplay getMap();
    public void setMap(FmMapDisplay map);
            
    public void setVisible(boolean v);
    public boolean  isVisible();

    public void redraw(FmMapDisplayDriver driver, Object canvas) throws Exception;
}
