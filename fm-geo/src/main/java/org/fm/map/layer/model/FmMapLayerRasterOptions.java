/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.map.layer.model;

import org.fm.map.model.FmPalTable;

/**
 *
 * @author rbelusic
 */
public class FmMapLayerRasterOptions extends FmMapLayerOptions {
    public int band;
    public FmPalTable pal;
    public Integer nodataColor;
    public Integer foregroundColor;
}
