package org.fm.map.model;

public class FmMapPolygonSymbol implements FmMapSymbol {

    private int outlineColor;
    private int outlineWidth;
    private int fillColor;

    public FmMapPolygonSymbol(int frgbac, int orgbac,int owidth) {
        fillColor = frgbac;
        outlineColor = orgbac;
        outlineWidth = owidth;
    }

    /**
     * @return the rgba
     */
    public int getRgba() {
        return fillColor;
    }

    /**
     * @param rgba the rgba to set
     */
    public void setRgba(int rgba) {
        this.fillColor = rgba;
    }

    /**
     * @return the outlineColor
     */
    public int getOutlineRgba() {
        return outlineColor;
    }

    /**
     * @param outlineColor the outlineColor to set
     */
    public void setOutlineRgba(int outlineColor) {
        this.outlineColor = outlineColor;
    }

    /**
     * @return the outlineWidth
     */
    public int getOutlineWidth() {
        return outlineWidth;
    }

    /**
     * @param outlineWidth the outlineWidth to set
     */
    public void setOutlineWidth(int outlineWidth) {
        this.outlineWidth = outlineWidth;
    }

    
}
