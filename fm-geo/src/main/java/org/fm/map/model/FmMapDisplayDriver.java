/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.map.model;

import org.fm.FmDispatcher;

/**
 *
 * @author rbelusic
 */
public interface FmMapDisplayDriver extends FmDispatcher {
    public Object getCanvas();
    public FmDeviceExtent getViewSize();
    public void clear(Object canvas);
    
    public void drawImage(Object canvas, int [] image, int cols, int rows,
            int xdsp,int ydsp);

    public void drawLine(Object canvas, FmDevicePoint[] ptarr, FmMapLineSymbol sym);
    public void drawMarker(Object canvas, FmDevicePoint pt, FmMapMarkerSymbol sym);
    public void drawPolygon(Object canvas, FmDevicePolygon devicePolygon, FmMapPolygonSymbol psym);
    
}
