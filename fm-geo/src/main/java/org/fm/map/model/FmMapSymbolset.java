package org.fm.map.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fm.geo.model.FmRgb;

/**
 *
            # comments
            [id] LINE [width] [rgba] s1 s2 s3 ... sn
            [id] POINT [size] [rgba] "font name" [PLAIN|BOLD|ITALIC] [txt]
 */
public class FmMapSymbolset {    
    public final static String SYM_POINT = "POINT";
    public final static String SYM_LINE = "LINE";
    public final static String SYM_POLYGON = "POLYGON";

    public final static String DEF_LINESET = "org/fm/geo/data/line-default.sym";
    public final static String DEF_MARKERSET = "org/fm/geo/data/marker-default.sym";

    public static FmMapSymbolset instance(String symName) throws Exception {
        return new FmMapSymbolset(symName);
    }

    private String fileName="";

    private final Map<Integer, FmMapSymbol> symbols
            = new HashMap<Integer, FmMapSymbol>();

    public FmMapSymbolset()  {
        
    }
    
    public FmMapSymbolset(String fname) throws Exception {
        fileName = fname;
        InputStream is = new FileInputStream(fname);
        _configure(is);
    }

    public FmMapSymbolset(InputStream stream) throws Exception {
        fileName = "";
        _configure(stream);
    }

    public Set<Map.Entry<Integer, FmMapSymbol>> entrySet() {
        return symbols.entrySet();
    }
    
    
    public int getSize() {
        return symbols.size();
    }
    
    public FmMapSymbol getSymbol(int index) {
        return symbols.get(index);
    }

    public void addSymbol(int index, FmMapSymbol s) {
        symbols.put(index,s);
    }

    private void _configure(InputStream stream) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                _parseLine(line);
            }
        } finally {
            br.close();
        }
    }

/*
            [id] LINE [width]  [r:g:b:a] s1 s2 s3 ... sn
            [id] POLY [outlineWidth]  [r:g:b:a] [r:g:b:a] #  outlineWidth fillRgba outlineRgba
            [id] POINT [size]  [r:g:b:a] "font name" [PLAIN|BOLD|ITALIC] [txt]
*/
    private void _parseLine(String line) throws Exception {
        line = line.trim();
        
        if (line.indexOf("#") > -1) {
            line = line.substring(0, line.indexOf("#"));
        }
        if(line.isEmpty()) return;
        
        String[] tokens = line.split("\\s+");
        int index = Integer.parseInt(tokens[0]);
        FmMapSymbol.Type type = 
                tokens[1].equalsIgnoreCase("POINT") ? 
                FmMapSymbol.Type.POINT : (
                    tokens[1].equalsIgnoreCase("LINE") ? 
                    FmMapSymbol.Type.LINE : (
                        tokens[1].equalsIgnoreCase("POLY") ? 
                        FmMapSymbol.Type.POLYGON : null                    
                    )
                );
        if(type == null) {
            throw new Exception("Invalid line format");
        }
        int size = Integer.parseInt(tokens[2]);
        
        int rgba = FmRgb.RGBA(tokens[3]);
        
        if(type == FmMapSymbol.Type.LINE) {
            float[] secs = new float[tokens.length-4];
            for(int i = 0; i < secs.length; i++) {
                secs[i] = Float.parseFloat(tokens[i+4]);
            }
            FmMapLineSymbol sym = new FmMapLineSymbol(size, rgba, secs, FmMapLineSymbol.Rounding.NONE);
            symbols.put(index, sym);
        } else if(type == FmMapSymbol.Type.POLYGON) {
            int otlineRgba  = FmRgb.RGBA(tokens[4]);
            FmMapPolygonSymbol sym = new FmMapPolygonSymbol(rgba,otlineRgba,size);
            symbols.put(index, sym);
        } else if(type == FmMapSymbol.Type.POINT) {
            String fontName = tokens[4];
            FmMapMarkerSymbol.Style style = 
                    tokens[5].equalsIgnoreCase("PLAIN") ? 
                    FmMapMarkerSymbol.Style.PLAIN : (
                        tokens[5].equalsIgnoreCase("BOLD") ? 
                        FmMapMarkerSymbol.Style.BOLD : (
                            tokens[5].equalsIgnoreCase("ITALIC") ? 
                            FmMapMarkerSymbol.Style.ITALIC : null                    
                        )
                    );
            if(style == null) {
                throw new Exception("Invalid line format");
            }
            String mrktxt = tokens[6];
            FmMapMarkerSymbol sym = new FmMapMarkerSymbol(size, rgba, fontName, style,mrktxt);
            symbols.put(index, sym);            
        } else {
            // silent ignore
        }
        
    }

    private String[] _split(String str) {
        List<String> list = new ArrayList<String>();
        Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(str);
        while (m.find()) {
            list.add(m.group(1).replace("\"", ""));
        }
        
        return list.toArray(new String[list.size()]);
    }
    
}
