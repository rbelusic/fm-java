package org.fm.map.model;

public class FmMapMarkerSymbol implements FmMapSymbol {
        public static enum Style  {
            PLAIN,BOLD,ITALIC            
        }
                
        private Style style;
        private String symchar;
        private String symfont;
        private int size;
        private int rgba;
        
        public FmMapMarkerSymbol(int s, int rgbac, String fontName, Style st, String txt) {
            style = st;
            symfont = fontName;
            symchar = txt;
            size = s;
            rgba = rgbac;
        }
        
        public int getRgba() {
            return rgba;
        }
        
        public Style getStyle() {
            return style;
        }
        
        public String getChar() {
            return symchar;
        }

        public String getFontName() {
            return symfont;
        }
        
        public int getSize() {
            return size;
        }
        
}
