package org.fm.map.model;

public class FmMapLineSymbol implements FmMapSymbol {
        public static enum Rounding {
            NONE,SQUARE,ROUND            
        }
        
        private Rounding rounding;
        private float width;
        private int rgba;
        private float[] sections;
        
        public FmMapLineSymbol(int w, int rgbac, float [] secs, Rounding r) {
            width = w;
            rgba = rgbac;
            sections = secs;
            rounding = r;
        }

        /**
         * @return the width
         */
        public float getWidth() {
            return width;
        }

        /**
         * @param width the width to set
         */
        public void setWidth(float width) {
            this.width = width;
        }

        /**
         * @return the rgba
         */
        public int getRgba() {
            return rgba;
        }

        /**
         * @param rgba the rgba to set
         */
        public void setRgba(int rgba) {
            this.rgba = rgba;
        }

        /**
         * @return the sections
         */
        public float[] getSections() {
            return sections;
        }

        /**
         * @param sections the sections to set
         */
        public void setSections(float[] sections) {
            this.sections = sections;
        }

        
        public Rounding getRounding() {
            return rounding;
        }

        public void setRounding(Rounding rounding) {
            this.rounding = rounding;
        }
        
}
