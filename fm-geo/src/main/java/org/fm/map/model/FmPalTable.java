package org.fm.map.model;

import org.fm.geo.model.FmRgb;

/**
 *
 * @author RobertoB
 */
public interface FmPalTable {    
    public int mapValue(int index);
    public FmRgb mapValueToColor(int value);
    
}
