package org.fm.map.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.IIOException;
import org.fm.geo.model.FmRgb;

/**
 *
 * @author RobertoB
 */
public class FmSimplePalTable implements FmPalTable {

    private final HashMap<Integer, Integer> colors =
            new HashMap<Integer, Integer>();

    private FmSimplePalTable(Map<Integer, Integer> cols) {
        _configure(cols);
    }

    public int mapValue(int index) {
        return colors.get(index);
    }

    public FmRgb mapValueToColor(int value) {
        Integer c = mapValue(value);
        return c == null ? null : FmRgb.instance(c);
    }

    // priv
    private void _configure(Map<Integer, Integer> cols) {
        colors.putAll(cols);
    }

    public void dump() {
        for (Map.Entry<Integer, Integer> e : colors.entrySet()) {
            int[] c = FmRgb.RGB(e.getValue());
            System.out.println("" + e.getKey() + " = " + c[0] + "," + c[1] + "," + c[2]);
        }
    }

    public static FmSimplePalTable instance(Map<Integer, Integer> cols) {
        return new FmSimplePalTable(cols);
    }

    // format: min max val
    public static FmSimplePalTable instance(String fname) throws IOException {
        Map<Integer, Integer> cols = new HashMap<Integer, Integer>();

        BufferedReader br = new BufferedReader(new FileReader(fname));
        try {
            String line = br.readLine();
            int lc = 0;
            while (line != null) {
                lc++;
                line = line.trim();
                if (!line.isEmpty() && !line.startsWith("#")) {
                    String[] tokens = line.split("\\s+");
                    if (tokens.length != 2) {
                        throw new IIOException("Invalid palette definition on line " + lc);
                    }

                    // split colors
                    String[] clrtokens = tokens[1].split("\\:");
                    if (clrtokens.length < 3) {
                        throw new IIOException("Invalid palette definition on line " + lc);
                    }
                    cols.put(
                            Integer.parseInt(tokens[0]),
                            FmRgb.RGBA(new int[]{
                        Integer.parseInt(clrtokens[0]),
                        Integer.parseInt(clrtokens[1]),
                        Integer.parseInt(clrtokens[2]),
                        clrtokens.length == 3 ? 255: Integer.parseInt(clrtokens[2])}));
                }
                line = br.readLine();
            }

            return new FmSimplePalTable(cols);
        } finally {
            br.close();
        }

    }
}
