package org.fm.map.model;

import org.fm.geo.model.*;

/**
 *
 * @author RobertoB
 */
public class FmDeviceExtent {

    private int xmin;
    private int ymin;
    private int xmax;
    private int ymax;

    public FmDeviceExtent(int xmin, int ymin, int xmax, int ymax) {
        setXmin(xmin);
        setYmin(ymin);

        setXmax(xmax);
        setYmax(ymax);
    }

    public FmDeviceExtent(FmDevicePoint pt1, FmDevicePoint pt2) {
        this(
                Math.min(pt1.getX(), pt2.getX()),
                Math.min(pt1.getY(), pt2.getY()),
                Math.max(pt1.getX(), pt2.getX()),
                Math.max(pt1.getY(), pt2.getY()));
    }

    public FmDeviceExtent(FmDeviceExtent e) {
        this(e.getXmin(), e.getYmin(), e.getXmax(), e.getYmax());
    }

    public FmDevicePoint getCenter() {
        return new FmDevicePoint(
                getWidth() / 2 + getXmin(), 
                getHeight() / 2 + getYmin()
            );
    }


    public int getWidth() {
        return Math.abs(getXmax() - getXmin()) + 1;
    }

    public int getHeight() {
        return Math.abs(getYmax() - getYmin()) + 1;
    }

    /**
     * @return the xmin
     */
    public int getXmin() {
        return xmin;
    }

    /**
     * @param xmin the xmin to set
     */
    public void setXmin(int xmin) {
        this.xmin = xmin;
    }

    /**
     * @return the ymin
     */
    public int getYmin() {
        return ymin;
    }

    /**
     * @param ymin the ymin to set
     */
    public void setYmin(int ymin) {
        this.ymin = ymin;
    }

    /**
     * @return the xmax
     */
    public int getXmax() {
        return xmax;
    }

    /**
     * @param xmax the xmax to set
     */
    public void setXmax(int xmax) {
        this.xmax = xmax;
    }

    /**
     * @return the ymax
     */
    public int getYmax() {
        return ymax;
    }

    /**
     * @param ymax the ymax to set
     */
    public void setYmax(int ymax) {
        this.ymax = ymax;
    }

    // add all
    public void addExtent(FmDeviceExtent extent) {
        if (getXmin() > extent.getXmin()) {
            setXmin(extent.getXmin());
        }
        if (getYmin() > extent.getYmin()) {
            setYmin(extent.getYmin());
        }
        if (getXmax() < extent.getXmax()) {
            setXmax(extent.getXmax());
        }
        if (getYmax() < extent.getYmax()) {
            setYmax(extent.getYmax());
        }
    }
    
    public void addExtent(FmDevicePoint pt) {
        if (getXmin() > pt.getX()) {
            setXmin(pt.getX());
        }
        if (getYmin() > pt.getY()) {
            setYmin(pt.getY());
        }
        if (getXmax() < pt.getX()) {
            setXmax(pt.getX());
        }
        if (getYmax() < pt.getY()) {
            setYmax(pt.getY());
        }
    }

    // common area
    public void limitTo(FmDeviceExtent extent) {
        if (getXmin() < extent.getXmin()) {
            setXmin(extent.getXmin());
        }
        if (getYmin() < extent.getYmin()) {
            setYmin(extent.getYmin());
        }
        if (getXmax() > extent.getXmax()) {
            setXmax(extent.getXmax());
        }
        if (getYmax() > extent.getYmax()) {
            setYmax(extent.getYmax());
        }
    }

    public boolean intersects(FmDeviceExtent extent) {
        return (extent.getXmin() > getXmax()
                || extent.getYmin() > getYmax()
                || extent.getXmax() < getXmin()
                || extent.getYmax() < getYmin()
                ? false : true);
    }
    
    public String toString() {
        return 
                "Xmin:" + getXmin() 
                + ", Ymin:" + getYmin() 
                + ", Xmax:" + getXmax() 
                + ", Ymax:" + getYmax();
    }
}
