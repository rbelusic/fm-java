package org.fm.map.model;

import java.util.ArrayList;
import java.util.List;
import org.fm.geo.model.FmGeoPoint;
import org.fm.geo.model.FmGeoPolygon;

public class FmDevicePolygon {
    private List<List<FmDevicePoint>> outlines;
    private FmDevicePolygon[] innerPolygons;

    public FmDevicePolygon(List<List<FmDevicePoint>> outline, FmDevicePolygon[] inner) {
        outlines = outline;
        innerPolygons = inner;
    }

    
    /**
     * @return the outlines
     */
    public List<List<FmDevicePoint>> getOutlines() {
        return outlines;
    }

    /**
     * @param outlines the outlines to set
     */
    public void setOutlines(List<List<FmDevicePoint>> outlines) {
        this.outlines = outlines;
    }

    /**
     * @return the innerPolygons
     */
    public FmDevicePolygon[] getInnerPolygons() {
        return innerPolygons;
    }

    /**
     * @param innerPolygons the innerPolygons to set
     */
    public void setInnerPolygons(FmDevicePolygon[] innerPolygons) {
        this.innerPolygons = innerPolygons;
    }
}
