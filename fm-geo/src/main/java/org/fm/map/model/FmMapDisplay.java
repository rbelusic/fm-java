package org.fm.map.model;

import java.util.List;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoPoint;
import org.fm.map.layer.model.FmMapLayer;

/**
 *
 * @author rbelusic
 */
public interface FmMapDisplay {

    public void addLayer(FmMapLayer l) throws Exception;

    public void removeLayer(FmMapLayer l);

    public FmMapLayer getLayer(String name);
    
    public List<FmMapLayer> getLayers();

    public FmGeoPoint device2geo(FmDevicePoint pd);

    public FmDevicePoint geo2device(FmGeoPoint pg);

    public FmDevicePoint geo2device(FmGeoPoint pg, FmGeoExtent ext);

    public FmGeoPoint getMapCenter();

    public void setMapCenter(FmGeoPoint pg);

    public FmGeoExtent getMapExtent();

    public void setMapExtent(FmGeoExtent mapExtent);

    double getMapScale();

    public void setMapScale(double mapScale);

    public FmDeviceExtent getViewSize();

    public void zoomIn();

    public void zoomOut();

    public void zoomToAll();

    public void redraw(Object canvas);
    
    public void setDriver(FmMapDisplayDriver driver);
    public FmMapDisplayDriver getDriver();
}
