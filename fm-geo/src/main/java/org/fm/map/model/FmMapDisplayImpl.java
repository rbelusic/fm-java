package org.fm.map.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoPoint;
import org.fm.map.layer.model.FmMapLayer;
import org.fm.map.layer.model.FmMapLayerOptions;

/**
 *
 * @author rbelusic
 */
public class FmMapDisplayImpl implements FmMapDisplay {
    private FmMapDisplayDriver driver;
    private List<FmMapLayer> layers =
            new ArrayList<FmMapLayer>();
    private FmGeoExtent mapExtent;
    private double mapScale;

    public FmMapDisplayImpl(FmMapDisplayDriver d) {
        driver = d;
    }

    public FmMapDisplayImpl() {
        driver = null;
    }

    public void redraw(Object canvas) {
        canvas = canvas == null ? getDriver().getCanvas() : canvas;
        
        getDriver().clear(canvas);

        for (FmMapLayer l : getLayers()) {
            if (l.isVisible()) {
                try {
                    l.redraw(getDriver(), canvas);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Logger.getLogger(FmMapDisplayImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public FmDeviceExtent getViewSize() {
        return getDriver().getViewSize();
    }

    @Override
    public void addLayer(FmMapLayer l) throws Exception {

        if (getLayer(l.getName()) != null) {
            throw new Exception(
                    "Layer with name [" + l.getName() + "] is already on map.");
        }

        layers.add(l);
        FmMapLayerOptions opts = l.getOptions();
        l.setMap(this);

        if (layers.size() == 1) {
            setMapExtent(l.getExtent());
        } else if (l.isVisible()) {
            l.redraw(getDriver(),getDriver().getCanvas());
        }
    }

    @Override
    public void removeLayer(FmMapLayer l) {
        layers.remove(l);
        l.setMap(null);
        if (l.isVisible()) {
            redraw(getDriver().getCanvas());
        }
    }

    public List<FmMapLayer> getLayers() {
        return layers;
    }
    
    public FmMapLayer getLayer(String name) {
        for(FmMapLayer l: layers){
            if(l.getName().equals(name)) return l;
        }
        
        return null;
    }

    public FmGeoPoint device2geo(FmDevicePoint pd) {
        if(mapExtent == null) {
            return new FmGeoPoint(pd.getX(), pd.getY());
        }
        
        double x = (pd.getX() * mapScale) + mapExtent.getXmin();
        double y = (getViewSize().getHeight() - pd.getY()) * mapScale + mapExtent.getYmin();
        return new FmGeoPoint(x, y);
    }

    public FmDevicePoint geo2device(FmGeoPoint pg) {
        return geo2device(pg, getMapExtent());
    }

    public FmDevicePoint geo2device(FmGeoPoint pg, FmGeoExtent ext) {
        if(ext == null) {
            return new FmDevicePoint((int)pg.getXpos(), (int)pg.getYpos());
        }

        int x = (int) ((pg.getXpos() - ext.getXmin()) / getMapScale());
        int y = getViewSize().getHeight() - (int) ((pg.getYpos() - ext.getYmin()) / getMapScale());
        return new FmDevicePoint(x, y);
    }

    @Override
    public FmGeoPoint getMapCenter() {
        return new FmGeoPoint(
                getMapExtent().getXmin() + getMapExtent().getWidth() / 2.,
                getMapExtent().getYmin() + getMapExtent().getHeight() / 2.);
    }

    @Override
    public void setMapCenter(FmGeoPoint pg) {
        double dx = getMapExtent().getHorizontalDistance() / 2.;
        double dy = getMapExtent().getVerticalDistance() / 2.;

        setMapExtent(
                new FmGeoExtent(
                pg.getXpos() - dx, pg.getYpos() - dy,
                pg.getXpos() + dx, pg.getYpos() + dy));
    }

    @Override
    public FmGeoExtent getMapExtent() {
        return mapExtent;
    }

    /**
     * @param mapExtent the mapExtent to set
     */
    @Override
    public void setMapExtent(FmGeoExtent mapExtent) {
        _newMapExtent(mapExtent);
        redraw(getDriver().getCanvas());
    }

    public double getMapScale() {
        return mapScale;
    }

    /**
     * @param mapScale the mapScale to set
     */
    @Override
    public void setMapScale(double mapScale) {
        _setMapScale(mapScale);
        FmGeoPoint mc = getMapExtent().getCenter();
        FmDeviceExtent bounds = getViewSize();

        _setMapExtent(new FmGeoExtent(
                mc.getXpos() - mapScale * bounds.getWidth() / 2.,
                mc.getYpos() - mapScale * bounds.getHeight() / 2.,
                mc.getXpos() + mapScale * bounds.getWidth() / 2.,
                mc.getYpos() + mapScale * bounds.getHeight() / 2.));
        redraw(getDriver().getCanvas());
    }

    @Override
    public void zoomOut() {
        setMapScale(getMapScale() * 1.2);
    }

    @Override
    public void zoomIn() {
        setMapScale(getMapScale() * 0.8);
    }

    @Override
    public void zoomToAll() {
        FmGeoExtent ne = null;
        for (FmMapLayer l : layers) {
            FmGeoExtent lext = l.getExtent();
            if (ne == null) {
                ne = new FmGeoExtent(lext);
            } else {
                ne.addExtent(lext);
            }
        }

        if (ne != null) {
            setMapExtent(ne);
        }

    }

    private void _newMapExtent(FmGeoExtent inpExtent) {
        if (inpExtent == null) {
            _setMapExtent(null);
            _setMapScale(1.);
            return;
        }
        FmDeviceExtent bounds = getViewSize();
        double scrAspRatio = bounds.getWidth() / bounds.getHeight();
        double extAspRatio = inpExtent.getHorizontalDistance() / inpExtent.getVerticalDistance();
        FmGeoPoint mc = inpExtent.getCenter();
        double corRatio = 0;
        if (scrAspRatio > extAspRatio) {
            corRatio = inpExtent.getVerticalDistance() / bounds.getHeight();
        } else {
            corRatio = inpExtent.getHorizontalDistance() / bounds.getWidth();
        }
        double halfX = (bounds.getWidth() / 2.) * corRatio;
        double halfY = (bounds.getHeight() / 2.) * corRatio;

        FmGeoExtent recalcExt = new FmGeoExtent(
                mc.getXpos() - halfX,
                mc.getYpos() - halfY,
                mc.getXpos() + halfX,
                mc.getYpos() + halfY);

        System.out.format("_rclMapExtent: %f, %f, %f, %f\n", recalcExt.getXmin(), recalcExt.getYmin(), recalcExt.getXmax(), recalcExt.getYmax());
        _setMapExtent(recalcExt);
        _setMapScale(corRatio);
        System.out.println(" << ===== _newMapExtent ==============================");
    }

    private void _setMapScale(double mapScale) {
        this.mapScale = mapScale;
    }

    private void _setMapExtent(FmGeoExtent mapExtent) {
        this.mapExtent = mapExtent;
    }

    /**
     * @return the driver
     */
    public FmMapDisplayDriver getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(FmMapDisplayDriver driver) {
        this.driver = driver;
    }
}
