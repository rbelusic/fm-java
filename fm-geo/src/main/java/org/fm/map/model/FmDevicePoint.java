/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fm.map.model;

/**
 *
 * @author RobertoB
 */
public class FmDevicePoint {
    private int x;
    private int y;

    public FmDevicePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param width the x to set
     */
    public void setX(int width) {
        this.x = width;
    }

    /**
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * @param height the y to set
     */
    public void setY(int height) {
        this.y = height;
    }
    
    
}
