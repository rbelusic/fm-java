package org.fm.map.model;

/**
 *
 * @author RobertoB
 */
public class FmDeviceLine {
    private FmDevicePoint[] points;

    public FmDeviceLine(FmDevicePoint[] pts) {
        points = pts;
    }
    
    
    public FmDevicePoint[] getPoints() {
        return points;
    }

    public FmDevicePoint getPoint(int i) {
        return i < 0 ? points[points.length - i] : points[i];
    }

    public int getNumberOfPoints() {
        return points != null ? points.length : 0;
    }
    
}
