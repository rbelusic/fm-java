/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.geo.model;

import org.fm.map.model.FmDevicePoint;

/**
 *
 * @author rbelusic
 */
public interface FmGeoReference {

    double getValue(int idx);

    FmGeoPoint toGeoPoint(FmDevicePoint p);

    FmDevicePoint toLocalPoint(FmGeoPoint p);
    
}
