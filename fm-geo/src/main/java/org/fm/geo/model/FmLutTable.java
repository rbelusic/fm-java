package org.fm.geo.model;

/**
 *
 * @author RobertoB
 */
public interface FmLutTable<T> {
    public T mapValue(double value);
}
