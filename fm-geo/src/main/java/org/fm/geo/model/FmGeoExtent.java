/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.geo.model;

/**
 *
 * @author RobertoB
 */
public class FmGeoExtent {
    private String CRS;
    
    private double xmin;
    private double ymin;
    private double xmax;
    private double ymax;

    public FmGeoExtent(double xmin, double ymin, double xmax, double ymax) {
        this(xmin, ymin, xmax, ymax,"");
    }
    public FmGeoExtent(double xmin, double ymin, double xmax, double ymax, String crs) {
        setXmin(xmin);
        setYmin(ymin);

        setXmax(xmax);
        setYmax(ymax);
        setCRS(crs);
    }

    public FmGeoExtent(FmGeoPoint pt1, FmGeoPoint pt2) {
        this(
                Math.min(pt1.getXpos(), pt2.getXpos()),
                Math.min(pt1.getYpos(), pt2.getYpos()),
                Math.max(pt1.getXpos(), pt2.getXpos()),
                Math.max(pt1.getYpos(), pt2.getYpos()));
    }

    public FmGeoExtent(FmGeoExtent e) {
        this(e.getXmin(), e.getYmin(), e.getXmax(), e.getYmax(),e.getCRS());
    }

    public FmGeoPoint getCenter() {
        return new FmGeoPoint(getHorizontalDistance() / 2. + getXmin(), getVerticalDistance() / 2. + getYmin(), null);
    }

    public double getHorizontalDistance() {
        return Math.abs(getXmax() - getXmin());
    }

    public double getVerticalDistance() {
        return Math.abs(getYmax() - getYmin());
    }

    public double getWidth() {
        return getHorizontalDistance() + 1;
    }

    public double getHeight() {
        return getVerticalDistance() + 1;
    }

    /**
     * @return the xmin
     */
    public double getXmin() {
        return xmin;
    }

    /**
     * @param xmin the xmin to set
     */
    public void setXmin(double xmin) {
        this.xmin = xmin;
    }

    /**
     * @return the ymin
     */
    public double getYmin() {
        return ymin;
    }

    /**
     * @param ymin the ymin to set
     */
    public void setYmin(double ymin) {
        this.ymin = ymin;
    }

    /**
     * @return the xmax
     */
    public double getXmax() {
        return xmax;
    }

    /**
     * @param xmax the xmax to set
     */
    public void setXmax(double xmax) {
        this.xmax = xmax;
    }

    /**
     * @return the ymax
     */
    public double getYmax() {
        return ymax;
    }

    /**
     * @param ymax the ymax to set
     */
    public void setYmax(double ymax) {
        this.ymax = ymax;
    }

    // add all
    public void addExtent(FmGeoExtent extent) {
        if (getXmin() > extent.getXmin()) {
            setXmin(extent.getXmin());
        }
        if (getYmin() > extent.getYmin()) {
            setYmin(extent.getYmin());
        }
        if (getXmax() < extent.getXmax()) {
            setXmax(extent.getXmax());
        }
        if (getYmax() < extent.getYmax()) {
            setYmax(extent.getYmax());
        }
    }

    // common area
    public void limitTo(FmGeoExtent extent) {
        if(extent == null) return;
        
        if (getXmin() < extent.getXmin()) {
            setXmin(extent.getXmin());
        }
        if (getYmin() < extent.getYmin()) {
            setYmin(extent.getYmin());
        }
        if (getXmax() > extent.getXmax()) {
            setXmax(extent.getXmax());
        }
        if (getYmax() > extent.getYmax()) {
            setYmax(extent.getYmax());
        }
    }

    public boolean intersects(FmGeoExtent extent) {
        return (extent.getXmin() > getXmax()
                || extent.getYmin() > getYmax()
                || extent.getXmax() < getXmin()
                || extent.getYmax() < getYmin()
                ? false : true);
    }

    /**
     * @return the CRS
     */
    public String getCRS() {
        return CRS;
    }

    /**
     * @param CRS the CRS to set
     */
    public void setCRS(String CRS) {
        this.CRS = CRS;
    }
}
