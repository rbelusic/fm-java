package org.fm.geo.model;

import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;

public class FmRgb {

    public final static HashMap<String, Object> colorNames = new HashMap<String, Object>() {
        {
            put("white", new int[]{255, 255, 255, 255});
            put("black", new int[]{0, 0, 0, 255});
            put("red", new int[]{255, 0, 0, 255});
            put("green", new int[]{0, 255, 0, 255});
            put("blue", new int[]{0, 0, 255, 255});
        }
    };
    private final int red;
    private final int green;
    private final int blue;
    private final int alpha;

    public static int[] RGB(int rgba) {
        final int alphaMask = 0xFF000000, blueMask = 0xFF0000, greenMask = 0xFF00, redMask = 0xFF;
        return new int[]{
            rgba & redMask,
            (rgba & greenMask) >> 8,
            (rgba & blueMask) >> 16,
            (rgba & alphaMask) >> 24
        };
    }

    public static int RGBA(FmRgb def) {
        return RGBA(def.get());
    }

    public static int RGBA(int[] rgb) {
        if (rgb == null || rgb.length < 4) {
            return 0;
        }
        //new java.awt.Color(rgb[0], rgb[1], rgb[2],rgb[3]).getRGB();
        int rgba = ((rgb[3] & 0xFF) << 24) |
                ((rgb[0] & 0xFF) << 16) |
                ((rgb[1] & 0xFF) << 8)  |
                ((rgb[2] & 0xFF) << 0);
        return rgba;
    }

    public static int RGBA(String def) {
        return RGBA(_parseColor(def));
    }

    /*    
     */
    public static FmRgb instance(String def) throws IOException {
        int[] rgba = _parseColor(def);

        if (rgba != null) {
            return new FmRgb(rgba);
        } else {
            throw new IOException("Invalid format: " + def);
        }
    }

    public static FmRgb instance(int rgba) {
        return new FmRgb(RGB(rgba));
    }

    public static FmRgb instance(int[] rgba) {
        return new FmRgb(rgba);
    }

    public static FmRgb instance(int r, int g, int b, int a) {
        return new FmRgb(r, g, b, a);
    }

    public FmRgb(int[] rgba) {
        this(rgba[0], rgba[1], rgba[2], rgba[3]);
    }

    public FmRgb(int r, int g, int b, int a) {
        red = r;
        green = g;
        blue = b;
        alpha = a;
    }

    /**
     * @return the red
     */
    public int getRed() {
        return red;
    }

    /**
     * @return the green
     */
    public int getGreen() {
        return green;
    }

    /**
     * @return the blue
     */
    public int getBlue() {
        return blue;
    }

    /**
     * @return the alpha
     */
    public int getAlpha() {
        return alpha;
    }

    public int[] get() {
        return new int[]{red, green, blue, alpha};
    }

    private static int[] _parseColor(String def) {
        String[] parts = def.split("\\:");
        // name spec
        int[] rgb = null;
        if (parts.length == 1) {
            rgb = (int[]) colorNames.get(def);
        } else if (parts.length > 2 && parts.length < 5) {
            rgb = new int[]{
                Integer.parseInt(parts[0]) & 255,
                Integer.parseInt(parts[1]) & 255,
                Integer.parseInt(parts[2]) & 255,
                Integer.parseInt(parts[3]) & 255
            };
        }
        return rgb;
    }
}
