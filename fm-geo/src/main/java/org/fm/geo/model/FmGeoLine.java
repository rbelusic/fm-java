package org.fm.geo.model;

import java.util.Map;

/**
 *
 * @author RobertoB
 */
public class FmGeoLine implements FmGeoFeature {
    private Map<String,Object> properties;
    
    private FmGeoPoint[] points;
    private FmGeoPoint centroid;
    private FmGeoExtent extent;
    private double length;
    private Integer value;

    public FmGeoLine(FmGeoPoint[] pts) {
        this(pts,null,null);
    }
    
    public FmGeoLine(FmGeoPoint[] pts,Integer v) {
        this(pts, v, null);        
    }
    
    public FmGeoLine(FmGeoPoint[] pts,Integer v, Map<String, Object> fdata) {
        if (pts.length < 2) {
            throw new IllegalArgumentException("FmGeoLine: number of points must be > 1");
        }

        points = pts;
        value = v;
        properties = fdata;
        _recalc();
    }

    /**
     * @return the points
     */
    public FmGeoPoint[] getPoints() {
        return points;
    }

    public FmGeoPoint getPoint(int i) {
        return i < 0 ? points[points.length - i] : points[i];
    }

    public int getNumberOfSegments() {
        return getPoints().length < 2 ? 0 : getPoints().length - 1;
    }

    public FmGeoLineSegment getSegment(int idx) {
        if (idx > 0 && idx < getNumberOfSegments()) {
            return new FmGeoLineSegment(getPoints()[idx], getPoints()[idx + 1]);
        }
        return null;
    }

    public double getLength() {
        return length;
    }

    public FmGeoExtent getExtent() {
        return extent;
    }

    public FmGeoPoint getCenter() {
        return centroid;
    }

    public double getArea() {
        return 0.0;
    }

    public void print() {
        System.out.println("  LINE (points=" + points.length + ")");
        for (FmGeoPoint p : points) {
            p.print();
        }
    }

    private void _recalc() {
        double len = 0.0, sumx = 0.0, sumy = 0.0;
        FmGeoExtent newextent = null;

        for (int i = 0; i < getPoints().length - 1; i++) {
            len += Math.sqrt(
                    Math.pow(getPoints()[i].getXpos() - getPoints()[i + 1].getXpos(), 2.0)
                    + Math.pow(getPoints()[i].getYpos() - getPoints()[i + 1].getYpos(), 2.0));
            sumx += getPoints()[i].getXpos();
            sumy += getPoints()[i].getYpos();

            if (newextent == null) {
                newextent = new FmGeoExtent(getPoints()[i], getPoints()[i]);
            } else {
                newextent.addExtent(new FmGeoExtent(getPoints()[i - 1], getPoints()[i]));
            }
        }

        length = len;
        centroid = new FmGeoPoint(sumx / getPoints().length, sumy / getPoints().length,null);
        extent = newextent;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer v) {
        value = v;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Map<String,Object> properties) {
        this.properties = properties;
    }

   
  
}
