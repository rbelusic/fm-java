package org.fm.geo.model;

/**
 *
 * @author RobertoB
 */
public class FmGeoLineSegment {

    private FmGeoPoint fromPoint;
    private FmGeoPoint toPoint;

    public FmGeoLineSegment(FmGeoPoint fp, FmGeoPoint tp) {
        fromPoint = fp;
        toPoint = tp;
    }

    /**
     * @return the fromPoint
     */
    public FmGeoPoint getFromPoint() {
        return fromPoint;
    }

    /**
     * @param fromPoint the fromPoint to set
     */
    public void setFromPoint(FmGeoPoint fromPoint) {
        this.fromPoint = fromPoint;
    }

    /**
     * @return the toPoint
     */
    public FmGeoPoint getToPoint() {
        return toPoint;
    }

    /**
     * @param toPoint the toPoint to set
     */
    public void setToPoint(FmGeoPoint toPoint) {
        this.toPoint = toPoint;
    }

    public double getLength() {
        return Math.sqrt(
            Math.pow((getFromPoint().getXpos() - getToPoint().getXpos()), 2.0)
            + Math.pow((getFromPoint().getYpos() - getToPoint().getYpos()), 2.0)
        );
    }

}
