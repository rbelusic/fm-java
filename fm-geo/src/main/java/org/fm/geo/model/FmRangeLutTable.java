package org.fm.geo.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.imageio.IIOException;

public class FmRangeLutTable<T> implements FmLutTable<T>{        
    public static class Range<T> implements Comparable<Range>{
        final double min;
        final double max;
        final T value;
                
        public Range(double from, double to, T val) {
            min = from;
            max = to;
            value = val;            
        }

        public int compareTo(Range o) {
            return (min > o.min ? 1 : (min == o.min ? 0 : -1));
        }
    }
    
    private Range<T>[] categories;
    private double[] categoriesEnd;
    
    private FmRangeLutTable(ArrayList<Range> ranges) {
        this(ranges.toArray(new Range[ranges.size()]));        
    }
    
    private FmRangeLutTable(Range[] ranges) {
        _configure(ranges);
    }
        
    public T mapValue(double value) {
        int pos = Arrays.binarySearch(categoriesEnd, value);
        int posorg = pos;
        if(pos < 0) {
            pos = (pos)*-1 -1;
        } else {
            pos = pos+1;
        }        

        if(pos == categoriesEnd.length) {
            return null;
        }
        T retc = value >= categories[pos].min  && value < categories[pos].max ? 
                categories[pos].value : null;
        
        return retc;
    }

    // priv
    private void _configure(Range [] rg) {
        // sort ranges by min value
        categories = rg;
        Arrays.sort(categories);        
        categoriesEnd = new double[categories.length];
        for(int ci = 0; ci < categories.length; ci++) {
            categoriesEnd[ci] = categories[ci].max;
        }
    }
    
    public void dump() {
        for(Range c: categories) {
            System.out.println("" + c.min + " => " + c.max + " =" + c.value);
        }
    
    }
    
    public static FmRangeLutTable instance(Range[] ranges) {
        return new FmRangeLutTable(ranges);
    }
    
    // format: min max val
    public static FmRangeLutTable<Integer> instance(String fname) throws IOException {
        ArrayList<Range> ranges = new ArrayList<Range>();
        
        BufferedReader br = new BufferedReader(new FileReader(fname));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            int lc = 0;
            while (line != null) {
                lc++;
                line = line.trim();
                if(!line.isEmpty() && !line.startsWith("#")) {
                    String[] tokens = line.split("\\s+");
                    if(tokens.length != 3) {
                        throw new IIOException("Invalid range definition on line " + lc);
                    }

                    ranges.add(
                        new Range(
                            Double.parseDouble(tokens[0]),
                            Double.parseDouble(tokens[1]),
                            Integer.parseInt(tokens[2])
                        )
                    );                
                }
                line = br.readLine();
            }
            
            return new FmRangeLutTable(ranges);
        } finally {
            br.close();
        }        
        
    }        
}
