package org.fm.geo.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.fm.map.model.FmDevicePoint;


/**
 *
 * @author RobertoB
 */
public class FmGeoReferenceHdrFile implements FmGeoReference {
/* {1,0,0,-1,0,0}
Line 1: A: pixel size in the x-direction in map units/pixel
Line 2: D: rotation about y-axis
Line 3: B: rotation about x-axis
Line 4: E: pixel size in the y-direction in map units, almost always negative[3]
Line 5: C: x-coordinate of the center of the upper left pixel
Line 6: F: y-coordinate of the center of the upper left pixel
*/
    public final static int A = 0; //A
    public final static int D = 1; //D
    public final static int B = 2;   //C
    public final static int E = 3;   //B
    public final static int C = 4;  //E
    public final static int F = 5;  //F
    
    private double BF;
    private double EC;
    private double AE_DB;
    private double DC;
    private double AF;
    private double values[] = {
        Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN
    };

    public FmGeoReferenceHdrFile(double [] inpvalues) {
        values = inpvalues;
        BF = values[B] * values[F];
        EC = values[E] * values[C];
        AE_DB = values[A] * values[E] - values[D] * values[B];
        DC = values[D] * values[C];
        AF = values[A] * values[F];
    }   
    
    public FmGeoReferenceHdrFile(String fname) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(fname));
        String line;

        int nl = 0;
        try {
            while (nl < 6 && (line = br.readLine()) != null) {
                values[nl] = Double.parseDouble(line);
                nl++;
            }
            BF = values[B] * values[F];
            EC = values[E] * values[C];
            AE_DB = values[A] * values[E] - values[D] * values[B];
            DC = values[D] * values[C];
            AF = values[A] * values[F];

        } finally {
            try {
                br.close();
            } catch (IOException ex) {
            }
        }
    }

    @Override
    public FmGeoPoint toGeoPoint(FmDevicePoint p) {
        double x = p.getX();
        double y = p.getY();

        return new FmGeoPoint(
                x * values[A] + y * values[B] + values[C],
                x * values[D] + y * values[E] + values[F],
                null);
    }

    @Override
    public FmDevicePoint toLocalPoint(FmGeoPoint p) {
        double xt = p.getXpos();
        double yt = p.getYpos();

        double x = (values[E] * xt
                - values[B] * yt
                + BF
                - EC) / AE_DB;
        double y = (-values[D] * xt
                + values[A] * yt
                + DC
                - AF) / AE_DB;


        return new FmDevicePoint((int) x, (int) y);
    }

    @Override
    public double getValue(int idx) {
        return(idx>=0 && idx < values.length ? values[idx] : Double.NaN);
    }
}
