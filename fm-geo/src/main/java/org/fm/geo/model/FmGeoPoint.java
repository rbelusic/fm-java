package org.fm.geo.model;

import java.util.Map;


public class FmGeoPoint implements FmGeoFeature {    
    private final static double ROUNDING = 0.000001;
    
    private Map<String,Object> properties;
    
    private double xpos;
    private double ypos;
    private double zpos;
    private Integer value;

    
    public FmGeoPoint(double x, double y) {
        this(x,y,null);
    }
    public FmGeoPoint(double x, double y, Integer v) {
        this(x, y,v,null);
    }
    public FmGeoPoint(double x, double y, Integer v, Map<String, Object> fdata) {
        setXpos(x);
        setYpos(y);
        setValue(v);
        setZpos(0.0);
        setProperties(fdata);
    }
    
    public boolean equals(FmGeoPoint p) {
        return(Math.abs(p.getXpos() - getXpos()) < ROUNDING && Math.abs(p.getYpos() - getYpos()) < ROUNDING);
    }
    
    /**
     * @return the xpos
     */
    public double getXpos() {
        return xpos;
    }

    /**
     * @param xpos the xpos to set
     */
    public void setXpos(double xpos) {
        this.xpos = xpos;
    }

    /**
     * @return the ypos
     */
    public double getYpos() {
        return ypos;
    }

    /**
     * @param ypos the ypos to set
     */
    public void setYpos(double ypos) {
        this.ypos = ypos;
    }

    public double getLength() {
        return 0.0;
    }

    public FmGeoExtent getExtent() {
        return new FmGeoExtent(getXpos(),getYpos(),getXpos(),getYpos());
    }

    public FmGeoPoint getCenter() {
        return new FmGeoPoint(getXpos(),getYpos(),null);
    }
    
    public double getArea() {
        return 0.0;
    }

    /**
     * @return the zpos
     */
    public double getZpos() {
        return zpos;
    }

    /**
     * @param zpos the zpos to set
     */
    public void setZpos(double zpos) {
        this.zpos = zpos;
    }

    public void print() {
        System.out.println("    POINT (x=" + xpos + " | y=" + ypos +  ")" );
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer v) {
        value = v;
    }

    /**
     * @return the properties
     */
    public Map<String,Object> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Map<String,Object> properties) {
        this.properties = properties;
    }
    
}
