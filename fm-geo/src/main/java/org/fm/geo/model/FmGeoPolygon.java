package org.fm.geo.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author RobertoB
 */
public class FmGeoPolygon implements FmGeoFeature {
    private Map<String,Object> properties;
    
    private FmGeoLine[] lines;
    private FmGeoPolygon[] innerPolygons;
    private FmGeoExtent extent;
    private FmGeoPoint centroid;
    private double length;
    private double area;
    private Integer value;
    private Integer NODATA = -999;

    public FmGeoPolygon(FmGeoLine[] outline, FmGeoPolygon[] inner) {
        this(outline,inner, 0);
    }
    
    public FmGeoPolygon(FmGeoLine[] outline, FmGeoPolygon[] inner, Integer v) {
        this(outline, inner, v, null);
    }
    
    public FmGeoPolygon(FmGeoLine[] outline, FmGeoPolygon[] inner, Integer v,Map<String, Object> fdata) {
        lines = outline;
        value = v;
        innerPolygons = inner == null ? new FmGeoPolygon[0] : inner;
        properties = fdata;
        _recalc();
    }

    public double getLength() {
        return length;
    }

    public FmGeoExtent getExtent() {
        return extent;
    }

    /**
     * LOŠE, TREBA UZETI U OBZIR UNUTARNJE POLIGONE JER ĆE OVA METODA SLUŽITI
     * KAO POZICIJA ZA LABELU
     *
     * @return
     */
    public FmGeoPoint getCenter() {
        return centroid;
    }

    public double getArea() {
        return area;
    }

    public FmGeoLine[] getLines() {
        return lines;
    }

    /**
     * @return the innerPolygons
     */
    public FmGeoPolygon[] getInnerPolygons() {
        return innerPolygons;
    }

    public List<List<FmGeoPoint>> getOutlines() {
        List<List<FmGeoPoint>> outlines = new ArrayList<List<FmGeoPoint>>();
        
        int[] ldone = new int[lines.length];
        for (int i = 0; i < ldone.length; i++) {
            ldone[i] = 0;
        }
        
        // svaka linija je u pocetku outline
        for(FmGeoLine lin: lines) {            
            List<FmGeoPoint> outline = new ArrayList<FmGeoPoint>();
            FmGeoPoint[] points = lin.getPoints();
            outline.addAll(Arrays.asList(points));
            
            outlines.add(outline);
        }
        
        // sad gledamo ima li povezanih
        for(int o=0; o < outlines.size(); o++) {
            List<FmGeoPoint> ol =  outlines.get(o);
            FmGeoPoint firstPoint = ol.get(0);
            FmGeoPoint lastPoint = ol.get(ol.size()-1);
            
            // self closed
            if(firstPoint.equals(lastPoint)) {
                ldone[o] = 1;
                continue;
            } 
            
            //find next line
            for(int o2=0; o2 < outlines.size(); o2++) {
                if(ldone[o2] == 1) continue;
                List<FmGeoPoint> ol2 =  outlines.get(o2);                
                FmGeoPoint firstPoint2 = ol2.get(0);
                FmGeoPoint lastPoint2 = ol2.get(ol2.size()-1);
                
                if(firstPoint2.equals(lastPoint)) {
                    // dodaj na kraj 1.
                    ol.addAll(ol2.subList(1, ol2.size()-1));
                    outlines.remove(ol2);    
                    ldone[o] = 1;
                    break;
                    
                } else if(lastPoint2.equals(firstPoint)) {
                    // dodaj na kraj 2.
                    ol2.addAll(ol.subList(1, ol.size()-1));
                    outlines.remove(ol);
                    ldone[o] = 1;
                    break;
                }
            }
        }

        for (int i = 0; i < ldone.length; i++) {
            if (ldone[i] == 0) {
                System.out.println("POLY: outline not closed!");
                print();
                return null;
            }
        }
        
        return outlines;
    }

    private void _recalc() {
        double sum = 0.0;
        double cx = 0.0, cy = 0.0;

        extent = null;
        length = 0.0;
        for (FmGeoLine pline : lines) {
            length += pline.getLength();
            if (extent == null) {
                extent = new FmGeoExtent(pline.getExtent());
            } else {
                extent.addExtent(pline.getExtent());
            }
            FmGeoPoint[] points = pline.getPoints();

            for (int i = 0; i < points.length - 1; i++) {
                cx = cx + (points[i].getXpos() + points[i + 1].getXpos())
                        * (points[i].getYpos() * points[i + 1].getXpos()
                        - points[i].getXpos() * points[i + 1].getYpos());
                cy = cy + (points[i].getYpos() + points[i + 1].getYpos())
                        * (points[i].getYpos() * points[i + 1].getXpos()
                        - points[i].getXpos() * points[i + 1].getYpos());

                sum = sum
                        + (points[i].getXpos() * points[i + 1].getYpos())
                        - (points[i].getYpos() * points[i + 1].getXpos());
            }
        }
        double clcarea = Math.abs(0.5 * sum);

        if (innerPolygons != null) {
            for (FmGeoPolygon p : innerPolygons) {
                clcarea -= p.getArea();
            }
        }
        area = clcarea;

        cx /= (6 * area);
        cy /= (6 * area);
        centroid = new FmGeoPoint(cx, cy,null);
    }

        public Integer getValue() {
        return value == null? NODATA : value;
    }

    public void setValue(Integer v) {
        value = v;
    }

    public void print() {
        System.out.println("POLY (lines=" + lines.length + " | innerPolys=" + innerPolygons.length + ")");
        for (FmGeoLine l : lines) {
            l.print();
        }
    }

    /**
     * @return the properties
     */
    public Map<String,Object> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Map<String,Object> properties) {
        this.properties = properties;
    }

}
