package org.fm.geo.model;

import java.util.Map;


/**
 *
 * @author RobertoB
 */
public interface FmGeoFeature {
    
    public Number getValue();
    
    public double getLength();
    
    public FmGeoExtent getExtent();
    
    public FmGeoPoint getCenter();    
    
    public double getArea();

    public Map<String, Object> getProperties();
}
