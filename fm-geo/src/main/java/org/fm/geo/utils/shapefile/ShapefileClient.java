/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.geo.utils.shapefile;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoFeature;
import org.fm.geo.model.FmGeoLine;
import org.fm.geo.model.FmGeoPoint;
import org.fm.geo.model.FmGeoPolygon;
import org.fm.geo.utils.shapefile.files.dbf.DBF_Field;
import org.fm.geo.utils.shapefile.types.ShpMultiPoint;
import org.fm.geo.utils.shapefile.types.ShpPoint;
import org.fm.geo.utils.shapefile.types.ShpPolyLine;
import org.fm.geo.utils.shapefile.types.ShpPolygon;
import org.fm.geo.utils.shapefile.types.ShpShape;
import org.fm.utils.StringUtil;

/**
 *
 * @author rbelusic
 */
public class ShapefileClient {

    private final static int SHP_MAGIC = 9994;
    private final static int SHP_VERSION = 1000;
    private final static int DBF_FIELD_SIZE_BYTES = 32;
    private String baseName;
    private FileChannel shpFile;
    private FileChannel shxFile;
    private FileChannel dbfFile;
    private Type shapeType;
    private int fileLength;
    private FmGeoExtent extent;
    private FmGeoExtent extentZM;
    private long numberOfRecords;
    private short dbfHeaderSize;
    private int dbfNumberOfRecords;
    private Date dbfDate;
    private byte dbfFileType;
    private ArrayList<DbfField> dbfFields;
    private short dbfRecordSize;

    public ShapefileClient(String filename) throws Exception {
        baseName = StringUtil.removeExtension(filename);
        _configure();
    }

    private void _configure() throws IOException, Exception {
        shpFile = FileChannel.open(
                FileSystems.getDefault().getPath(getBaseName() + ".shp"),
                EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE));

        shxFile = FileChannel.open(
                FileSystems.getDefault().getPath(getBaseName() + ".shx"),
                EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE));

        dbfFile = FileChannel.open(
                FileSystems.getDefault().getPath(getBaseName() + ".dbf"),
                EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE));

        _readShpHeader();
        _readDbfHeader();
    }

    /**
     * @return the baseName
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName the baseName to set
     */
    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    private void _readDbfHeader() throws Exception {
        ByteBuffer bb = ByteBuffer.allocate(32);
        bb.clear();
        dbfFile.position(0);
        dbfFile.read(bb);
        bb.flip();

        // READ HEADER
        bb.order(ByteOrder.LITTLE_ENDIAN);
        dbfFileType = bb.get(0);
        dbfDate = new Date(bb.get(1), bb.get(2), bb.get(3));
        dbfNumberOfRecords = bb.getInt(4);
        dbfHeaderSize = bb.getShort(8);
        dbfRecordSize = bb.getShort(10);

        int pos = 32; // start of fields
        int numFields = (dbfHeaderSize - pos - 1) / DBF_FIELD_SIZE_BYTES; // cant this be easier???
        dbfFields = new ArrayList<DbfField>();
        for (int i = 0; i < numFields; i++) {
            bb.clear();
            dbfFile.position(pos);
            dbfFile.read(bb);
            bb.flip();

            dbfFields.add(new DbfField(bb));
            pos += DBF_FIELD_SIZE_BYTES;
        }
    }

    private void _readShpHeader() throws Exception {
        ByteBuffer bb = ByteBuffer.allocate(100);
        bb.order(ByteOrder.BIG_ENDIAN);

        try {
            bb.clear();
            shpFile.position(0);
            shpFile.read(bb);
            bb.flip();

            if (bb.getInt(0) != SHP_MAGIC) {
                throw new Exception("(ShapeFile) error: SHP_MAGIC = " + SHP_MAGIC);
            }

            // file length
            fileLength = bb.getInt(24);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            int SHP_version_read = bb.getInt(28);
            if (SHP_version_read != SHP_VERSION) {
                throw new Exception("(ShapeFile) error: SHP_VERSION = " + SHP_VERSION);
            }
            int SHP_shape_type = bb.getInt(32);

            try {
                shapeType = Type.byID(SHP_shape_type);
            } catch (Exception e) {
                e.printStackTrace();
            }
            double xmin = bb.getDouble(36); // x-min
            double ymin = bb.getDouble(44); // y-min
            double xmax = bb.getDouble(52); // x-max
            double ymax = bb.getDouble(60); // y-max

            extent = new FmGeoExtent(xmin, ymin, xmax, ymax);

            double zmin = bb.getDouble(68); // z-min
            double zmax = bb.getDouble(76); // z-max
            double mmin = bb.getDouble(84); // m-min
            double mmax = bb.getDouble(92); // m-max

            extentZM = new FmGeoExtent(zmin, mmin, zmax, mmax);
            setNumberOfRecords((new File(baseName + ".shx").length() - 100) / 8);
        } finally {
            bb.order(ByteOrder.BIG_ENDIAN);
        }
    }

    /**
     * @return the shapeType
     */
    public Type getShapeType() {
        return shapeType;
    }

    /**
     * @return the fileLength
     */
    public int getFileLength() {
        return fileLength;
    }

    /**
     * @return the extent
     */
    public FmGeoExtent getExtent() {
        return extent;
    }

    /**
     * @return the extentZM
     */
    public FmGeoExtent getExtentZM() {
        return extentZM;
    }

    private long[] getRecordIndex(long n, long[] info) throws Exception {
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.order(ByteOrder.BIG_ENDIAN);

        shxFile.position(100 + n * 8);
        shxFile.read(bb);
        bb.flip();

        info[0] = bb.getInt() * 2;
        info[1] = bb.getInt() * 2;

        return info;
    }

    private Map<String, Object> getRecordData(long n) throws Exception {
        HashMap<String, Object> data = new HashMap<String, Object>();
        ByteBuffer bb = ByteBuffer.allocate(dbfRecordSize);
        bb.clear();
        dbfFile.position((long) dbfHeaderSize + n * (long) dbfRecordSize);
        dbfFile.read(bb);
        bb.flip();

        bb.order(ByteOrder.LITTLE_ENDIAN);

        byte[] recData = new byte[dbfRecordSize];
        bb.get(recData);

        try {
            String recStringData = new String(recData, "ISO-8859-1");
            int from = 1;
            for (int i = 0; i < dbfFields.size(); i++) {
                DbfField fld = dbfFields.get(i);
                int to = from + fld.length;
                data.put(fld.name, fld.parse(recStringData.substring(from, to)));
                from = to;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (StringIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        return data;
    }

    
    public List<FmGeoFeature> getRecord(long n) throws Exception {
        long[] shpIndex = new long[2];
        getRecordIndex(n, shpIndex);

        ByteBuffer bb = ByteBuffer.allocate((int) shpIndex[1] + 8); // add header length
        shpFile.position(shpIndex[0]);
        shpFile.read(bb);
        bb.flip();
        bb.order(ByteOrder.BIG_ENDIAN);
        int recordNumber = bb.getInt();
        int contentLength = bb.getInt();
        bb.order(ByteOrder.LITTLE_ENDIAN);
        int shpTypeInt = bb.getInt();
        bb.rewind();
        Type shpType = Type.byID(shpTypeInt);
        Map<String, Object> fdata = getRecordData(n);
        
        List<FmGeoFeature> features = new ArrayList<FmGeoFeature>();
        ShpShape shape = null;
        switch (shpType) {
            case Polygon:
            case PolygonM:
            case PolygonZ:
                shape = new ShpPolygon(shpType);
                shape.read(bb);
                //System.out.println(" Loading Shape of type POLY ...");
                ShpPolygon poly = (ShpPolygon) shape;
                //pl.print();
                int[] polyparts = poly.getParts();
                double[][] polypoints = poly.getPoints();
                ArrayList<FmGeoPolygon> innerPolys = new ArrayList<FmGeoPolygon>();
                ArrayList<FmGeoPoint> polyfmpts = new ArrayList<FmGeoPoint>();
                ArrayList<FmGeoLine> polyOutline = new ArrayList<FmGeoLine>();

                for (int i = 0; i < polyparts.length; i++) {
                    polyfmpts.clear();
                    for (int p = polyparts[i]; p < (i == polyparts.length - 1 ? polypoints.length : polyparts[i + 1]); p++) {
                        polyfmpts.add(new FmGeoPoint(polypoints[p][0], polypoints[p][1]));
                    }
                    FmGeoPoint[] fmptsArr = polyfmpts.toArray(new FmGeoPoint[polyfmpts.size()]);
                    FmGeoLine[] outlines = new FmGeoLine[1];
                    outlines[0] = new FmGeoLine(fmptsArr,shape.getRecordNumber());
                    if (polyparts.length > 1 && poly.isInnerCircle(i)) {
                        innerPolys.add(new FmGeoPolygon(outlines, null,shape.getRecordNumber()));
                    } else {
                        polyOutline.add(outlines[0]);
                    }
                }


                features.add(new FmGeoPolygon(
                        polyOutline.toArray(new FmGeoLine[polyOutline.size()]),
                        innerPolys.toArray(new FmGeoPolygon[innerPolys.size()]),
                        shape.getRecordNumber(),fdata));
                break;
            case PolyLine:
            case PolyLineM:
            case PolyLineZ:
                shape = new ShpPolyLine(shpType);
                shape.read(bb);
                ShpPolyLine pl = (ShpPolyLine) shape;
                int[] parts = pl.getParts();
                double[][] plpoints = pl.getPoints();
                ArrayList<FmGeoPoint> fmpts = new ArrayList<FmGeoPoint>();
                for (int i = 0; i < parts.length; i++) {
                    fmpts.clear();
                    for (int p = parts[i]; p < (i == parts.length - 1 ? plpoints.length : parts[i + 1]); p++) {
                        fmpts.add(new FmGeoPoint(plpoints[p][0], plpoints[p][1],shape.getRecordNumber()));
                    }
                    features.add(
                            new FmGeoLine(
                            fmpts.toArray(new FmGeoPoint[fmpts.size()]),
                            shape.getRecordNumber(),fdata));
                }

                break;
            case Point:
            case PointM:
            case PointZ:
                shape = new ShpPoint(shpType);
                shape.read(bb);
                double[] point = ((ShpPoint) shape).getPoint();
                features.add(new FmGeoPoint(point[0], point[1], (int) point[2],fdata));
                break;
            case MultiPoint:
            case MultiPointM:
            case MultiPointZ:
                shape = new ShpMultiPoint(shpType);
                shape.read(bb);
                double[][] mppoints = ((ShpMultiPoint) shape).getPoints();
                for (int i = 0; i < ((ShpMultiPoint) shape).getNumberOfPoints(); i++) {
                    features.add(
                            new FmGeoPoint(mppoints[i][0], mppoints[i][1], (int) mppoints[i][2],fdata));
                }
                break;
        }

        return features;
    }

    /**
     * @return the numberOfRecords
     */
    public long getNumberOfRecords() {
        return numberOfRecords;
    }

    /**
     * @param numberOfRecords the numberOfRecords to set
     */
    public void setNumberOfRecords(long numberOfRecords) {
        this.numberOfRecords = numberOfRecords;
    }

    public void addShape(FmGeoFeature f) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static class DbfField {
        private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
        public final String name;
        public final char type;
        public final int displacement;
        public final int length;
        public final byte decimalPlaces;
        public final int next;
        public final byte step;

        private DbfField(ByteBuffer bb) throws UnsupportedEncodingException {
            // read data
            byte[] fldNameBytes = new byte[11];  //0-11
            bb.get(fldNameBytes);
            String tname = new String(fldNameBytes, "ISO-8859-1");
            name = tname.substring(0, tname.indexOf('\0')).trim();

            type = (char) bb.get();            
            displacement = bb.getInt();
            length = bb.get() & 0xFF; // so we get values from 0-255.
            decimalPlaces = bb.get();
            next = bb.getInt();
            step = bb.get();
        }
        
        public Object parse(String rec) {            
            if(type =='N') {
                return  decimalPlaces > 0 ? new Double(rec): new Integer(rec);
            } else if(type =='D') {
                try {
                    return dateFormat.parse(rec);
                } catch (ParseException ex) {
                    return new String(rec.trim());
                }
            } else if(type =='L') {
                char value = rec.charAt(0);
                if(value == '1' || value == 'T' || value == 't' || value == 'Y' || value == 'y') {
                    return new Boolean(true);
                } else {
                    return new Boolean(false);
                }
            } else {
                return new String(rec.trim());
            }
        
        }
    }
}
