package org.fm.geo.utils;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.fm.geo.model.FmGeoExtent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author RobertoB
 */
public class WmsClient {
    public class WmsLayer {
        private HashMap<String, String> attrs = new HashMap<String, String>();
        private ArrayList<String> CRS = new ArrayList<String>();
        private Map<String, FmGeoExtent> bbox = new HashMap<String, FmGeoExtent>();
        private String title;
        private String name;
        private String abstract_;
        private String keywordList;
        private boolean queryable;

        public WmsLayer(WmsLayer parent) {
            if (parent != null) {
                CRS.addAll(parent.CRS);
                bbox.putAll(parent.bbox);
            }
        }

        /**
         * @return the attrs
         */
        public HashMap<String, String> getAttrs() {
            return attrs;
        }

        /**
         * @param attrs the attrs to set
         */
        public void setAttrs(HashMap<String, String> attrs) {
            this.attrs = attrs;
        }

        /**
         * @return the CRS
         */
        public ArrayList<String> getCRS() {
            return CRS;
        }

        /**
         * @param CRS the CRS to set
         */
        public void setCRS(ArrayList<String> CRS) {
            this.CRS = CRS;
        }

        /**
         * @return the bbox
         */
        public Map<String, FmGeoExtent> getBbox() {
            return bbox;
        }

        public FmGeoExtent getExtent(String crs) {
            if(crs== null || crs.isEmpty()) {
                return(bbox.values().iterator().next());
            }
            return bbox.get(crs);
        }

        /**
         * @param bbox the bbox to set
         */
        public void setBbox(Map<String, FmGeoExtent> bbox) {
            this.bbox = bbox;
        }

        /**
         * @return the title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @param title the title to set
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the abstract_
         */
        public String getAbstract() {
            return abstract_;
        }

        /**
         * @param abstract_ the abstract_ to set
         */
        public void setAbstract(String abstract_) {
            this.abstract_ = abstract_;
        }

        /**
         * @return the keywordList
         */
        public String getKeywordList() {
            return keywordList;
        }

        /**
         * @param keywordList the keywordList to set
         */
        public void setKeywordList(String keywordList) {
            this.keywordList = keywordList;
        }

        /**
         * @return the queryable
         */
        public boolean isQueryable() {
            return queryable;
        }

        /**
         * @param queryable the queryable to set
         */
        public void setQueryable(boolean queryable) {
            this.queryable = queryable;
        }
    }
    
    // wmsclient
    private Map<String, WmsLayer> mapLayers = new HashMap<String, WmsLayer>();
    private ArrayList<String> mapFormats = new ArrayList<String>();
    private final DocumentBuilder xmlbuilder;
    private String baseUrl;
    private String format;

    // constructor
    public WmsClient(String url) throws ParserConfigurationException, Exception {
        this.xmlbuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        setBaseUrl(url);
        _loadCapabilities();
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    private void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Map<String, WmsLayer> getLayers() {
        return mapLayers;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    // priv
    // 	http://geoportal.dgu.hr/wms
    private void _loadCapabilities() throws Exception {
        String url = getBaseUrl() + "?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetCapabilities";

        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(fetch(url)));
        Document xmldoc = xmlbuilder.parse(is);

        getLayers().clear();

        // capab node        
        NodeList nodes = xmldoc.getElementsByTagName("Capability");
        if (nodes.getLength() == 0) {
            return;
        }
        Node capNode = nodes.item(0);

        // layers
        nodes = ((Element) capNode).getElementsByTagName("Layer");
        for (int i = 0; i < nodes.getLength(); i++) {
            parseLayer(nodes.item(i), null);
        }

        // request/getmap/formats
        nodes = ((Element) capNode).getElementsByTagName("Request");
        if (nodes.getLength() == 0) {
            return;
        }
        Node reqNode = nodes.item(0);
        nodes = ((Element) reqNode).getElementsByTagName("GetMap");
        if (nodes.getLength() == 0) {
            return;
        }
        Node mapNode = nodes.item(0);
        nodes = ((Element) mapNode).getElementsByTagName("Format");
        for (int i = 0; i < nodes.getLength(); i++) {
            mapFormats.add(nodes.item(i).getTextContent());
            if (getFormat() == null) {
                setFormat(nodes.item(i).getTextContent());
            }
        }

    }

    public Image getMap(String layerName, String proj,
            double xmin, double ymin,
            double xmax, double ymax,
            int width, int height) throws MalformedURLException, IOException {
        String url = getBaseUrl()
                + "?SERVICE=WMS"
                + "&VERSION=1.1.1"
                + "&REQUEST=GetMap"
                + "&SRS=" + proj
                + "&BBOX=" + xmin + "," + ymin + "," + xmax + "," + ymax
                + "&WIDTH=" + width
                + "&HEIGHT=" + height
                + "&FORMAT=" + getFormat()
                + "&TRANSPARENT=TRUE"
                + "&EXCEPTIONS=application/vnd.ogc.se_inimage"
                + "&LAYERS=" + layerName;

        System.out.println("URL:" + url);
        return ImageIO.read(new URL(url));
    }

    public String getFeatureInfo(
            String layerName, String proj,
            double xmin, double ymin,
            double xmax, double ymax,
            int width, int height,
            int xpix, int ypix) {
        String url = getBaseUrl()
                + "?SERVICE=WMS"
                + "&VERSION=1.1.1"
                + "&REQUEST=GetFeatureInfo"
                + "&SRS=" + proj
                + "&BBOX=" + xmin + "," + ymin + "," + xmax + "," + ymax
                + "&WIDTH=" + width
                + "&HEIGHT=" + height
                + "&INFO_FORMAT=text/plain"
                + "&LAYERS=" + layerName
                + "&QUERY_LAYERS=" + layerName
                + "&X=" + xpix
                + "&Y=" + ypix;
        System.out.println("URL:" + url);
        BufferedReader in;
        String inputLine, info = "";
        try {
            in = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
            while ((inputLine = in.readLine()) != null) {
                info += inputLine + "\n";
            }
            in.close();

        } catch (IOException e) {
        }

        return info;
    }

    // priv
    private void parseLayer(Node n, WmsLayer parent) {
        WmsLayer layer = new WmsLayer(parent);
        Node qyttr = n.getAttributes().getNamedItem("queryable");
        layer.setQueryable(qyttr == null ? false : qyttr.getNodeValue().equals("1"));

        NodeList chnodes = n.getChildNodes();
        for (int j = 0; j < chnodes.getLength(); j++) {
            Node chn = chnodes.item(j);

            if (chn.getNodeName().equals("Title")) {
                layer.setTitle(chn.getTextContent());
            } else if (chn.getNodeName().equals("Name")) {
                layer.setName(chn.getTextContent());
            } else if (chn.getNodeName().equals("KeywordList")) {
                layer.setKeywordList(chn.getTextContent());
            } else if (chn.getNodeName().equals("Abstract")) {
                layer.setAbstract(chn.getTextContent());
            } else if (chn.getNodeName().equals("CRS")) {
                layer.getCRS().add(chn.getTextContent());
            } else if (chn.getNodeName().equals("BoundingBox")) {
                FmGeoExtent bbox = parseBbox(chn);
                layer.getBbox().put(bbox.getCRS(), bbox);
            }
        }

        NodeList nodes = ((Element) n).getElementsByTagName("Layer");
        for (int i = 0; i < nodes.getLength(); i++) {
            parseLayer(nodes.item(i), layer);
        }

        if (layer.getName() != null && !layer.name.isEmpty()) {
            ArrayList<String> newCRS = new ArrayList<String>();
            Map<String, FmGeoExtent> newbbox = new HashMap<String, FmGeoExtent>();
            for (String crs : layer.getCRS()) {
                if (layer.getBbox().containsKey(crs)) {
                    newCRS.add(crs);
                    newbbox.put(crs, layer.getBbox().get(crs));
                }
            }
            if (!layer.bbox.isEmpty()) {
                mapLayers.put(layer.getName(), layer);
            }
        }
    }

    private FmGeoExtent parseBbox(Node n) {
        NamedNodeMap attrs = n.getAttributes();

        return new FmGeoExtent(
                Double.parseDouble(attrs.getNamedItem("minx").getNodeValue()),
                Double.parseDouble(attrs.getNamedItem("miny").getNodeValue()),
                Double.parseDouble(attrs.getNamedItem("maxx").getNodeValue()),
                Double.parseDouble(attrs.getNamedItem("maxy").getNodeValue()),
                attrs.getNamedItem("CRS").getNodeValue());
    }

    private String fetch(String urlstr) throws IOException {
        URL url = new URL(urlstr);
        InputStream is = url.openStream();
        int ptr = 0;
        StringBuffer buffer = new StringBuffer();
        while ((ptr = is.read()) != -1) {
            buffer.append((char) ptr);
        }
        System.out.println(buffer.toString());
        return buffer.toString();
    }

    private void dumpLayers() {
        System.out.println("Accepts:");
        for (String ff : mapFormats) {
            System.out.printf("  %s\n", ff);
        }

        for (Map.Entry<String, WmsLayer> e : mapLayers.entrySet()) {
            WmsLayer layer = e.getValue();
            System.out.println("Layer: [" + layer.getName() + "]");
            System.out.println("  title:" + layer.getTitle());
            System.out.println("  abstract:" + layer.getAbstract());
            System.out.println("  keywords:" + layer.getKeywordList());
            for (FmGeoExtent bb : layer.getBbox().values()) {
                System.out.printf("  BBOX: [%s] => %f %f %f %f\n", bb.getCRS(), bb.getXmin(), 
                        bb.getYmin(), bb.getXmax(), bb.getYmax());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        WmsClient cli = new WmsClient("http://geoportal.dgu.hr/wms");
        cli.dumpLayers();
        Image img = cli.getMap("TK25", "CRS:84", 13.237613, 45.220592, 13.948977, 45.531936, 1024, 768);
        File outputfile = new File("saved.jpg");
        ImageIO.write((RenderedImage) img, "jpg", outputfile);

        System.out.println("INFO:");
        System.out.println(cli.getFeatureInfo("TK25", "CRS:84", 13.237613, 45.220592, 13.948977, 45.531936, 1024, 768, 256, 645));
    }
}
