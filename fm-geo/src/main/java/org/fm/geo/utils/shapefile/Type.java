package org.fm.geo.utils.shapefile;

public enum Type {

    //  Null_Shape   (  0 ), 
    //  
    //  Point        (  1 ), 
    //  PolyLine     (  3 ), 
    //  Polygon      (  5 ), 
    //  MultiPoint   (  8 ), 
    //  
    //  PointZ       ( 11 ), 
    //  PolyLineZ    ( 13 ), 
    //  PolygonZ     ( 15 ), 
    //  MultiPointZ  ( 18 ), 
    //  
    //  PointM       ( 21 ), 
    //  PolyLineM    ( 23 ), 
    //  PolygonM     ( 25 ), 
    //  MultiPointM  ( 28 ), 
    //  
    //  MultiPatch   ( 31 )  
    /**
     * ID= 0
     */
    NullShape(0, false, false),
    /**
     * ID= 1
     */
    Point(1, false, false),
    /**
     * ID=11
     */
    PointZ(11, true, true),
    /**
     * ID=21
     */
    PointM(21, false, true),
    /**
     * ID= 3
     */
    PolyLine(3, false, false),
    /**
     * ID=13
     */
    PolyLineZ(13, true, true),
    /**
     * ID=23
     */
    PolyLineM(23, false, true),
    /**
     * ID= 5
     */
    Polygon(5, false, false),
    /**
     * ID=15
     */
    PolygonZ(15, true, true),
    /**
     * ID=25
     */
    PolygonM(25, false, true),
    /**
     * ID= 8
     */
    MultiPoint(8, false, false),
    /**
     * ID=18
     */
    MultiPointZ(18, true, true),
    /**
     * ID=28
     */
    MultiPointM(28, false, true),
    /**
     * ID=31
     */
    MultiPatch(31, true, true);
    private int ID;
    private boolean has_z_values;
    private boolean has_m_values;

    private Type(int ID, boolean has_z_values, boolean has_m_values) {
        this.has_z_values = has_z_values;
        this.has_m_values = has_m_values;
        this.ID = ID;
    }

    public int ID() {
        return this.ID;
    }

    public static Type byID(int ID) throws Exception {
        for (Type st : Type.values()) {
            if (st.ID == ID) {
                return st;
            }
        }
        throw new Exception("ShapeType: " + ID + " does not exist");
    }

    public boolean hasZvalues() {
        return has_z_values;
    }

    public boolean hasMvalues() {
        return has_m_values;
    }

    public boolean isTypeOfPolygon() {
        return (this == Type.Polygon | this == Type.PolygonM | this == Type.PolygonZ);
    }

    public boolean isTypeOfPolyLine() {
        return (this == Type.PolyLine | this == Type.PolyLineM | this == Type.PolyLineZ);
    }

    public boolean isTypeOfPoint() {
        return (this == Type.Point | this == Type.PointM | this == Type.PointZ);
    }

    public boolean isTypeOfMultiPoint() {
        return (this == Type.MultiPoint | this == Type.MultiPointM | this == Type.MultiPointZ);
    }
}