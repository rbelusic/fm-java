
package org.fm.geo.dataset.model;

import java.util.ArrayList;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoFeature;
import org.fm.geo.model.FmGeoPoint;
import org.fm.map.model.FmMapSymbolset;

/**
 *
 * @author RobertoB
 */
public interface FmGeoDatasetVector extends FmGeoDataset {

    public DmGeoDatasetVectorInfo getInfo();

    public FmGeoDatasetVectorOptions getOptions();

    public ArrayList<FmGeoFeature> getFeatures();

    public ArrayList<FmGeoFeature> findFeatures(FmGeoExtent e);

    public FmGeoFeature findFeature(FmGeoPoint p);

    public void addFeatures(ArrayList<FmGeoFeature> features) throws Exception;

    public void addFeature(FmGeoFeature features) throws Exception;
}
