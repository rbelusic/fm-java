package org.fm.geo.dataset.model;


public interface DmGeoDatasetRasterInfo extends DmGeoDatasetInfo {
    
    public <T extends Number>  Class<T>  getValueClass(Class<T> aClass);
    
    public void setValueClass(Class<? extends Number> aClass);

    /* 
     * NROWS          number of rows in the image
     */
    public Integer getNrows(Integer def);

    public Integer setNrows(Integer def);

    /* 
     NCOLS          number of columns in the image
     */
    public Integer getNcols(Integer def);

    public Integer setNcols(Integer def);

    /* 
     NBANDS         number of spectral bands in the image (1 for a DEM)
     */
    public Integer getNbands(Integer def);

    public Integer setNbands(Integer def);    
    
    /* 
     ULXMAP         longitude of the center of the upper-left pixel (decimal degrees)
     */
    public Double getUlxmap(Double def);

    public Double setUlxmap(Double def);

    /* 
     ULYMAP         latitude  of the center of the upper-left pixel (decimal degrees)
     */
    public Double getUlymap(Double def);

    public Double setUlymap(Double def);

    /* 
     XDIM           x dimension of a pixel in geographic units (decimal degrees)
     */
    public Double getXdim(Double def);

    public Double setXdim(Double def);

    /* 
     YDIM           y dimension of a pixel in geographic units (decimal degrees)
     */
    public Double getYdim(Double def);

    public Double setYdim(Double def);    

    /* 
     NODATA          No data value
     */
    public String getNodata(String def);

    public String setNodata(String def);    
}
