package org.fm.geo.dataset.model;

/**
 *
 * @author RobertoB
 */
public interface FmGeoDatasetRaster extends FmGeoDataset {
    public DmGeoDatasetRasterInfo getInfo();

    public Number getNoDataValue();

    public FmGeoDatasetTile getTile(int row, int band, int col, int width, int height) throws Exception;
    public void setTile(FmGeoDatasetTile t)  throws Exception;
}
