package org.fm.geo.dataset.impl;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.dm.DmFactory;
import org.fm.geo.dataset.model.FmGeoDatasetOptions;
import org.fm.geo.dataset.model.FmGeoDatasetVector;
import org.fm.geo.dataset.model.DmGeoDatasetVectorInfo;
import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.geo.dataset.model.FmGeoDatasetVectorOptions;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoFeature;
import org.fm.geo.model.FmGeoPoint;
import org.fm.geo.model.FmGeoReferenceWorldFile;
import org.fm.geo.utils.shapefile.ShapefileClient;

public class FmGeoDatasetShapefile implements FmGeoDatasetVector {

    private FmGeoDatasetVectorOptions options;
    private DmGeoDatasetShapefileInfo info;
    private FmGeoExtent extent;
    private ArrayList<FmGeoFeature> features;
    private boolean changed;
    private FileChannel dsFile;
    private ShapefileClient client;

    public <T extends FmGeoDatasetOptions> void open(T opts) throws Exception {
        options = (FmGeoDatasetVectorOptions) opts;
        _configure();
    }
    // I

    public void close() {
        try {
            dsFile.close();
        } catch (IOException ex) {
            Logger.getLogger(FmGeoDatasetDem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DmGeoDatasetVectorInfo getInfo() {
        return DmFactory.create(DmGeoDatasetShapefileInfo.class, info.getAttr());
    }

    public ArrayList<FmGeoFeature> getFeatures() {
        ArrayList<FmGeoFeature> response = new ArrayList<FmGeoFeature>();

        for (int i = 0; i < client.getNumberOfRecords(); i++) {
            try {
                List<FmGeoFeature> features = client.getRecord(i);
                for (FmGeoFeature f : features) {
                    response.add(f);
                }
            } catch (Exception ex) {
                Logger.getLogger(FmGeoDatasetShapefile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return response;
    }

    // I
    public ArrayList<FmGeoFeature> findFeatures(FmGeoExtent e) {
        ArrayList<FmGeoFeature> response = new ArrayList<FmGeoFeature>();

        for (int i = 0; i < client.getNumberOfRecords(); i++) {
            try {
                List<FmGeoFeature> features = client.getRecord(i);
                for (FmGeoFeature f : features) {
                    if (f.getExtent().intersects(e)) {
                        response.add(f);
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(FmGeoDatasetShapefile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return response;
    }

    // I
    public FmGeoFeature findFeature(FmGeoPoint p) {
        return null;

    }

    // I
    public void addFeatures(ArrayList<FmGeoFeature> ft) {
        for (FmGeoFeature f : ft) {
            client.addShape(f);
        }
        changed = true;
    }

    public void addFeature(FmGeoFeature f) {
        client.addShape(f);
        changed = true;
    }

    public FmGeoDatasetVectorOptions getOptions() {
        return options;

    }

    public FmGeoExtent getExtent() {
        return client.getExtent();
    }

    public FmGeoReferenceWorldFile getReference() {
        return null;
    }

    private void _configure() throws Exception {
        client = new ShapefileClient(options.fileName);
    }

    public Type getType() {
        return FmGeoDataset.Type.VECTOR;
    }
}
