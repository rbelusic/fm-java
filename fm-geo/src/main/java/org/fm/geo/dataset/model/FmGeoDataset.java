package org.fm.geo.dataset.model;

import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoReferenceWorldFile;

public interface FmGeoDataset {

    public static enum Type {

        RASTER, VECTOR;
        
    }

    public static enum OpenMode {

        READ, WRITE, CREATE;
    }

    public Type getType();
    
    public DmGeoDatasetInfo getInfo();

    public <T extends FmGeoDatasetOptions> void open(T options) throws Exception;

    public void close();

    public <T extends FmGeoDatasetOptions> T getOptions();

    public FmGeoExtent getExtent();

    public FmGeoReferenceWorldFile getReference();
}
