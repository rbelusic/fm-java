/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.geo.dataset.impl;

import org.fm.geo.dataset.model.DmGeoDatasetRasterInfo;

/**
 * DM used for reckless loading of DEM hdr files
 * @author rbelusic
 */
public interface DmGeoDatasetDemInfo extends DmGeoDatasetRasterInfo {
    /*
     BYTEORDER      byte order in which image pixel values are stored 
     M = Motorola byte order (most significant byte first)
     */

    public String getByteorder(String def);

    public void setByteorder(String def);

    /*
     LAYOUT         organization of the bands in the file
     BIL = band interleaved by line (note: the DEM is a single
     band image)
     */
    public String getLayout(String def);

    public void setLayout(String def);


    /* 
     NBITS          number of bits per pixel (16 for a DEM)
     */
    public Integer getNbits(Integer def);

    public Integer setNbits(Integer def);

    /* 
     * 
     BANDROWBYTES   number of bytes per band per row (twice the number of columns
     for a 16-bit DEM)
     */
    public Integer getBandrowbytes(Integer def);

    public Integer setBandrowbytes(Integer def);

    /* 
     TOTALROWBYTES  total number of bytes of data per row (twice the number of
     columns for a single band 16-bit DEM)
     */
    public Integer getTotalrowbytes(Integer def);

    public Integer setTotalrowbytes(Integer def);

    /* 
     BANDGAPBYTES   the number of bytes between bands in a BSQ format image
     (0 for a DEM)
     */
    public Integer getBandgapbytes(Integer def);

    public Integer setBandgapbytes(Integer def);

    /* 
     NODATA         value used for masking purposes
     */
    public Integer getNodata(Integer def);

    public Integer setNodata(Integer def);

}
