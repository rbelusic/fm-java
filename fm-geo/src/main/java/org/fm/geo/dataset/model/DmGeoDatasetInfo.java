package org.fm.geo.dataset.model;

import org.fm.dm.DmObject;

public interface DmGeoDatasetInfo extends DmObject {
    /* 
     * TYPE       
     */
    public FmGeoDataset.Type getType(FmGeoDataset.Type def);

    public FmGeoDataset.Type setType(FmGeoDataset.Type def);    

    
}
