package org.fm.geo.dataset.model;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;

public class FmGeoDatasetTile<T extends Number> {

    private final static String ERR_BUFF_TYPE = "Invalid buffer type";
    final public int band;
    final public int x;
    final public int y;
    final public int width;
    final public int height;
    final public int length;
    final public int cellTypeBits;
    final public int cellTypeBytes;
    final public Class<T> type;
    
    private boolean changed;
    private ByteBuffer buffer;
    private Method typePutMethod;
    private final Method typeGetMethod;

    public FmGeoDatasetTile(Class<T> bufferClass, int bnd, int ty, int tx, int th, int tw) throws Exception {
        type = bufferClass;
        band = bnd;
        x = tx;
        y = ty;
        width = tw;
        height = th;
        length = width * height;
        changed = false;
        Class cellType;

        if (type.equals(Byte.class)) {
            cellType = Byte.TYPE;
            cellTypeBits = Byte.SIZE;
        } else if (type.equals(Short.class)) {
            cellType = Short.TYPE;
            cellTypeBits = Short.SIZE;
        } else if (type.equals(Integer.class)) {
            cellType = Integer.TYPE;
            cellTypeBits = Integer.SIZE;
        } else if (type.equals(Float.class)) {
            cellType = Float.TYPE;
            cellTypeBits = Float.SIZE;
        } else if (type.equals(Double.class)) {
            cellType = Double.TYPE;
            cellTypeBits = Double.SIZE;
        } else {
            throw new Exception(ERR_BUFF_TYPE);
        }
        cellTypeBytes = cellTypeBits / 8;
        buffer = ByteBuffer.allocate(length * cellTypeBytes);

        // find typed method
        Method m;
        try {
            m = getClass().getDeclaredMethod("get" + type.getSimpleName(), new Class[]{});
        } catch(Exception e) {
            throw new Exception(ERR_BUFF_TYPE);
        }
        typeGetMethod = m;
        
        try {           
            m = getClass().getDeclaredMethod("put" + type.getSimpleName(), new Class[]{type});
        } catch(Exception e) {
            throw new Exception(ERR_BUFF_TYPE);
        }
        typePutMethod = m;
    }

    public void put(byte[] buff, int off) throws Exception {
        buffer.rewind();
        buffer.put(buff, off, buff.length);
        setChanged(true);
    }

    public void put(byte[] buff, int row, int col) throws Exception {
        int off = (row * width + col) * cellTypeBytes;
        put(buff, off);
    }

    public <T extends Number> T get(int row, int col) throws Exception {
        int off = (row * width + col) * cellTypeBytes;
        buffer.position(off);
        T value = (T) typeGetMethod.invoke(this);
        return value;
    }

    public <T extends Number> void put(T value, int row, int col) throws Exception {
        int off = (row * width + col) * cellTypeBytes;
        buffer.position(off);
        typePutMethod.invoke(this, value);
        setChanged(true);
    }

    /**
     * @return the changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }
    
    // private buffer access
    private Byte getByte() {
        return buffer.get();
    }
    
    private void putByte(Byte b) {
        buffer.put(b);
    }
    
    private Short getShort() {
        return buffer.getShort();
    }
    
    private void putShort(Short n) {
        buffer.putShort(n);
    }
    
    private Integer getInt() {
        return buffer.getInt();
    }
    
    private void putInt(Integer n) {
        buffer.putInt(n);
    }
    
    private Float getFloat() {
        return buffer.getFloat();
    }
    
    private void putFloat(Float n) {
        buffer.putFloat(n);
    }
    
    private Double getDouble() {
        return buffer.getDouble();
    }
    
    private void putDouble(Double n) {
        buffer.putDouble(n);
    }
    
}
