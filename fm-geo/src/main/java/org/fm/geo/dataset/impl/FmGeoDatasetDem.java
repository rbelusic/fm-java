package org.fm.geo.dataset.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.geo.dataset.model.FmGeoDatasetOptions;
import org.fm.geo.dataset.model.FmGeoDatasetRaster;
import org.fm.geo.dataset.model.DmGeoDatasetRasterInfo;
import org.fm.geo.dataset.model.FmGeoDataset;
import org.fm.geo.dataset.model.FmGeoDatasetRasterOptions;
import org.fm.geo.model.FmGeoExtent;
import org.fm.geo.model.FmGeoReferenceWorldFile;

import org.fm.utils.StringUtil;
import org.fm.geo.dataset.model.FmGeoDatasetTile;

/**
 *
 * @author rbelusic
 */
public class FmGeoDatasetDem implements FmGeoDatasetRaster {

    private final static List<FmGeoDatasetTile> cache = new ArrayList<FmGeoDatasetTile>();
    private FmGeoDatasetRasterOptions options;
    private FmGeoExtent dsExtent;
    private DmGeoDatasetDemInfo dsHeader;
    private FmGeoReferenceWorldFile dsReference;
    private FileChannel dsFile;
    private Number noDataValue = -9999;

    public <T extends FmGeoDatasetOptions> void open(T opts) throws Exception {
        options = (FmGeoDatasetRasterOptions) opts;
        _configure();
    }

    public DmGeoDatasetRasterInfo getInfo() {
        return DmFactory.create(DmGeoDatasetDemInfo.class, dsHeader.getAttr());
    }

    public FmGeoDatasetTile getTile(int row, int band, int col, int width, int height) throws Exception {
        //System.out.printf("TILE: r: %d, c: %d, w: %d, h: %d\n",row,col,width,height);
        FmGeoDatasetTile tile = new FmGeoDatasetTile(
                dsHeader.getValueClass(null), band, row, col, height, width);

        int off = row * dsHeader.getTotalrowbytes(null)
                + band * dsHeader.getBandrowbytes(null)
                + col * tile.cellTypeBytes;
        byte[] arr = new byte[width * tile.cellTypeBytes];
        for (int r = 0; r < height; r++) {
            _read(off, width * tile.cellTypeBytes).get(arr);
            tile.put(arr, r, 0);
            off += dsHeader.getTotalrowbytes(null);
        }
        return tile;

    }

    public void setTile(FmGeoDatasetTile t) throws Exception {
    }

    public void close() {
        try {
            dsFile.close();
        } catch (IOException ex) {
            Logger.getLogger(FmGeoDatasetDem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public FmGeoDatasetOptions getOptions() {
        return options;

    }

    public FmGeoExtent getExtent() {
        return dsExtent;

    }

    private void _configure() throws IOException {
        // load header first
        String hdrFileLc = StringUtil.replaceExtension(options.fileName, ".hdr");
        String hdrFileUc = StringUtil.replaceExtension(options.fileName, ".HDR");
        try {
            dsHeader = _reclessFileLoad(DmGeoDatasetDemInfo.class, hdrFileLc, -1);
        } catch (FileNotFoundException ex) {
            dsHeader = _reclessFileLoad(DmGeoDatasetDemInfo.class, hdrFileUc, -1);
        }
        dsHeader.setType(Type.RASTER);
        int nbits = dsHeader.getNbits(null);
        dsHeader.setValueClass(
                nbits == 8 ? Byte.class : (nbits == 16 ? Short.class : (null)));
        String ndstr = dsHeader.getNodata("0");
        noDataValue =
                nbits == 8 ? new Byte(ndstr) : (
                nbits == 16 ? new Short(ndstr) : (
                nbits == 32 ? new Integer(ndstr) : (
                null)));



        // create new reference from header, ignore file
        dsReference = new FmGeoReferenceWorldFile(
                new double[]{
            dsHeader.getXdim(Double.NaN),
            0., 0.,
            -dsHeader.getYdim(Double.NaN),
            dsHeader.getUlxmap(Double.NaN),
            dsHeader.getUlymap(Double.NaN)
        });

        // calculate extent
        dsExtent = new FmGeoExtent(
                dsHeader.getUlxmap(null),
                dsHeader.getUlymap(null) - dsHeader.getYdim(null) * dsHeader.getNrows(null),
                dsHeader.getUlxmap(null) + dsHeader.getXdim(null) * dsHeader.getNcols(null),
                dsHeader.getUlymap(null));

        // open DEM
        Path fp = FileSystems.getDefault().getPath(options.fileName);

        dsFile = FileChannel.open(fp,
                EnumSet.of(StandardOpenOption.READ, StandardOpenOption.WRITE));
    }

    public <T extends DmObject> T _reclessFileLoad(Class<T> cls, String fileName, int ccase) throws FileNotFoundException, IOException {
        BufferedReader br;

        br = new BufferedReader(new FileReader(fileName));

        T dm = DmFactory.create(cls);

        String line;
        try {
            while ((line = br.readLine()) != null) {
                String[] attr = line.split("\\s+");
                attr[0] =
                        ccase == -1
                        ? attr[0].toLowerCase()
                        : (ccase == 1 ? attr[0].toUpperCase() : attr[0]);
                if (dm.isAttr(attr[0])) {
                    dm.setAttr(attr[0], attr[1]);
                }
            }
            return dm;
        } finally {
            try {
                br.close();
                for (Map.Entry e : dm.getAttr().entrySet()) {
                    System.out.println("RECLLOAD ["
                            + dm.getSubClassName() + "] "
                            + e.getKey() + "=" + e.getValue());
                }
            } catch (IOException ex) {
            }
        }
    }

    private ByteBuffer _read(int start, int len) throws IOException {

        ByteBuffer byteBuffer = ByteBuffer.allocate(len);
        byteBuffer.clear();

        dsFile.position(start);
        dsFile.read(byteBuffer);
        byteBuffer.flip();

        return byteBuffer;
    }

    public FmGeoReferenceWorldFile getReference() {
        return dsReference;
    }

    /**
     * @return the noDataValue
     */
    public Number getNoDataValue() {
        return noDataValue;
    }

    public Type getType() {
        return FmGeoDataset.Type.RASTER;
    }
}
