/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.geo.algol;

import java.util.TreeSet;
import org.fm.geo.model.FmGeoLineSegment;
import org.fm.geo.model.FmGeoPoint;

public class DelaunayTriangulation {

    private int[][] adjMatrix;

    DelaunayTriangulation(int size) {
        this.adjMatrix = new int[size][size];
    }

    public int[][] getAdj() {
        return this.adjMatrix;
    }

    public TreeSet getEdges(FmGeoPoint[] points) {
        TreeSet result = new TreeSet();

        if (points.length == 2) {
            this.adjMatrix[0][1] = 1;
            this.adjMatrix[1][0] = 1;
            result.add(new FmGeoLineSegment(points[0], points[0]));
            return result;
        }

        for (int i = 0; i < points.length - 2; i++) {
            for (int j = i + 1; j < points.length; j++) {
                for (int k = i + 1; k < points.length; k++) {
                    if (j == k) {
                        continue;
                    }
                    double xn = (points[j].getYpos() - points[i].getYpos()) * (points[k].getZpos() - points[i].getZpos())
                            - (points[k].getZpos() - points[i].getYpos()) * (points[j].getZpos() - points[i].getZpos());

                    double yn = (points[k].getXpos() - points[i].getXpos()) * (points[j].getZpos() - points[i].getZpos())
                            - (points[j].getXpos() - points[i].getXpos()) * (points[k].getZpos() - points[i].getZpos());

                    double zn = (points[j].getXpos() - points[i].getXpos()) * (points[k].getZpos() - points[i].getYpos())
                            - (points[k].getXpos() - points[i].getXpos()) * (points[j].getYpos() - points[i].getYpos());
                    boolean flag;
                    if (flag = (zn < 0 ? 1 : 0) != 0) {
                        for (int m = 0; m < points.length; m++) {
                            flag = (flag) && ((points[m].getXpos() - points[i].getXpos()) * xn
                                    + (points[m].getYpos() - points[i].getYpos()) * yn
                                    + (points[m].getZpos() - points[i].getZpos()) * zn <= 0);
                        }
                    }

                    if (!flag) {
                        continue;
                    }
                    result.add(new FmGeoLineSegment(points[i], points[j]));
                    result.add(new FmGeoLineSegment(points[j], points[k]));
                    result.add(new FmGeoLineSegment(points[k], points[i]));
                    this.adjMatrix[i][j] = 1;
                    this.adjMatrix[j][i] = 1;
                    this.adjMatrix[k][i] = 1;
                    this.adjMatrix[i][k] = 1;
                    this.adjMatrix[j][k] = 1;
                    this.adjMatrix[k][j] = 1;
                }

            }
        }

        return result;
    }
}
